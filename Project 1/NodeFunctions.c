#include <stdio.h>
#include <stdlib.h>
#include "Structures_Defines.h"

int CreateNodeUtilHigh(int id, struct HashTableElement* tobecreated, int size){//dhmiourgei enan Komvo   
    if(tobecreated == NULL){
        printf("failure: HashElement is NULL in CreateNodeUtilHigh\n");
        return 0;
    }
    
    static int vertex = 1;
    struct Node* newNode;//an apotyxei to malloc
    if((newNode = malloc(sizeof(struct Node))) == NULL){
        printf("failure: newNode malloc FAILED in CreateNodeUtilHigh\n");
        exit(1);
    }//arxikopoiei tn Outlink Inlink list tou neou komvou
    newNode->InlinkList = NULL;
    newNode->OutlinkList = NULL;
    newNode->id = id;
    newNode->visited = 0;//tn thetei mh episkepsimo
    newNode->vertex = vertex;
    tobecreated->GraphNode = newNode;//to element tou hashtable deixnei stn komvo
    tobecreated->id = id;// to id tou element einai to id tou komvou
    tobecreated->key = id%size;//omoiws kai to key toy element einai to id modulo size
    tobecreated->nextCollisionGraphNode = NULL;//to epomenp element einai Collision Element k arxikopoeitai me NULL
    vertex++;
    newNode = NULL;
    printf("success: CREATED N%07d\n", id);
    return 1;
}

int DeleteNodeUtilHigh(int id, struct HashTableElement* tobedeleted){//diagrafei enan Komvo N
    
    if((tobedeleted == NULL)||(tobedeleted->GraphNode == NULL)){
        printf("failure: HashElement is NULL or pointer to GraphNode%07d is NULL in DeleteNodeUtilHigh\n", id);
        return 0;
    }
    
    if(tobedeleted->GraphNode->OutlinkList != NULL){
        if(tobedeleted->GraphNode->OutlinkList->startOutLink == NULL){
            printf("CRITICAL failure: InlinkList without startOutlink!\n");
            exit(1);//den ginetai na yparxei outlinklist xwris arxiko element
        }
        if(tobedeleted->GraphNode->OutlinkList->startOutLink != NULL){
            DeleteOutLinksForN(tobedeleted->GraphNode);//alliws diegrapse ta Outlinks tou N
            DeleteOutLinkListForN(tobedeleted->GraphNode); //kai tn list efoson einai adeia
        }
    }    
    
    if(tobedeleted->GraphNode->InlinkList != NULL){
        if(tobedeleted->GraphNode->InlinkList->startInLink == NULL){
            printf("CRITICAL failure: InlinkList without startOutlink!\n");
            exit(1);//den ginetai na yparxei inlinklist xwris arxiko element
        }
        if(tobedeleted->GraphNode->InlinkList->startInLink != NULL){
            DeleteInLinksForN(tobedeleted->GraphNode);//alliws diegrapse ta Inlinks tou N
            DeleteInLinkListForN(tobedeleted->GraphNode);//kai tn list efoson einai adeia
        }   
    }//efoson exoun diagrafei oi lists tou komvou ston opoio deixnei to HashTableElement, arxikopoioumai me -1 ta members tou      
    tobedeleted->GraphNode->id = -1;
    tobedeleted->GraphNode->vertex = -1;
    tobedeleted->GraphNode->visited = -1;
    return 1;
}

int PrintNode(struct Node* N){
    if(N == NULL){
        printf("failure: N is NULL in PrintNode\n");
        return 0;       
    } 
    
    if(N->OutlinkList == NULL){
        return 1; 
    }
    
    if(N->OutlinkList->startOutLink == NULL){
       printf("CRITICAL failure: N%07d 's startOutLinK is NULL in PrintNode\n", N->id); 
       exit(1);//den ginetai na yparxei list xwris element
    }
    struct OutLinkElem* cursor = N->OutlinkList->startOutLink;
    struct Link* cursorLink = cursor->LinkToNextNode;
    struct Node* cursorNode = cursorLink->toNode;
    while(cursor!= NULL){//gia kathe oulink element, typwnei tis plhrofories pou apaitoudai
        printf("(N%07d, %f___%d), ", cursorNode->id, cursorLink->amount, cursorNode->id);
        cursor = cursor->nextOutLink;
        if(cursor  == NULL){
            cursorLink = NULL;
            cursorNode = NULL;
            break;
        }
        cursorLink = cursor->LinkToNextNode;
        cursorNode = cursorLink->toNode;
    }
    printf("\n");
    return 1;
}

int LookUpUtilHigh(int in, int out, int sum, int id, struct HashStruct** hashStruct){
    
    if((*hashStruct) == NULL){
        printf("failure: HashStruct for N%07d is NULL in LookUpUtilHigh\n", id);
        return 0;
    }
    int table_index = id%(*hashStruct)->size;//vres an yparxei o komvos stn grafo
    int check1 = SearchNodeInHashTableAndCollisionLists(hashStruct, id);
    struct HashTableElement* cursorN1 = (*hashStruct)->table[table_index];
    if(check1 >=0){//psaxnei tn komvo
        while(cursorN1!=NULL){
            if(check1 == 0){
                break;
            }
            
            cursorN1 = cursorN1->nextCollisionGraphNode;
            --check1;
        }
        return LookUpUtilMid(in, out, sum, cursorN1->GraphNode);   
    }
    else{//alliws ean dn yparxei o komvos
        printf("failure: N%07d doesnt exist in Graph\n", id);
        return 0;
    }
    printf("failure: Error N%07d in LookUpUtilHigh\n", id);
    return 0; 
}

int LookUpUtilMid(int in, int out, int sum, struct Node* N){ 

    if(N == NULL){
        printf("failure: N%07d is NULL in LookUpUtilMid\n", N->id);
        return 0;
    }
    double sumout = 0;
    if(N->OutlinkList != NULL){//ean exei ekswterikes akmes
        struct OutLinkElem* cursorOut = N->OutlinkList->startOutLink;
        while(cursorOut!= NULL){//briskei to synoliko amount tous
            sumout+=cursorOut->LinkToNextNode->amount;
            cursorOut = cursorOut->nextOutLink;
        }
        cursorOut = NULL;
        
    }
    else{
        sumout = 0;
    }
    
    double sumin = 0;
    if(N->InlinkList!= NULL){
        struct InLinkElem* cursorIn = N->InlinkList->startInLink;
        while(cursorIn!= NULL){//omoiws gia eswterikes akmes
            sumin+=cursorIn->LinkToCurrentNode->amount;
            cursorIn = cursorIn->nextInLink;
        }
        cursorIn = NULL;

    }
    else{
        sumin = 0;
    }

    if(out == 1){
        printf("success: out(N%07d) = %f\n",N->id, sumout); 
        return 1;
    }
    else if(in == 1){
        printf("success: in(N%07d) = %f\n",N->id, sumin);
        return 1;
    }
    else if(sum == 1){//to sum h diafora
        printf("success: sum(N%07d) = %f\n",N->id, sumin-sumout);
        return 1;
    }
    printf("failure: Error in LookUpMid\n");
    return 0;
}