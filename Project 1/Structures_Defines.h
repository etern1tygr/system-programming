/* 
 * File:   Structures.h
 * Author: etern1ty
 *
 * Created on February 26, 2016, 4:11 PM
 */


struct HashStruct{//periexei to hashtable kai megethos
    int size;
    struct HashTableElement** table;
};


struct HashTableElement{//einai to element tou hashtable
    int key;
    int id;
    struct HashTableElement* nextCollisionGraphNode;//deixnei ston epomeno collision node
    struct Node* GraphNode; //kai stn komvo tou grafou
};


struct InLinkElem{// dipla syndedemenh lista apo elements p deixnoun se eswterikes akmes
    struct InLinkElem* nextInLink;
    struct InLinkElem* prevInLink;
    struct Link* LinkToCurrentNode;
};


struct ListOfInLinks{//h lista
    int size;
    struct Node* GraphNode;//se poio komvo anhkei
    struct InLinkElem* startInLink;
    struct InLinkElem* lastInLink;

};
 

struct OutLinkElem{// dipla syndedemenh lista apo elements p deixnoun se ekswterikes akmes
    struct OutLinkElem* nextOutLink;
    struct OutLinkElem* prevOutLink;
    struct Link* LinkToNextNode;
};


struct ListOfOutLinks{//h adistoixh lista
    int size;
    struct Node* GraphNode;//se poio komvo anhkei
    struct OutLinkElem* startOutLink;
    struct OutLinkElem* lastOutLink;
};


struct Link{//h akmh metaksy dyo komvwn, deixnei stn komvo proelefsis kai katalhkshs
    double amount;
    struct Node* toNode;
    struct Node* fromNode;
};


struct Node {//o komvos me tis list's tou
    int id;
    int vertex;
    int visited;
    struct ListOfInLinks* InlinkList;
    struct ListOfOutLinks* OutlinkList;
};


int InLinkListCreate(struct Node* N);
int OutLinkListCreate(struct Node* N);

int InLinkElemCreate(struct Node* N, struct Link* newIncoming);
int OutLinkElemCreate(struct Node* N, struct Link* newOutcoming);

int DeleteInLinkListForN(struct Node* N);
int DeleteOutLinkListForN(struct Node* N);

int DeleteInLinkElemForN2(struct Node* N1, struct Node* N2, struct Link* pivotLink);
int DeleteOutLinkElemForN1(struct Node* N1, struct Node* N2, struct Link* pivotLink);

int DeleteInLinksForN(struct Node* N);
int DeleteOutLinksForN(struct Node* N);

int ConnUtilHigh(struct HashStruct** hashStruct, int id1, int id2);
int ConnUtilMid(struct Node* N1, struct Node* N2);
int ConnUtilBottom(struct Node* N1, struct Node* N2, struct OutLinkElem* cursor, struct Node* N1stable);

int LookUpUtilHigh(int in, int out, int sum, int id, struct HashStruct** hashStruct);
int LookUpUtilMid(int in, int out, int sum, struct Node* N);

int TriangleUtilHigh(struct HashStruct** hashStruct, int id, int k);
int TriangleUtilMid(struct Node* N, int k);

int CreateHashStruct(struct HashStruct** hashStruct, int HashEntries);
int DeleteHashStruct(struct HashStruct** hashStruct);

int FillHashTableElem(struct HashStruct** hashStruct, int id);
int UnFillHashTableElem(struct HashStruct** hashStruct, int id);

int SearchNodeInHashTableAndCollisionLists(struct HashStruct** hashStruct, int id);
int EmptyGraph(struct HashStruct** hashStruct);
int PrintGraph(struct HashStruct** hashStruct);
int PrintNode(struct Node* N);

int CreateCollisionElement(struct HashTableElement* prevElement, int id );
int DeleteCollisionElement(struct HashTableElement** tableElement, int stepsToCollisionNode);

int CreateNodeUtilHigh(int id, struct HashTableElement* tobecreated, int size);
int DeleteNodeUtilHigh(int id, struct HashTableElement* tobedeleted);

int AddTranUtilHigh(struct HashStruct** hashStruct, int id1, int id2, double amount);
int AddTranUtilMid(struct Node* N1, struct Node* N2, double amount);

int DelTranUtilHigh(struct HashStruct** hashStruct, int id1, int id2);
int DelTranUtilMid(struct Node* N1, struct Node* N2);

int TraceFlowUtilBottom(int* array, int i, struct Node* N, int path_length, int pstable, struct OutLinkElem* cursor, struct Node* Nstable, double summit, int* found);
int TraceFlowUtilMid(struct Node* N, int path_length);
int TraceFlowUtilHigh(struct HashStruct** hashStruct, int id, int path_length);

int AllCyclesUtilHigh(struct HashStruct** hashStruct, int id);
int AllCyclesUtilMid(struct Node* N);
int AllCyclesUtilBottom(struct Node* N, int path_length, struct OutLinkElem* cursor, struct Node* Nstable, int* found);

void TerminalArg(FILE** f, int* HashEntries, int argc, char** argv);

int createnodes(struct HashStruct** hashStruct, char* str);
int delnodes(struct HashStruct** hashStruct, char* str);
int addtran(struct HashStruct** hashStruct, char* str);
int deltran(struct HashStruct** hashStruct, char* str);
int lookup(struct HashStruct** hashStruct, char* str);
int triangle(struct HashStruct** hashStruct, char* str);
int conn(struct HashStruct** hashStruct, char* str);
int traceflow(struct HashStruct** hashStruct, char* str);
int allcycles(struct HashStruct** hashStruct, char* str);
int print(struct HashStruct** hashStruct, char* str);
int bye(struct HashStruct** hashStruct, char* str);