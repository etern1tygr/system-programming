#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "Structures_Defines.h"


int createnodes(struct HashStruct** hashStruct, char* str){
    char* token;
    char* function;
    int token_length;
    int id;
    int isgarbage = 0;
    int node_position = 0;


    char command[strlen(str)+1];//static pinaka n chars +1 gia null terminator
    strncpy(command, str, (strlen(str)));//copy to string stn static
    if(command[strlen(str)-1] == '\n'){
        command[strlen(str)-1] = '\0';
    }
    else{
        command[strlen(str)] = '\0';
    }
    fflush( stdout );
    token = strtok(command, " ");//apo edw kai pera xrhshmopoioumai tn strtok
    function = "createnodes";
    if(!strcmp(token, function)){//eksetazoume an prokeitai gia tn synarthsh p theloume
        token = strtok(NULL, " ");
        printf("\n");
        if(token == NULL){// a dn parei orismata mhnhma lathous
            printf("failure: No arguments at all\n");
            return 1;
        }
        while (token != NULL){
            printf("\n");
            token_length = strlen(token);
            if(token_length == 7){//prepei na eisagoume 7digit = id string
                char digits[token_length];
                strncpy(digits, token, token_length);
                int i = 0;
                while(i<= token_length-1){//elegxos ean proketai gia digits
                    if(isdigit(digits[i]) == 0){
                        isgarbage = 1; //ean dn einai
                        break;
                    }
                    i++;
                }
                if(isgarbage){//to kanoume ignore kai synezoyme me to epomeno string/token ths protashs
                    token = strtok(NULL, " ");
                    isgarbage = 0;
                    continue;
                }
                
                node_position++;//gia osa 7digit tokens lavoume, kratame to plhthos tous
                id = atoi(token);//kai pername to id
                if(!FillHashTableElem(hashStruct, id)){
                     printf("failure: In FillHashTableElem\n");
                }
            }
            token = strtok(NULL, " ");
        }
        if(node_position == 0){//an telika kai me to teleftaio token dn lavoume 7digit string, tote failure
            printf("failure: Not valid Node id\n");
            return 1;
        }
        return 1;
    }//an dn einai h function p theloume
    return 0;
}

int delnodes(struct HashStruct** hashStruct, char* str){//akrivws h idia logikh xrhshmopoieitai kai stn delnodes
    char* token;
    char* function;
    int token_length;
    int id;
    int isgarbage = 0;
    int node_position = 0;


    char command[strlen(str)+1];
    strncpy(command, str, (strlen(str)));
    if(command[strlen(str)-1] == '\n'){
        command[strlen(str)-1] = '\0';
    }
    else{
        command[strlen(str)] = '\0';
    }
    fflush( stdout );
    token = strtok(command, " ");
    function = "delnodes";
    if(!strcmp(token, function)){
        token = strtok(NULL, " ");
        printf("\n");
        if(token == NULL){
            printf("failure: No arguments at all\n");
            return 1;
        }
        while (token != NULL){
            token_length = strlen(token);
            printf("\n");
            if(token_length == 7){
                char digits[token_length];
                strncpy(digits, token, token_length);
                int i = 0;
                while(i<= token_length-1){
                    if(isdigit(digits[i]) == 0){
                        isgarbage = 1;
                        break;
                    }
                    i++;
                }
                if(isgarbage){
                    token = strtok(NULL, " ");
                    isgarbage = 0;
                    continue;
                }
                node_position++;
                id = atoi(token);
                if(!UnFillHashTableElem(hashStruct, id)){
                    printf("failure: In UnFillHashTableElem \n");
                }

            }
            token = strtok(NULL, " ");
        }
        if(node_position == 0){
            printf("failure: Not valid Node id \n");
            return 1;
        }
        return 1;
    }
    return 0;
}

int lookup(struct HashStruct** hashStruct, char* str){
    char* token;
    char* function;
    char* argument_in = "in";
    char* argument_out = "out";
    char* argument_sum = "sum";
    int in = 0;
    int out = 0;
    int sum = 0;
    int token_length;
    int id;
    
    char command[strlen(str)+1];
    strncpy(command, str, (strlen(str)));
    if(command[strlen(str)-1] == '\n'){
        command[strlen(str)-1] = '\0';
    }
    else{
        command[strlen(str)] = '\0';
    }
    fflush( stdout );
    token = strtok(command, " ");
    function = "lookup";
    if(!strcmp(token, function)){
        token = strtok(NULL, " ");
        if(token == NULL){
            printf("failure: No arguments at all\n");
            return 1;
        }
        while(token!= NULL){
            token_length = strlen(token);
            if(!strcmp(token, argument_in)){//an diavasoume "in"
                in++;
                if(in>1){//an to diavasoume panw apo mia fora einai error
                    printf("failure: Double in argument\n");
                    return 1;
                }
            }
            else if(!strcmp(token, argument_out)){//omoiws gia out kai sum
                out++;
                if(out>1){
                    printf("failure: Double out argument\n");
                    return 1;
                }
            }
            else if(!strcmp(token, argument_sum)){
                sum++;
                if(sum>1){
                    printf("failure: Double sum argument\n");
                    return 1;
                }
            }
            else if((token_length == 7)&&(in||out||sum)){//an meta to out h out h sum diavasoume 7digit
                char digits[token_length];
                strncpy(digits, token, token_length);
                int i = 0;
                while(i<= token_length-1){
                    if(isdigit(digits[i]) == 0){//elegxoume an einai odws digit
                        printf("failure: Not valid Node id\n");
                        return 1;
                    }
                    i++;
                }
                id = atoi(token);
                if((strtok(NULL, " "))!= NULL){
                    printf("failure: More than two arguments\n");
                    return 1;
                }
                if((in == 1)&&(out == 0)&&(sum == 0)){//ean lavame in kai 7digit
                    if(!LookUpUtilHigh(in, 0, 0, id, hashStruct)){
                        printf("failure: In LookUpUtilHigh \n");
                    }
                    return 1;
                }
                else if((out == 1)&&(in == 0)&&(sum == 0)){//ean lavame out kai 7digit
                    if(!LookUpUtilHigh(0, out, 0, id, hashStruct)){
                        printf("failure: Error, LookUpUtilHigh FAILED in lookup\n");
                    }
                    return 1;
                
                }
                else if((sum == 1)&&(in == 0)&&(out == 0)){//ean lavame sum kai 7digit
                    if(!LookUpUtilHigh(0, 0, sum, id, hashStruct)){
                        printf("failure: In LookUpUtilHigh \n");
                    }
                    return 1;
                }
                else{//alliws
                    printf("failure: More than two arguments or less than two\n");
                    return 1;
                }
            }
            else{//an dn einai out out/in/sum oute 7pshfio
                printf("failure: Wrong parameter sequence\n");
                return 1;
            }
            token = strtok(NULL, " ");
        }
    }
    return 0;
}

int print(struct HashStruct** hashStruct, char* str){
    char* token;
    char* function;
    char command[strlen(str)+1];
    strncpy(command, str, (strlen(str)));
    if(command[strlen(str)-1] == '\n'){
        command[strlen(str)-1] = '\0';
    }
    else{
        command[strlen(str)] = '\0';
    }
    fflush( stdout );
    token = strtok(command, " ");
    function = "print";
    if(!strcmp(token, function)){
        if((strtok(NULL," ")) != NULL){//ean meta to print akolouthei kai allo token tote einai sfalma
            printf("failure: More than one argument\n");
            return 1;
        }
        if(!PrintGraph(hashStruct)){//alliws 
            printf("failure: In PrintGraph \n");
        }
        return 1;
    }
    return 0;
}