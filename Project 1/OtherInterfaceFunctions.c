#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "Structures_Defines.h"


int triangle(struct HashStruct** hashStruct, char* str){
    char* token;
    char* function;
    int token_length;
    int id;
    int node_position = 0;
    int k;
    int k_position = 0;
    
    char command[strlen(str)+1];
    strncpy(command, str, (strlen(str)));
    if(command[strlen(str)-1] == '\n'){
        command[strlen(str)-1] = '\0';
    }
    else{
        command[strlen(str)] = '\0';
    }
    fflush( stdout );
    token = strtok(command, " ");
    function = "triangle";
    if(!strcmp(token, function)){
        token = strtok(NULL, " ");
        if(token == NULL){
            printf("failure: No arguments at all\n");
            return 1;
        }
        while (token != NULL){
            token_length = strlen(token);
            if(token_length == 7){
                char digits[token_length];
                strncpy(digits, token, token_length);
                int i = 0;
                while(i<= token_length-1){//ean dn diavazei prwto argument 7digit node id
                    if(isdigit(digits[i]) == 0){
                        printf("failure: Not valid Node id\n");
                        return 1;
                    }
                    i++;
                }
                node_position++;
                if(node_position == 1){//molis diavasei enan krataei to id
                    id = atoi(token);
                }
            }
            else if((token_length >=0)&&(node_position == 1)){//ean diavase node id tote anamenei amount
                if((strtok(NULL, " "))!= NULL){//an meta to amount akolouthei kai allo argument
                    printf("failure: More than two arguments\n");
                    return 1;
                }
                char digits[token_length];
                strncpy(digits, token, token_length);
                int i = 0;
                while(i<= token_length-1){//elegxoume ean to amount token einai odws numeric
                    if(isdigit(digits[i]) == 0){//an dn einai prokeitai gia lathos edolh
                        printf("failure: Argument is not valid k\n");
                        return 1;
                    }
                    i++;
                }
                k_position = 1;
                k = atoi(token);
                if(!TriangleUtilHigh(hashStruct, id, k)){
                    printf("failure: In TriangleUtilHigh \n");
                }
                return 1;
            }
            else{//ean diavazoume kt allo apo node id ws prwto argument 
                printf("failure: Wrong parameter sequence\n");
                return 1;
            }
            token = strtok(NULL, " ");
        }
        if(k_position!= 1){//an telika dn lavame amount stn eisodo aneksarthta me to ti einai ta alla arguments
            printf("failure: Less than two arguments \n");
            return 1;
        }
        return 1;
    }
    return 0;    
}

int traceflow(struct HashStruct** hashStruct, char* str){//omoiws me triangle
    char* token;
    char* function;
    int token_length;
    int id;
    int node_position = 0;
    int l;
    int l_position = 0;
    
    char command[strlen(str)+1];
    strncpy(command, str, (strlen(str)));
    if(command[strlen(str)-1] == '\n'){
        command[strlen(str)-1] = '\0';
    }
    else{
        command[strlen(str)] = '\0';
    }
    fflush( stdout );
    token = strtok(command, " ");
    function = "traceflow";
    if(!strcmp(token, function)){
        token = strtok(NULL, " ");
        if(token == NULL){
            printf("failure: No arguments at all\n");
            return 1;;
        }
        while (token != NULL){
            token_length = strlen(token);
            if(token_length == 7){
                char digits[token_length];
                strncpy(digits, token, token_length);
                int i = 0;
                while(i<= token_length-1){
                    if(isdigit(digits[i]) == 0){
                        printf("failure: Not a valid Node id\n");
                        return 1;
                    }
                    i++;
                }
                node_position++;
                if(node_position == 1){
                    id = atoi(token);
                }
            }
            else if((token_length >=0)&&(node_position == 1)){
                if((strtok(NULL, " "))!= NULL){
                    printf("failure: More than two arguments\n");
                    return 1;
                }
                char digits[token_length];
                strncpy(digits, token, token_length);
                int i = 0;
                while(i<= token_length-1){
                    if(isdigit(digits[i]) == 0){
                        printf("failure: Not valid argument\n");
                        return 1;
                    }
                    i++;
                }
                l_position = 1;
                l = atoi(token);
                if(!TraceFlowUtilHigh(hashStruct, id, l)){
                    printf("failure: In TraceFlowUtilHigh \n");
                }
                return 1;
            }
            else{
                printf("failure: Wrong parameter sequence\n");
                return 1;
            }
            token = strtok(NULL, " ");
        }
        if(l_position!= 1){
            printf("failure: Less than two arguments \n");
            return 1;
        }
        return 1;
    }
    return 0;  
}

int allcycles(struct HashStruct** hashStruct, char* str){
    char* token;
    char* function;
    int token_length;
    int id;
    int node_position = 0;
    
    char command[strlen(str)+1];
    strncpy(command, str, (strlen(str)));
    if(command[strlen(str)-1] == '\n'){
        command[strlen(str)-1] = '\0';
    }
    else{
        command[strlen(str)] = '\0';
    }
    fflush( stdout );
    token = strtok(command, " ");
    function = "allcycles";
    if(!strcmp(token, function)){
        token = strtok(NULL, " ");
        if(token == NULL){
            printf("failure: No arguments at all\n");
            return 1;
        }
        while (token != NULL){
            token_length = strlen(token);
            if(token_length == 7){//ean diavasoume node id
                char digits[token_length];
                strncpy(digits, token, token_length);
                int i = 0;
                while(i<= token_length-1){
                    if(isdigit(digits[i]) == 0){//eksetazoume an einai eguro
                        printf("failure: Not valid Node id\n");
                        return 1;
                    }
                    i++;
                }
                node_position++;
                if((node_position == 1)&&(strtok(NULL, " ") == NULL)){//eam tlka einai eguro kai dn akolouthei allo token
                    id = atoi(token);
                    if(!AllCyclesUtilHigh(hashStruct, id)){
                        printf("failure: In AllCyclesUtilHigh \n");
                    }
                    return 1;
                }
            }
            else{
                printf("failure: Wrong parameter \n");
                return 1;
            }
        }
        if(node_position!= 1){
            printf("failure: Less than one argument \n");
            return 1;
        }
        return 1;
    }
    return 0;    
}

int bye(struct HashStruct** hashStruct, char* str){
    char* token;
    char* function;
    char command[strlen(str)+1];
    strncpy(command, str, (strlen(str)));
    if(command[strlen(str)-1] == '\n'){
        command[strlen(str)-1] = '\0';
    }
    else{
        command[strlen(str)] = '\0';
    }
    fflush( stdout );
    token = strtok(command, " ");
    function = "bye";
    if(!strcmp(token, function)){
        if((strtok(NULL," ")) != NULL){//an yparxei kai allo argument meta to "bye"
            printf("failure: More than one argument\n");
            return 1;
        }
        if(!EmptyGraph(hashStruct)){
            printf("failure: In EmptyGraph \n");
        }
        return 1;
    }
    return 0;
}

