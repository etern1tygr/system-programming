#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include "Structures_Defines.h"



int AddTranUtilHigh(struct HashStruct** hashStruct, int id1, int id2, double amount){
    if(id1 == id2){   //den bainei o idios komvos pali stn grafo
        printf("failure: id1 = id2 (%07d = %07d), in AddTranUtilHigh\n", id1, id2); 
        return 0;//
    }
    
    if((*hashStruct) == NULL){
        printf("failure: HashStruct is NULL in AddTranUtilHigh\n");
        return 0;
    }
    int table_index1 = id1%(*hashStruct)->size;
    int table_index2 = id2%(*hashStruct)->size;//psaxnoume an yparxoun oi komvoi sth grafo
    int check1 = SearchNodeInHashTableAndCollisionLists(hashStruct, id1);
    int check2 = SearchNodeInHashTableAndCollisionLists(hashStruct, id2);
    struct HashTableElement* cursorN1 = (*hashStruct)->table[table_index1];
    struct HashTableElement* cursorN2 = (*hashStruct)->table[table_index2];
    if((check1 >=0 )&&(check2 >=0)){//an yparxoun 
        while(cursorN1!=NULL){
            if(check1 == 0){//an einai sto hash table
                break;
            }
            
            cursorN1 = cursorN1->nextCollisionGraphNode;//alliws psakse sta collision list
            --check1;
        }
        
        while(cursorN2!=NULL){//omoiws gia tn allon komvo
            if(check2 == 0){
                break;
            }
            
            cursorN2 = cursorN2->nextCollisionGraphNode;
            --check2;
        }
        return AddTranUtilMid(cursorN1->GraphNode, cursorN2->GraphNode, amount);
    }
    else{//alliws dn yparxei kanenas tous h kapoios apo tous dyo, opote dn kanei addtran
        printf("failure: One or both Nodes do not exist in Graph\n");
        return 0;
    }
    printf("failure: Error in AddTranUtilHigh\n");
    return 0;
}

int AddTranUtilMid(struct Node* N1, struct Node* N2, double amount){//dhmiourgei akmh N1->N2
    if((N1 == NULL)||(N2 == NULL)){
        printf("failure: N1 is NULL or N2 is NULL in AddTranUtilMid\n");
        return 0;
    }
    //dhmiourgei tis lists an dn yparxoun
    OutLinkListCreate(N1);
    InLinkListCreate(N2);
   
    struct OutLinkElem* from = N1->OutlinkList->startOutLink;
    struct InLinkElem* to = N2->InlinkList->startInLink;
    
    struct Link* target = malloc(sizeof(struct Link));
    if(target == NULL){//apotyxia malloc
        printf("failure: malloc FAILED in AddTranUtilMid\n");
        exit(1);
    }
    //topothetoume amount kai deiktes gia pros kai apo komvo
    target->amount = amount;
    target->toNode = N2;
    target->fromNode = N1;
    
    if((from == NULL)||(to == NULL)){//dhmiourgei ta Elements gia ekastote list komvou ean einai adeies
        OutLinkElemCreate(N1, target);  
        InLinkElemCreate(N2, target);
        printf("success: ADDED transaction (N%07d, N%07d) with amount %f\n", N1->id, N2->id, amount);
        return 1;
    }
    else{//an dn einai adeies
        struct OutLinkElem* cursorN1 = N1->OutlinkList->startOutLink;        
        while(cursorN1!= NULL){//gia kathe Element           
            if((cursorN1->LinkToNextNode->toNode == N2)&&(cursorN1->LinkToNextNode->fromNode == N1)){//an deixnei stn akmh p theloume na dhmiourghsoume         
                cursorN1->LinkToNextNode->amount += amount;//shmainei oti yparxei hdh, ara kanei add to amount mono
                from = NULL;//free to malloc
                to = NULL;
                target->fromNode = NULL;
                target->toNode = NULL;
                free(target);
                target = NULL;
                cursorN1 = NULL;
                printf("success: ADDED transaction (N%d, N%d) with amount %f\n", N1->id, N2->id, amount);
                return 1;
            }  
            cursorN1 = cursorN1->nextOutLink;
        }
        cursorN1 = NULL;//an den yparxei h akmh se mh kenes lists, th dhmiourgei
        OutLinkElemCreate(N1, target);  
        InLinkElemCreate(N2, target);
        target = NULL;
        printf("success: ADDED transaction (N%07d, N%07d) with amount %f\n", N1->id, N2->id, amount);
        return 1;
    }//alliws error
    printf("failure: Error, transaction NOT CREATED OR FOUND in AddTranUtilMid\n");
    return 0;
}

int DelTranUtilHigh(struct HashStruct** hashStruct, int id1, int id2){
    if(id1 == id2){//omoiws dn kanei delete akmh metaksi idiou komvou
        printf("failure: id1 == id2 (%07d = %07d), in AddTranUtilHigh\n", id1, id2); 
        exit(1);
    }
    if((*hashStruct) == NULL){
        printf("failure: HashStruct is NULL in DelTranUtilHigh\n");
        exit(1);
    }
    int table_index1 = id1%(*hashStruct)->size;
    int table_index2 = id2%(*hashStruct)->size;//psaxnei tous komvous sto grafo
    int check1 = SearchNodeInHashTableAndCollisionLists(hashStruct, id1);
    int check2 = SearchNodeInHashTableAndCollisionLists(hashStruct, id2);
    struct HashTableElement* cursorN1 = (*hashStruct)->table[table_index1];
    struct HashTableElement* cursorN2 = (*hashStruct)->table[table_index2];
    if((check1 >=0 )&&(check2 >=0)){//omoiws me addtran
        while(cursorN1!=NULL){
            if(check1 == 0){
                break;
            }
            
            cursorN1 = cursorN1->nextCollisionGraphNode;
            --check1;
        }
        
        while(cursorN2!=NULL){
            if(check2 == 0){
                break;
            }
            
            cursorN2 = cursorN2->nextCollisionGraphNode;
            --check2;
        }
        return DelTranUtilMid(cursorN1->GraphNode, cursorN2->GraphNode);
    }
    else{//alliws den yparxoun komvoi wste na diagrapsei tn akmh
        printf("failure: One or both Nodes do not exist in Graph\n");
        return 0;
    }
    printf("failure: Error in DelTranUtilHigh\n");
    return 0; 
}

int DelTranUtilMid(struct Node* N1, struct Node* N2){//diagrafei tn akmh N1->N2
    
    if((N1 == NULL)||(N2 == NULL)||(N1->OutlinkList == NULL)||(N2->InlinkList == NULL)||(N1->OutlinkList->startOutLink == NULL)||(N2->InlinkList->startInLink == NULL)){
        printf("failure: N1 is NULL or N2 is NULL or N1's OutlinkList is NULL or N2's InlinkList  is NULL or startOutlink is NULL or startInlink is NULL in DelTranUtilMid\n");
        return 0;
    }
    struct OutLinkElem* cursorN1 = N1->OutlinkList->startOutLink;
    while(cursorN1!= NULL){//gia kathe element tou N1   
        if((cursorN1->LinkToNextNode->toNode == N2)&&(cursorN1->LinkToNextNode->fromNode == N1)){//an einai h akmh p theloume        
            DeleteOutLinkElemForN1(N1, N2, cursorN1->LinkToNextNode);//diagrafei to element ths apo tn pleyra tou N1
            cursorN1 = NULL;
            break;
        }
        cursorN1 = cursorN1->nextOutLink;
    }
    cursorN1 = NULL;
 
    struct InLinkElem* cursorN2 = N2->InlinkList->startInLink;  
    struct Link* cursorLinkN2 = NULL;
    while(cursorN2!= NULL){ //omoiws apo tn plevra tou N2           
        if((cursorN2->LinkToCurrentNode->toNode == N2)&&(cursorN2->LinkToCurrentNode->fromNode == N1)){         
            cursorLinkN2 = cursorN2->LinkToCurrentNode;
            DeleteInLinkElemForN2(N1, N2, cursorN2->LinkToCurrentNode);
            cursorLinkN2->fromNode = NULL;
            cursorLinkN2->toNode = NULL;
            free(cursorLinkN2);//diagrafei kai tn idia tn akmh telika
            cursorLinkN2 = NULL;
            printf("success: transaction (N%07d, N%07d) DELETED\n", N1->id, N2->id);
            cursorN2 = NULL;
            return 1;
        }  
        cursorN2 = cursorN2->nextInLink;   
    }
    cursorN2 = NULL;
    //alliws failure
    printf("failure: Error, transaction NOT DELETED in DelTranUtilMid\n");
    return 0; 
}  

int ConnUtilHigh(struct HashStruct** hashStruct, int id1, int id2){
    if(id1 == id2){//den vriskei conn metaksi idiou komvou
        printf("failure: id1 == id2 (%07d = %07d), in ConnUtilHigh\n", id1, id2); 
        return 0;
    }
    
    if((*hashStruct) == NULL){
        printf("failure: HashStruct is NULL in ConnUtilHigh\n");
        return 0;
    }
       int table_index1 = id1%(*hashStruct)->size;
    int table_index2 = id2%(*hashStruct)->size;//omoia psaxnei tous komvous stn grafo
    int check1 = SearchNodeInHashTableAndCollisionLists(hashStruct, id1);
    int check2 = SearchNodeInHashTableAndCollisionLists(hashStruct, id2);
    struct HashTableElement* cursorN1 = (*hashStruct)->table[table_index1];
    struct HashTableElement* cursorN2 = (*hashStruct)->table[table_index2];
    if((check1 >=0 )&&(check2 >=0)){
        while(cursorN1!=NULL){
            if(check1 == 0){
                break;
            }
            
            cursorN1 = cursorN1->nextCollisionGraphNode;
            --check1;
        }
        
        while(cursorN2!=NULL){
            if(check2 == 0){
                break;
            }
            
            cursorN2 = cursorN2->nextCollisionGraphNode;
            --check2;
        }
        return ConnUtilMid(cursorN1->GraphNode, cursorN2->GraphNode);
    }
    else{//alliws den iparxouyn komvoi wste na vrei to conn
        printf("failure: One or both Nodes do not exist in Graph\n");
        return 0;
    }
    printf("failure: Error in ConnUtilHigh\n");
    return 0; 
}

int ConnUtilMid(struct Node* N1, struct Node* N2){
    if((N1 == NULL)||(N2 == NULL)){
        printf("failure: N1 is NULL or N2 is NULL in ConnUtilMid\n");
        return 0;
    }
    else if((N1->OutlinkList!=NULL)&&(N1->OutlinkList->startOutLink == NULL)){
        printf("CRITICAL failure: N%07d startOutLink is NULL in ConnUtilMid\n", N1->id);
        exit(1);//den ginetai na yparxei list xwris arxiko element
    }
    else if(N1->OutlinkList == NULL){//an den yparxei list tote to con den vrethike
        printf("conn(N%07d,N%07d) not found, Outlinklist doesnt exist\n", N1->id, N2->id);
        return 1;
    }
    else{//alliws ean yparxei
        struct OutLinkElem* cursor = N1->OutlinkList->startOutLink;
        struct Node* N1stable = N1;
        if(ConnUtilBottom( N1, N2, cursor, N1stable)){
            printf(")\n");
            cursor = NULL;
            N1stable = NULL;
            return 1;
        }
        printf("\n");
        printf("conn(N%07d,N%07d) not found\n", N1->id, N2->id);
        cursor = NULL;
        N1stable = NULL;
        return 1;
    }
}

int ConnUtilBottom(struct Node* N1, struct Node* N2, struct OutLinkElem* cursor, struct Node* N1stable) { 
    if((N1 == NULL)||(N2 == NULL)){//
        cursor = NULL;
        return 0;
    } 
    
    if(N1->InlinkList!=NULL){//visited einai osoi tous exoume episkeftei k boroume na tous ksanaepiskeftoume
        N1->visited = 1;//tous markaroume gia na mn tous episkeftoume pali
    }
    
    if(N1 == N2){//ean ftasoume stn komvo stoxo, ektypwnodai ANAPODA oi komvoi
        printf("success: conn(N%07d, N%07d) = (N%d", N1stable->id, N2->id, N2->id);
        cursor = NULL;
        N1->visited = 0;
        return 1;
    }
    if((N1->OutlinkList == NULL)||(N1->OutlinkList->startOutLink == NULL)){
        cursor = NULL;//ean ftasoume se komvo p den odhgei se allous kaname lathos k epistrefoume xwris na ektypwsoume
        N1->visited = 0;
        return 0;
    }
    
    while(cursor!=NULL){ //gia kathe outlink element tou N
        if(cursor->LinkToNextNode->toNode->visited == 1){//ean o komvos metavashs einai markarismenos
            cursor = cursor->nextOutLink; //tote pame se element mexri na mh deixnei se mh markarismeno, gia apofygh kyklwn
            continue;
        }
        else if((cursor->LinkToNextNode->toNode->OutlinkList == NULL)||(cursor->LinkToNextNode->toNode->OutlinkList->startOutLink == NULL)){//ean o epomenos komvos dn exei oulinklist tote metavenoume se afton
            if(ConnUtilBottom(cursor->LinkToNextNode->toNode, N2, cursor, N1stable)){//omws o cursor paramenei statheros tn komvos p eimaste hdh
                printf(", N%07d", N1->id);
                N1->visited = 0;
                cursor = NULL;
                return 1;
            }
        }
        else if(ConnUtilBottom(cursor->LinkToNextNode->toNode, N2, cursor->LinkToNextNode->toNode->OutlinkList->startOutLink, N1stable)){//ean exei Outlink list o komvos metavasis k dn einai markarismenos, metavenoume se afton
            printf(", N%07d", N1->id);//allazodas kai tn cursor na anaferetai sth lista tou komvou metavashs
            cursor = NULL;
            N1->visited = 0;
            return 1;         
        }
        cursor = cursor->nextOutLink;
    }
    N1->visited = 0;
    return 0;
}