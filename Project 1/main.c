
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "Structures_Defines.h"

int main(int argc, char** argv){
   int HashEntries = 0;

   FILE* f = NULL;
   TerminalArg(&f, &HashEntries, argc, argv);//kathorizei ta arguments apo to terminal

   struct HashStruct* hashStruct = NULL;

   if(!CreateHashStruct(&hashStruct, HashEntries)){//dhmiourgoume to HashStruct 
       printf("failure: Error, CreateHashStruct FAILED in main\n");
       return 0;
   }
   char str[8000];
   while(1){

       if(f != NULL){//ean exei anoiksei arxeio
           if(fgets(str, sizeof(str), f) == NULL){//diavazoume grammh grammh ews otou EOF
               fclose(f);//kleinoume to arxeio se periptwsh eof
               DeleteHashStruct(&hashStruct);//katastrefoume to hashstruct
               f = NULL;
               return 0;
           }
       }
       else{//alliws diavazoume apo grammh edolwn
          if(fgets(str, sizeof(str), stdin) == NULL){//pali eows eof
              DeleteHashStruct(&hashStruct);//opou kai katastrefoume to hashstruct
              return 0;
          }
       }

       //eksetazoume gia poia synarthsh prokeitai, alliws lathos synarthsh kai epistrefoume stn fgets wste na eisagoume neo input
       if(createnodes(&hashStruct, str)){
           printf("\n");
           fflush(stdout);
           continue;  
       }

       else if(delnodes(&hashStruct, str)){
           printf("\n");
           fflush(stdout);
           continue;
       }
       else if(addtran(&hashStruct, str)){
           printf("\n");
           fflush(stdout);
           continue;
       }
       else if(deltran(&hashStruct, str)){
           printf("\n");
           fflush(stdout);
           continue;
        
       }
       else if(lookup(&hashStruct, str)){
           printf("\n");
           fflush(stdout);
           continue;
       }
       else if(triangle(&hashStruct, str)){
           printf("\n");
           fflush(stdout);
           continue;
        
       } 
       else if(print(&hashStruct, str)){
           printf("\n");
           fflush(stdout);
           continue;
       }
       else if(traceflow(&hashStruct, str)){
           printf("\n");
           fflush(stdout);
           continue;
       }
       else if(allcycles(&hashStruct, str)){
           printf("\n");
           fflush(stdout);
           continue;
       }
       else if(conn(&hashStruct, str)){
           printf("\n");
           fflush(stdout);
           continue;
       }
       else if(bye(&hashStruct, str)){
           printf("\n");
           fflush(stdout);
           continue;
       }
       else{
           printf("Wrong Function or action, try again\n");
           printf("\n");
           fflush(stdout);
       }
   }
}