#include <stdio.h>
#include <stdlib.h>
#include "Structures_Defines.h"



int CreateCollisionElement(struct HashTableElement* prevElement, int id){//dhmiourgei enan HashTableElement otan exoume collision
    if((prevElement == NULL)||(prevElement->GraphNode == NULL)){
        printf("failure: Last ELement or Element's pointer to N%07d is NULL in Collision List in CreateCollisionElement\n", id);
        return 0;
    }
    struct HashTableElement* newHashTableElement = malloc(sizeof(struct HashTableElement)); 
    if(newHashTableElement == NULL){
        printf("failure: malloc failed in CreateCollisionElement\n");
        exit(1);
    }
    newHashTableElement->GraphNode = NULL;//to neo element dn deixnei kapou gia tn wra
    newHashTableElement->id = id;//exei tou komvou pou tha eisaxthei metepeita]
    newHashTableElement->key = prevElement->key;//exei to key toy head hashtable element
    newHashTableElement->nextCollisionGraphNode = NULL;//to epomeno collision element einai null
    prevElement->nextCollisionGraphNode = newHashTableElement;//to prohgoumeno deixnei sto neo
    newHashTableElement = NULL;
    return 1;
}

int DeleteCollisionElement(struct HashTableElement** tableElement, int stepsToCollisionNode){//Diagrafei to  collision element
    if((*tableElement) == NULL){
        printf("failure: Table Element is NULL in DeleteCollisionElement\n");
        return 0;
    }
    
    if(stepsToCollisionNode < 0){//den ginetai na exoume arnhtiko arithmo vhmatwn apo to head table element pros to collision element
        return -1;
    }
    int steps = 0;
    struct HashTableElement* tobedeleted = *tableElement;
    while((*tableElement)->nextCollisionGraphNode!= NULL){//oso to epomeno collision element den einai null
        
        tobedeleted = (*tableElement)->nextCollisionGraphNode;//ftanoume sto prohgoymeno element apo afto p theloume na diagrapsoume, kai afto p tha diagrafei einai to epomenos
        if(steps == stepsToCollisionNode - 1){//afto symvainei otan..
     
            if(tobedeleted->GraphNode->InlinkList != NULL || tobedeleted->GraphNode->OutlinkList != NULL){
                printf("failure: N outlinks or linklinks exist! \n");                
                return 0;//an exei akmes
            }
            (*tableElement)->nextCollisionGraphNode = tobedeleted->nextCollisionGraphNode;//an den exei to epomeno collision element tou kersora, einai to epomeno tou deleted
            (*tableElement) = tobedeleted;//thetoume poio tha ginei delete telika
            tobedeleted = NULL;
            return 1;
        }
        steps++;
        (*tableElement) = (*tableElement)->nextCollisionGraphNode;
    }
    return 0;
}

int CreateHashStruct(struct HashStruct** hashStruct, int HashEntries){//dhmiourgei to hashtable me ola ta elements
    if((((*hashStruct) = malloc(sizeof(struct HashStruct)))== NULL)||(HashEntries == 0)){
        printf("failure: HashStruct malloc FAILED or zero HashEntries in CreateHashStruct\n");
        exit(1);
    }
    (*hashStruct)->size = HashEntries;
    if(((*hashStruct)->table = malloc(sizeof(struct HashTableElement*)*HashEntries)) == NULL){
        printf("failure: HashTable malloc FAILED in CreateHashStruct\n");
        exit(1);
    }
    int i;
    for(i=0; i<HashEntries; i++){//gia kathe hash element
        if(((*hashStruct)->table[i] = malloc(sizeof(struct HashTableElement)))== NULL){
            printf("failure: HashTable[%d] malloc FAILED in CreateHashStruct\n", i);
            exit(1);
        }
        (*hashStruct)->table[i]->nextCollisionGraphNode = NULL;//den exei arxika collision lisy
        (*hashStruct)->table[i]->GraphNode = NULL;//dn deixnei se komvo
        (*hashStruct)->table[i]->id = -1;//den exei id
        (*hashStruct)->table[i]->key = -1;//oute kleidi(id%size)       
    }
    printf("HashStruct and its components CREATED in CreateHashStruct\n");
    return 1;
}

int FillHashTableElem(struct HashStruct** hashStruct, int id){//gemizei to hash element me enan komvo grafou
    if((*hashStruct) == NULL){
        printf("failure: hashStruct for N%07d is NULL in FillHashTableElem\n", id);
        return 0;
    }
    int check = SearchNodeInHashTableAndCollisionLists(hashStruct, id);//psaxnei na vrei ean yparxei komvos me id 
    if(check == 0){ // found in head
        printf("failure: found in head, Will NOT FILL HashTableElem with N%07d in CreateHashTableElem\n", id);
        return 0;
    }
    else if(check >= 1){ // found in collision list
        printf("failure: found in collision list, Will NOT FILL HashTableElem with N%07d in CreateHashTableElem\n", id);
        return 0;
    }
    else if(check == -1){ // error
        printf("failure: Error, Will NOT FILL HashTableElem with N%07d in CreateHashTableElem\n", id);
        return 0;
    }
    //ean ton vrei
    int table_index = id%((*hashStruct)->size);
    struct HashTableElement* cursor = (*hashStruct)->table[table_index];
    if((*hashStruct)->table[table_index]->id != -1){ // ean einai element p exei hdh komvo stn yparxousa thesh me table[i]->id kai p theloume na valoume tn neo, exoume collision safto to keli
        while(cursor->nextCollisionGraphNode!= NULL){//ftase sto last collision element ean exei collision list
            cursor = cursor->nextCollisionGraphNode;
        }//ean exei hdh element stn collision list, o cursor einai sto last collision element, alliws sto table[i], dld to head ths collision list otan einai praktika kenh
        if(!CreateCollisionElement(cursor, id)){
            cursor = NULL;
            printf("failure: Will NOT FILL HashTableElem(no Collision Detection) with N%07d in CreateHashTableElem\n", id);
            return 0;
        }
        cursor = cursor->nextCollisionGraphNode;
    } 
    else{//alliws an dn exei hdh komvo, tote dn exoume collision opote eisagoume to neo id sto element
        (*hashStruct)->table[table_index]->id = id;
    }
    //se kathe periptwsh ftiaxnoume tn komvo
    if(CreateNodeUtilHigh(id, cursor, (*hashStruct)->size)){
        cursor = NULL;
        return 1;
    }
    printf("failure: Error, COULD NOT FILL HashTableElem with N%07d in CreateHashTableElem\n", id);
    cursor = NULL;
    return 0;
}

int UnFillHashTableElem(struct HashStruct** hashStruct, int id){//adeiazei to table element apo tn komvo grafou
    if((*hashStruct) == NULL){
        printf("failure: hashStruct for N%07d is NULL in DeleteHashTableElem\n", id);
        return 0;
    }
    
    if(id == -1){//mono ean to table element exei komvo(id -1 shmainei dn einai keno to element)
        printf("N's id (id = %07d) is -1 in DeleteHashTableElem\n", id);
        return 0;
    }
    int stepsToCollisionNode;
    int collision_flag = 0;
    int table_index = id%((*hashStruct)->size);
    struct HashTableElement* cursor = (*hashStruct)->table[table_index];//psaxnoume tn komvo sta hash elements
    if((stepsToCollisionNode = SearchNodeInHashTableAndCollisionLists(hashStruct, id))== -1){//error
        printf("failure: Error, HashTableElement NOT UNFILLED from N%07d in DeleteHashTableElem\n", id);
        return 0;
    } 
    else if(stepsToCollisionNode >= 1){//an yparxoun steps, vrisketai se collision list
        collision_flag = 1;//ean vrethei se collision list tn diagrafoume ean dn exei akmes
        int check = DeleteCollisionElement(&cursor, stepsToCollisionNode);
        if(check<0){
            printf("failure: HashTableElement NOT UNFILLED from N%07d in DeleteHashTableElem\n", id);
            return 0;
        }
        else if(check == 0){
            printf("failure: N%07d outlinks or inlinks exist, did NOT delete! \n", id);                
            return 0;
        }
    }
    else if(stepsToCollisionNode == -2){//not found, an dn vrethei pouthena sto hashtable
        printf("failure: Nothing to Delete, N%07d already deleted or doesnt exist\n", id);
        return 0;
    }
    //alliws ean ta collision steps einai 0, shmainei oti vrisketai stn kefalh(p dn tn diagrafoume, diagrafoume to hash element mono ean einai collision element)
    if((collision_flag == 0)&&(cursor->GraphNode->InlinkList != NULL || cursor->GraphNode->OutlinkList != NULL)) {
        printf("failure: N%07d outlinks or inlinks exist, did NOT delete! \n", id);                
        return 0;
    }
    //se kathe pwriptwsh diagrafete o komvos kai arxikopoioudai ta periexomena tou element
    if(DeleteNodeUtilHigh(id, cursor)){//!=
        cursor->id = cursor->GraphNode->id;
        free(cursor->GraphNode);
        cursor->GraphNode = NULL;
        if(collision_flag){//an malista prokeitai gia collision element diagrafetai kai afto
            free(cursor);
        }
        cursor = NULL;
        printf("success: DELETED N%07d\n", id);
        return 1;
    }
    printf("failure: Error, HashTableElement NOT UNFILLED from N%07d in DeleteHashTableElem\n", id);
    return 0;
}

int SearchNodeInHashTableAndCollisionLists(struct HashStruct** hashStruct, int id){//sarwnei to hashtable element gia komvous grafou
    int table_index = id%(*hashStruct)->size;
    if(((*hashStruct) == NULL)||((*hashStruct)->table == NULL)){
        printf("N%07d, hashStruct or its table is NULL in SearchNodeInHashTableAndCollisionLists\n", id);
        return -1;
    }
    
    if(((*hashStruct)->table[table_index]!= NULL)&&
            (((*hashStruct)->table[table_index]->key) == (table_index)&&
            ((*hashStruct)->table[table_index]->id == id))){//an yparxei mesa stis kefales toy hashtable element
        return 0;
    }
    else if(((*hashStruct)->table[table_index]!= NULL)&&(((*hashStruct)->table[table_index]->key) == table_index)
            &&((*hashStruct)->table[table_index]->id != id)){//an yparxei se collision list, epistrefei poso apexei apo tn kefali
        struct HashTableElement* cursor = (*hashStruct)->table[table_index];
        int steps = 0;
        while(cursor!= NULL){
            if(cursor->id == id){
                cursor = NULL;
                return steps;
            }
            cursor = cursor->nextCollisionGraphNode;
            steps++;
        }
    }
    return -2;
}

int EmptyGraph(struct HashStruct** hashStruct){//adeiazei ta hash table elements apo komvous grafou
    
   if(((*hashStruct) == NULL)||((*hashStruct)->table == NULL)){
        printf("failure: hashStruct or its table is NULL in EmptyGraph\n");
        return 0;
   }
   int i;
   for(i=0; i<(*hashStruct)->size; i++){//gia kathe table element
        if((*hashStruct)->table[i] == NULL){//den prepei na einai null efoson exoume dhmiourghsei hash struct
            printf("failure: Table[%d] is NULL in EmptyGraph\n", i);
            return 0;
        }
        struct HashTableElement* cursorCollision = (*hashStruct)->table[i]->nextCollisionGraphNode;
        struct HashTableElement* tmp = cursorCollision;
        while(tmp!= NULL){//gia kathe collision element ksekinodas apthn arxh(to head ths list dn einai collision element)
            
            DeleteNodeUtilHigh(cursorCollision->id, cursorCollision);//arxikopoihsh komvou pou tha diagrafei metepeita
            tmp = tmp->nextCollisionGraphNode;
            (*hashStruct)->table[i]->nextCollisionGraphNode = cursorCollision->nextCollisionGraphNode; //thetoume neo epomeno collision element
            cursorCollision->id = cursorCollision->GraphNode->id;
            free(cursorCollision->GraphNode);//free tn komvo tou element telikws
            cursorCollision->GraphNode = NULL;
            free(cursorCollision);//free kai to element
            cursorCollision = tmp;
            
        }
        if((*hashStruct)->table[i]->GraphNode == NULL){//alliws an dn yparxei collision element kai to head d exei komvo, proxwrame xwris delete node
            continue;
        }
        DeleteNodeUtilHigh((*hashStruct)->table[i]->GraphNode->id, (*hashStruct)->table[i]);//alliws diagrafh tou komvou sto head etc
        (*hashStruct)->table[i]->id = (*hashStruct)->table[i]->GraphNode->id;
        free((*hashStruct)->table[i]->GraphNode);
        (*hashStruct)->table[i]->GraphNode = NULL;
    }
    printf("success: cleaned memory\n");
    return 1;
}

int DeleteHashStruct(struct HashStruct** hashStruct){
    
      
    if(((*hashStruct) == NULL)||((*hashStruct)->table == NULL)){
        printf("failure: hashStruct or its table is NULL in DeleteHashStruct\n");
        return 0;;
    }
     
    if(EmptyGraph(hashStruct)){//adeiazoume to hashtable apo tous komvous, dld katastrofh grafou
        int i;
        for(i=0; i<(*hashStruct)->size; i++){//kai free ola ta hash elements(dld ta heads kathe collision list)
            free((*hashStruct)->table[i]);
            (*hashStruct)->table[i] = NULL;
        }
        free((*hashStruct)->table);//kane free telika to table kai to struct
        (*hashStruct)->table = NULL;
        free((*hashStruct));
        (*hashStruct) = NULL;
        printf("HashStruct DELETED in DeleteHashStruct\n");
        return 1;
    }
    printf("failure: Error, HashStruct NOT DELETED in DeleteHashStruct\n");
    return 0;
}

int PrintGraph(struct HashStruct** hashStruct){
    if(((*hashStruct) == NULL)||((*hashStruct)->table == NULL)){
        printf("failure: hashStruct or its table is NULL in PrintGraph\n");
        return 0;
    }
    int i;
    struct HashTableElement* cursorCollision;
    for(i=0; i<(*hashStruct)->size; i++){//gia kathe head element/hashtable element
        if((*hashStruct)->table[i] == NULL){
            printf("failure: Table[%d] is NULL in PrintGraph\n", i);
            return 0;
        } 

        cursorCollision = (*hashStruct)->table[i];
        while(cursorCollision!= NULL){//ektypwnei prwta ta collision elements kathe listas
            if(cursorCollision->GraphNode == NULL){//ean den yparxei komvos gia to head element, proxwrame sto epomeno tou collision element opou 
                cursorCollision = cursorCollision->nextCollisionGraphNode;// an yparxei tha deixnei se komvo efoson mono tote ginetai create
                continue;                         
            }
            printf("vertex(N%07d) = ", cursorCollision->GraphNode->id);
            if(!PrintNode(cursorCollision->GraphNode)){
                printf("failure: Error in PrintGraph\n");
                return 0;
            }
            printf("\n");
            cursorCollision =  cursorCollision->nextCollisionGraphNode;
        }
        
    }
    cursorCollision = NULL;
    printf("Graph PRINTED in PrintGraph\n");
    return 1;
}