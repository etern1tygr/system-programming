#include <stdio.h>
#include <stdlib.h>
#include "Structures_Defines.h"

int TraceFlowUtilHigh(struct HashStruct** hashStruct, int id, int path_length){
    
    if((*hashStruct) == NULL){
        printf("failure: HashStruct is NULL in TraceFlowUtilHigh\n");
        return 0;
    }
    int table_index = id%((*hashStruct)->size);
    int check1 = SearchNodeInHashTableAndCollisionLists(hashStruct, id);
    struct HashTableElement* cursorN1 = (*hashStruct)->table[table_index];
    if(check1 >=0){//an yparxei o komvos
        while(cursorN1!=NULL){
            if(check1 == 0){
                break;
            }
            
            cursorN1 = cursorN1->nextCollisionGraphNode;
            --check1;
        }
        return TraceFlowUtilMid(cursorN1->GraphNode, path_length);
    }
    else{//alliws ean dn yparxei o komvos
        printf("failure: N%07d doesnt exist in Graph\n", id);
        return 0;
    }
    printf("failure: Error for N%07d in TraceFlowUtilHigh\n", id);
    return 0;  
}

int TraceFlowUtilMid(struct Node* N, int path_length){
    if(N == NULL){
        printf("failure: N is NULL in TraceFlowUtilMid\n");
        return 0;
    }
    else if((N->OutlinkList!=NULL)&&(N->OutlinkList->startOutLink == NULL)){
        printf("CRITICAL failure: N%07d startOutLink is NULL in TraceFlowUtilMid\n", N->id);
        exit(1);//dn borei na yparxei outlink list xwris head
    }
    else if(N->OutlinkList == NULL){//adn dn exei list
        printf("traceflow(N%07d, %d) not found, Outlinklist doesnt exist)\n", N->id, path_length);
        return 1;
    }
    else if(path_length == 0){//an prokeitai gia monopati mhdenikou mhkous
        printf("traceflow(N%07d, %d) not found, path_length is zero)\n", N->id, path_length);
        return 1;
    }
    else{//alliws
        struct OutLinkElem* cursor = N->OutlinkList->startOutLink;
        struct Node* Nstable = N;
        double summit = 0;
        int found = 0;
        int array[path_length+1];
        int i;
        for(i=0; i<=path_length; i++){
            array[i] = -1;
        }
        i=0;
        TraceFlowUtilBottom(array, i, N, path_length, path_length, cursor, Nstable, summit, &found);
        if(!found){
            printf("traceflow(N%07d, %d) not found\n", N->id, path_length);
        }
        cursor = NULL;
        Nstable = NULL;
        return 1; 
    }  
}

int TraceFlowUtilBottom(int* array, int i, struct Node* N, int path_length, int pstable, struct OutLinkElem* cursor, struct Node* Nstable, double summit, int* found){
    if(N == NULL){
        cursor = NULL;
        return 0;
    }
    array[i] = N->id;
    
    if(N->InlinkList!=NULL){//an boroume na ftasoume se afton kai exoume ftasei gia 1h fora, markare ton
        N->visited = 1;
    }

    if(path_length == 0 ){//an ftasame sto epithymhto mhkos
        printf("success: traceflow(N%07d, %d) = \n", Nstable->id, pstable);
        int j;
        printf("(");
        for(j=0; j<=pstable; j++){
            printf("N%d, ", array[j]);
        }
        array[pstable] = -1;
        printf("%f)\n", summit);
        *found = 1;
        cursor = NULL;
        array[i] = -1;
        N->visited = 0;
        return 1;
    }
    
    if((N->OutlinkList == NULL)||(N->OutlinkList->startOutLink == NULL)){
        cursor = NULL;
        array[i] = -1;
        N->visited = 0;
        return 0;
    }
    
   
    while(cursor!=NULL){//gia kathe oulink element tou komvou
        
        if(cursor->LinkToNextNode->toNode->visited == 1){//ean tn exoume episkeftei hdh tn epomeno komvo, gia apofygh kyklwn
            cursor = cursor->nextOutLink;
            continue;
        }     
        else if((cursor->LinkToNextNode->toNode->OutlinkList == NULL)||(cursor->LinkToNextNode->toNode->OutlinkList->startOutLink == NULL)){
            TraceFlowUtilBottom(array, i+1, cursor->LinkToNextNode->toNode, path_length-1, pstable, cursor, Nstable, cursor->LinkToNextNode->amount+summit, found);//an dn exei ekswterikes akmes, kratame stathero ton cursor stn previous node   
        }
        else{
            //alliws allazoume kai tn cursor na anaferetai sth list tou komvou metavashs
            TraceFlowUtilBottom(array, i+1, cursor->LinkToNextNode->toNode, path_length-1, pstable, cursor->LinkToNextNode->toNode->OutlinkList->startOutLink, Nstable, cursor->LinkToNextNode->amount+summit, found);
        }
        cursor = cursor->nextOutLink;
    }
    array[i] = -1;
    N->visited = 0;
    return 1;
}

int TriangleUtilHigh(struct HashStruct** hashStruct, int id, int k){
    
    if((*hashStruct) == NULL){
        printf("HashStruct for N%07d is NULL  in TriangleUtilHigh\n", id);
        return 0;
    }
    int table_index = id%(*hashStruct)->size;//omoiws psaxnei ean yparxei o komvos stn grafo
    int check1 = SearchNodeInHashTableAndCollisionLists(hashStruct, id);
    struct HashTableElement* cursorN1 = (*hashStruct)->table[table_index];
    if(check1 >=0 ){
        while(cursorN1!=NULL){
            if(check1 == 0){
                break;
            }
            
            cursorN1 = cursorN1->nextCollisionGraphNode;
            --check1;
        }
        return TriangleUtilMid(cursorN1->GraphNode, k);
    }  
    else{//alliws ean dn yparxei o komvos
        printf("failure: N%07d doesnt exist in Graph\n", id);
        return 0;
    }
    printf("Error for N%07d in TriangleUtilHigh\n", id);
    return 0;  
}

int TriangleUtilMid(struct Node* N, int k){
    if(N == NULL){
        printf("failure: N is NULL in TriangleUtilMid\n");
        return 0;
    }
    else if((N->OutlinkList!=NULL)&&(N->OutlinkList->startOutLink == NULL)){
        printf("CRITICAL failure: N%07d startOutLink is NULL in TriangleUtilMid\n", N->id);
        exit(1);
    }
    else if(N->OutlinkList == NULL){//an dn yparxei list outlinks tote..
        printf("triangle(N%07d, %d) not found\n", N->id, k);
        return 1;
    }
    else{
        int found = 0;
        struct OutLinkElem* cursor1 = N->OutlinkList->startOutLink;
        struct Link* cursorLink1 = cursor1->LinkToNextNode;
        struct Node* cursorNode1 = cursorLink1->toNode;
        while(cursor1!=NULL){ //gia kathe akmh p odhgei se epomeno komvi
            if((cursorNode1->OutlinkList != NULL)&&(cursorNode1->OutlinkList->startOutLink != NULL)&&(cursorLink1->amount>= k)&&(cursorNode1 != N)){
                // an o epomenos komvos exei oulinklist mh kenh kai h akmh p ftanei se afton exei amount>k kai o komvos dn einai o idios me tn prohgoumeno(dn kanei kyklo)
                //tote phgaine stn epomeno
                struct OutLinkElem* cursor2 = cursorNode1->OutlinkList->startOutLink;
                struct Link* cursorLink2 = cursor2->LinkToNextNode;
                struct Node* cursorNode2 = cursorLink2->toNode;
                while(cursor2!= NULL){
                    if((cursorNode2->OutlinkList != NULL)&&(cursorNode2->OutlinkList->startOutLink != NULL)&&(cursorLink2->amount>= k)&&(cursorNode2 != N)&&(cursorNode2 != cursorNode1)){  
                        // an o epomenos komvos exei oulinklist mh kenh kai h akmh p ftanei se afton exei amount>k kai o komvos dn einai o idios me tous prohgoumenous(dn kanei kyklo)
                        //tote phgaine stn epomeno
                        struct OutLinkElem* cursor3 = cursorNode2->OutlinkList->startOutLink;
                        struct Link* cursorLink3 = cursor3->LinkToNextNode;
                        struct Node* cursorNode3 = cursorLink3->toNode;
                        while(cursor3!= NULL){
                            if((cursorNode3 == N)&&(cursorLink3->amount>= k)&&(cursorNode3 != cursorNode2)&&(cursorNode3 != cursorNode1)){
                                // an o komvos exei akmh p ftanei se epomeno me amount>k kai o komvos dn einai o idios me tous prohgoumenous(dn kanei kyklo)
                                // alla kai an aftos o epomenos einai o idios me tn arxiko, tote phgaine stn epomeno, dld ekei p ksekinhsame
                                if(found){
                                    printf("(N%07d, N%07d, N%07d)\n",N->id, cursorNode1->id, cursorNode2->id);

                                }
                                else{
                                    found  = 1;
                                    printf("success: triangle(N%07d, %d)\n", N->id, k);
                                    printf("(N%07d, N%07d, N%07d)\n", N->id, cursorNode1->id, cursorNode2->id);
                                }
                            }
                            cursor3 = cursor3->nextOutLink;//alliws stn epomeno 
                            if(cursor3 != NULL){//an einai NULL dn yoarxoun epomenes akmes/komvoi
                                cursorLink3 = cursor3->LinkToNextNode;
                                cursorNode3 = cursorLink3->toNode;   
                            }
                        } 
                    }
                    cursor2 = cursor2->nextOutLink;//omoiws
                    if(cursor2 != NULL){
                        cursorLink2 = cursor2->LinkToNextNode;
                        cursorNode2 = cursorLink2->toNode;
                    }
                }
            }
            cursor1 = cursor1->nextOutLink;//omoiws
            if(cursor1 != NULL){
                cursorLink1 = cursor1->LinkToNextNode;
                cursorNode1 = cursorLink1->toNode;
            }
        }
        if(!found){//an dn vrethike 
            printf("triangle(N%07d, %d) not found\n", N->id, k);
        }
        return 1;
    }
    printf("failure: Error for N%07d in TriangleUtilMid\n", N->id);
    return 0;
}

int AllCyclesUtilHigh(struct HashStruct** hashStruct, int id){
    
    if((*hashStruct) == NULL){
        printf("failure: HashStruct is NULL in AllCyclesUtilHigh\n");
        return 0;
    }
    int table_index = id%((*hashStruct)->size);
    int check1 = SearchNodeInHashTableAndCollisionLists(hashStruct, id);
    struct HashTableElement* cursorN1 = (*hashStruct)->table[table_index];
    if(check1 >=0){//an yparxei o komvos
        while(cursorN1!=NULL){
            if(check1 == 0){
                break;
            }
            
            cursorN1 = cursorN1->nextCollisionGraphNode;
            --check1;
        }
        return AllCyclesUtilMid(cursorN1->GraphNode);
    }
    else{//alliws ean dn yparxei o komvos
        printf("failure: N%07d doesnt exist in Graph\n", id);
        return 0;
    }
    printf("failure: Error for N%07d in AllCyclesUtilHigh\n", id);
    return 0;  
}

int AllCyclesUtilMid(struct Node* N){
    
     if(N == NULL){
        printf("failure: N is NULL in AllCyclesUtilMid\n");
        return 0;
    }
    else if((N->OutlinkList!=NULL)&&(N->OutlinkList->startOutLink == NULL)){
        printf("CRITICAL failure: N%07d startOutLink is NULL in AllCyclesUtilMid\n", N->id);
        exit(1);//dn borei na yparxei outlink list xwris head
    }
    else if(N->OutlinkList == NULL){//adn dn exei list
        printf("cycles(N%07d) not found, Outlinklist doesnt exist\n", N->id);
        return 1;
    }
    else{//alliws
        struct OutLinkElem* cursor = N->OutlinkList->startOutLink;
        struct Node* Nstable = N;
        int found = 0;
        int path_length = 2;
        
        AllCyclesUtilBottom(N, path_length, cursor, Nstable, &found);
        if(!found){
            printf("cycles(N%07d) not found\n", N->id);
        }
        cursor = NULL;
        Nstable = NULL;
        return 1; 
    }  
}

int AllCyclesUtilBottom(struct Node* N, int path_length, struct OutLinkElem* cursor, struct Node* Nstable, int* found){
    
    if(N == NULL){
        cursor = NULL;
        return 0;
    }

    if(N->InlinkList!=NULL){//an boroume na ftasoume se afton kai exoume ftasei gia 1h fora, markare ton
        N->visited = 1;
    }

    if((path_length == 0 )&&(N == Nstable)){//an ftasame stn komvo afethria, exoyme kyklo me ligoterous apo 3 komvous
        cursor = NULL;
        return 0;
    }
    
    if((N == Nstable)&&(path_length != 2)){//an ftasame stn komvo afethria
        cursor = NULL;
        *found = 1;
        printf("success: cycles(N%07d) \n", N->id);
        return 1;
    }
    
    
    if((N->OutlinkList == NULL)||(N->OutlinkList->startOutLink == NULL)){
        cursor = NULL;
        N->visited = 0;
        return 0;
    }

    while(cursor!=NULL){//gia kathe oulink element tou komvou
        
        if((cursor->LinkToNextNode->toNode->visited == 1)&&(cursor->LinkToNextNode->toNode != Nstable)){//ean tn exoume episkeftei hdh tn epomeno komvo, gia apofygh kyklwn
            cursor = cursor->nextOutLink;
            continue;
        }     
        else if((cursor->LinkToNextNode->toNode->OutlinkList == NULL)||(cursor->LinkToNextNode->toNode->OutlinkList->startOutLink == NULL)){
            AllCyclesUtilBottom(cursor->LinkToNextNode->toNode, path_length-1, cursor, Nstable, found);//an dn exei ekswterikes akmes, kratame stathero ton cursor stn previous node   
        }
        else{
            //alliws allazoume kai tn cursor na anaferetai sth list tou komvou metavashs
            AllCyclesUtilBottom(cursor->LinkToNextNode->toNode, path_length-1, cursor->LinkToNextNode->toNode->OutlinkList->startOutLink, Nstable, found);
        }
        cursor = cursor->nextOutLink;   
    }
    N->visited = 0;
    return 1;
}