#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "Structures_Defines.h"


int addtran(struct HashStruct** hashStruct, char* str){
    char* token;
    char* function;
    int token_length;
    int id1;
    int id2;
    int amount;
    int node_position = 0;
    int amount_position = 0;
    char command[strlen(str)+1];
    strncpy(command, str, (strlen(str)));
    if(command[strlen(str)-1] == '\n'){
        command[strlen(str)-1] = '\0';
    }
    else{
        command[strlen(str)] = '\0';
    }
    fflush( stdout );
    token = strtok(command, " ");
    function = "addtran";
    if(!strcmp(token, function)){
        token = strtok(NULL, " ");
        if(token == NULL){
            printf("failure: No arguments at all\n");
            return 1;
        }
        while (token != NULL){            
            token_length = strlen(token);
            if(token_length == 7){//ean diavasoume node id 7pshfiwn
                char digits[token_length];
                strncpy(digits, token, token_length);
                int i = 0;
                while(i<= token_length-1){
                    if(isdigit(digits[i]) == 0){//eksetazoume ean einai egyros
                        printf("failure: Not valid Node id\n");
                        return 1;
                    }
                    i++;
                }
                node_position++;
                if(node_position == 1){
                    id1 = atoi(token);
                }
                if(node_position == 2){//ean diavazoume 2 komvous, topothetoume ta id's tous adistoixa
                    id2 = atoi(token);
                }
            }
            else if((token_length >=0)&&(node_position == 2)){//ean diavasame 2 node id's kai twra diavazoume amount
                char digits[token_length];
                strncpy(digits, token, token_length);
                int i = 0;
                while(i<= token_length-1){//eksetazoume ean einai egyro
                    if(isdigit(digits[i]) == 0){
                        printf("failure: Not valid amount\n");
                        return 1;
                    }
                    i++;
                }
                amount_position = 3;
                amount = atoi(token);  
                if(strtok(NULL, " ")!= NULL){//ean meta to amount akolouthei kiallo argument
                    printf("failure: Wrong number of arguments\n");
                    return 1;
                }
                if(!AddTranUtilHigh(hashStruct, id1, id2, amount)){
                    printf("failure: In AddTranUtilHigh \n");
                }
                return 1;
            }
            else{
                printf("failure: Not valid Node id \n");
                return 1;
            }
            token = strtok(NULL, " ");
        }
        if(amount_position!= 3){
            printf("failure: No valid arguments at all\n");
            return 1;
        }
        return 1;
    }
    return 0;
}   

int deltran(struct HashStruct** hashStruct, char* str){//omoiws me parapanw
    char* token;
    char* function;
    int token_length;
    int id1;
    int id2;
    int node_position = 0;
    
    char command[strlen(str)+1];
    strncpy(command, str, (strlen(str)));
    if(command[strlen(str)-1] == '\n'){
        command[strlen(str)-1] = '\0';
    }
    else{
        command[strlen(str)] = '\0';
    }
    fflush( stdout );
    token = strtok(command, " ");
    function = "deltran";
    if(!strcmp(token, function)){
        token = strtok(NULL, " ");
        if(token == NULL){
            printf("failure: No arguments at all\n");
            return 1;
        }
        while (token != NULL){
            token_length = strlen(token);
            if(token_length == 7){
                char digits[token_length];
                strncpy(digits, token, token_length);
                int i = 0;
                while(i<= token_length-1){
                    if(isdigit(digits[i]) == 0){
                        printf("failure: Not valid Node id\n");
                        return 1;
                    }
                    i++;
                }
                node_position++;
                if(node_position == 1){
                    id1 = atoi(token);
                }
                if(node_position == 2){//ean diavasoume diadoxika kai 2o komvo
                    if(strtok(NULL, " ") != NULL){
                        printf("failure: More than two arguments\n");
                        return 1;
                    }
                    id2 = atoi(token);
                    if(!DelTranUtilHigh(hashStruct, id1, id2)){
                        printf("failure: In DelTranUtilHigh \n");
                    }
                    return 1;
                }
            }
            else{//ean den diavasoume 7pshfio ws prwto argument
                printf("failure: Not valid Node id \n");
                return 1;
            }
            token = strtok(NULL, " ");
        }
        if(node_position < 2){//an telika dn diavasoyme 2 nodes
            printf("failure: Less than two arguments\n");
            return 1;
        }
        return 1;
    }
    return 0;  
}

int conn(struct HashStruct** hashStruct, char* str){//h logikh einai omoia. afth th fora diavazoume mono dyo arguments, ta node ids
    char* token;
    char* function;
    int token_length;
    int id1;
    int id2;
    int node_position = 0;
    
    char command[strlen(str)+1];
    strncpy(command, str, (strlen(str)));
    if(command[strlen(str)-1] == '\n'){
        command[strlen(str)-1] = '\0';
    }
    else{
        command[strlen(str)] = '\0';
    }
    fflush( stdout );
    token = strtok(command, " ");
    function = "conn";
    if(!strcmp(token, function)){
        token = strtok(NULL, " ");
        if(token == NULL){
            printf("failure: No arguments at all\n");
            return 1;
        }
        while (token != NULL){
            token_length = strlen(token);
            if(token_length == 7){
                char digits[token_length];
                strncpy(digits, token, token_length);
                int i = 0;
                while(i<= token_length-1){
                    if(isdigit(digits[i]) == 0){
                        printf("failure: Not valid Node id\n");
                        return 1;
                    }
                    i++;
                }
                node_position++;
                if(node_position == 1){
                    id1 = atoi(token);
                }
                if(node_position == 2){
                    if(strtok(NULL, " ") != NULL){
                        printf("failure: More than two arguments in conn\n");
                        return 1;
                    }
                    id2 = atoi(token);
                    if(!ConnUtilHigh(hashStruct, id1, id2)){
                        printf("failure: In ConnUtilHigh \n");
                    }
                    return 1;
                } 
            }
            else{
                printf("failure: Not valid Node id \n");
                return 1;
            }
            token = strtok(NULL, " ");
        }
        if(node_position < 2){
            printf("failure: Less than two arguments in conn \n");
            return 1;
        }
        return 1;
    }
    return 0;     
}