#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

void TerminalArg(FILE** f, int* HashEntries, int argc, char** argv){
    
    if(argc < 3){
        printf("CRITICAL failure: Error in terminalArg, Less than 3 arguments\n");
        exit(1);
    }
       
    //an lavoume  toul 3 orismata, tote prepei na einai ta ./elegxos -b HashEntries + garbage
    if((!strcmp(argv[1], "-b"))&&(atoi(argv[2])>0)&&((argc<=3)||(strcmp(argv[3], "-o")))){
        *f = NULL;
        *HashEntries = atoi(argv[2]);
    }
 
    //an lavoume toul 5, eite than einai ./elegxos -o filename -b HashEntries + garbage
    else  if((!strcmp(argv[1], "-o"))&&(!strcmp(argv[3], "-b"))&&(atoi(argv[4])>0)){
        *f = fopen(argv[2], "r");
        if(*f == NULL){
            printf("CRITICAL failure: Error, no filename found\n");
            exit(1);
        }
        *HashEntries = atoi(argv[4]);
    }
    //eite than einai ./elegxos -b HashEntries -o filename + garbage
    else if((!strcmp(argv[1], "-b"))&&(atoi(argv[2])>0)&&(!strcmp(argv[3], "-o"))){
        *f = fopen(argv[4], "r");
        if(*f == NULL){
            printf("CRITICAL failure: Error, no filename found\n");
            exit(1);
        }
        *HashEntries = atoi(argv[2]);  
    }
    else{//oles oi alles periptwseis einai error, OXI garbage endiamesa.
        printf("CRITICAL failure: Error in terminalArg, wrong filename and HashEntries sequence\n");
        exit(1);
    }
}