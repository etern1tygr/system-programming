#include <stdio.h>
#include <stdlib.h>
#include "Structures_Defines.h"

int InLinkListCreate(struct Node* N) {//dhmiourgei kenh lista gia Inlinks gia tn komvo N
    if (N == NULL) {
        printf("failure: N is NULL in InListCreate\n");
        return 0;
    }

    if (N->InlinkList == NULL) {//dhmiourgei ti lista
        if ((N->InlinkList = malloc(sizeof (struct ListOfInLinks))) == NULL) {
            printf("failure: N%07d InlinkList malloc FAILED in InLinkListCreate\n", N->id);
            exit(1);
        }
        N->InlinkList->GraphNode = N;//thetei tn komvo, N,  ston opoio anhkei
        N->InlinkList->startInLink = NULL; //arxikopoiei tn arxh
        N->InlinkList->lastInLink = NULL;//to telos
        N->InlinkList->size = 0;//kai to megethos
        return 1;
    }//alliws exei hdh dhmiourghthei
    return 1;
}

int OutLinkListCreate(struct Node* N) {//dhmiourgei thn kenh lista gia Outlinks gia komvo N, omoiws me parapanw
    if (N == NULL) {
        printf("failure: N is NULL in OutLinkListCreate\n");
        return 0;
    }

    if (N->OutlinkList == NULL) {
        if ((N->OutlinkList = malloc(sizeof (struct ListOfOutLinks))) == NULL) {
            printf("failure: N%07d OutlinkList malloc FAILED in OutLinkListCreate\n", N->id);
            exit(1);
        }
        N->OutlinkList->GraphNode = N;
        N->OutlinkList->startOutLink = NULL;
        N->OutlinkList->lastOutLink = NULL;
        N->OutlinkList->size = 0;
        return 1;
    }
    return 1;
}

int InLinkElemCreate(struct Node* N, struct Link* newIncoming) {//dhmiourgei ena stoixeio sthn InLinkList tou komvou N

    if ((N == NULL) || (newIncoming == NULL)) {
        printf("failure: N is NULL or newIncoming Link is NULL in InLinkElemCreate\n");
        return 0;
    }

    if (N->InlinkList == NULL) {//an den yparxei h InLinkList, dhmiourghse mia kenh
        InLinkListCreate(N);
    }

    struct InLinkElem* cursor = N->InlinkList->startInLink;
    struct InLinkElem* newElem;//dhmiourgei to neo InLinkElement
    if ((newElem = malloc(sizeof (struct InLinkElem))) == NULL) {
        printf("failure: newElem's malloc for N%07d FAILED in InLinkElemCreate\n", N->id);
        exit(1);
    }

    if (cursor == NULL) {//an den yparxei arxiko Element, dld h InLinkList einai kenh
        newElem->nextInLink = NULL; //these ton epomeno tou neou Element ws NULL
        newElem->prevInLink = NULL;//ton prohgoumeno se void
        newElem->LinkToCurrentNode = newIncoming;//deixnei stp Link pou paei stn epomeno Komvo
        N->InlinkList->startInLink = newElem;//thes to neo Element ws to arxiko
        N->InlinkList->lastInLink = newElem;//ws to teliko
        N->InlinkList->GraphNode = N;//these tn Komvo stn opoio anhkei h lista
        newElem = NULL;
        cursor = NULL;
        return 1;
    }

    while (cursor->nextInLink != NULL) {//alliws an den einai kenh
        cursor = cursor->nextInLink;//vres to teleftaio != NULL Element
    }

    newElem->nextInLink = NULL; //these ton epomeno tou neou Element ws NULL
    newElem->prevInLink = cursor; //ton prohgoumeno tou sto teleftaio egyro Element
    newElem->LinkToCurrentNode = newIncoming; //na deixnei stn akmh pou deixnei ston Komvo N(ston eafto tou dld)
    cursor->nextInLink = newElem;//these ton epomeno tou egyrou Element sto neo Element
    N->InlinkList->lastInLink = newElem; //these ton neo Element ws to teliko
    N->InlinkList->GraphNode = N;
    newElem = NULL;
    cursor = NULL;
    return 1;
}

int OutLinkElemCreate(struct Node* N, struct Link* newOutcoming) {//omoiws gia tn dhmiourgia OutLinkElement gia OutLinkLIst gia komvo N
    if ((N == NULL) || (newOutcoming == NULL)) {
        printf("failure: N is NULL or newOutcoming Link is NULL in OutLinkElemCreate\n");
        return 0;
    }

    if (N->OutlinkList == NULL) {
        OutLinkListCreate(N);
    }
    struct OutLinkElem* cursor = N->OutlinkList->startOutLink;
    struct OutLinkElem* newElem;
    if ((newElem = malloc(sizeof (struct OutLinkElem))) == NULL) {
        printf("failure: newElem's malloc for N%07d FAILED in OutLinkElemCreate\n", N->id);
        exit(1);
    }
    if (cursor == NULL) {
        newElem->LinkToNextNode = newOutcoming;
        newElem->nextOutLink = NULL;
        newElem->prevOutLink = NULL;
        N->OutlinkList->startOutLink = newElem;
        N->OutlinkList->lastOutLink = newElem;
        N->OutlinkList->GraphNode = N;
        newElem = NULL;
        cursor = NULL;
        return 1;
    }

    while (cursor->nextOutLink != NULL) {
        cursor = cursor->nextOutLink;
    }
    newElem->LinkToNextNode = newOutcoming;
    newElem->nextOutLink = NULL;
    newElem->prevOutLink = cursor;
    cursor->nextOutLink = newElem;
    N->OutlinkList->lastOutLink = newElem;
    N->OutlinkList->GraphNode = N;
    newElem = NULL;
    cursor = NULL;
    return 1;

}

int DeleteInLinkListForN(struct Node* N) {//diagrafei tn InLinkList gia komvo N, MONO ean den exei kanena stoixeio
    if (N == NULL) {
        printf("failure: N is NULL in DeleteInLinkListForN\n");
        return 0;
    }

    if (N->InlinkList == NULL) {//an einai hdh NULL, shmainei oti exei ginei deleted prohgoumenos
        return 1;
    }

    if (N->InlinkList->startInLink == NULL) {//alliws ean yparxei h lista, alla einai adeia
        N->InlinkList->GraphNode = NULL;//arxikopoihse olous tous deiktes se NULL
        N->InlinkList->lastInLink = NULL;
        free(N->InlinkList);//kai free tn mnhmh
        N->InlinkList = NULL;
        return 1;
    } else {//alliws shmainei oti h List dn einai kenh, alla h DeleteInLinkList kaleitai mono ean einai kenh, opote einai critical
        printf("CRITICAL failure: N%07d InlinkList IS NOT EMPTY yet in DeleteInLinkListForN\n", N->id);
        exit(1);
    }
}

int DeleteOutLinkListForN(struct Node* N) {//Omoiws gia diagrafh OutLinkList gia Komvo N
    if (N == NULL) {
        printf("failure: N%07d is NULL in DeleOutLinkListForN\n", N->id);
        exit(1);
    }

    if (N->OutlinkList == NULL) {
        return 1;
    }

    if (N->OutlinkList->startOutLink == NULL) {
        N->OutlinkList->GraphNode = NULL;
        N->OutlinkList->lastOutLink = NULL;
        free(N->OutlinkList);
        N->OutlinkList = NULL;
        return 1;
    } else {
        printf("CRITICAL failure: N%07d OutlinkList IS NOT EMPTY yet in DeleteOutLinkListForN\n", N->id);
        exit(1);
    }
}

int DeleteInLinkElemForN2(struct Node* N1, struct Node* N2, struct Link* pivotLink) {//Diagrafei stoixeio gia InLinkList, gia Komvo N
    if ((N1 == NULL) || (N2 == NULL) || (pivotLink == NULL)) {
        printf("failure: N is NULL or N is NULL or pivotLink is NULL in DeleteInLinkElemForN2\n");
        return 0;
    }

    if (N2->InlinkList == NULL) {// einai critical dioti h DeleteInLinkElem kaleitai mono ean den einai kenh h lista
        printf("CRITICAL failure: N%07d InlinkList SHOULD NOT BE NULL in DeleteInLinkElemForN2\n", N2->id);
        exit(1);
    } 
    else if (N2->InlinkList->startInLink == NULL) {//omoiws, den ginetai na yparxei InLinkList xwris arxiko Element
        printf("CRITICAL failure: N%07d startInlink SHOULD NOT BE NULL in DeleteInLinkElemForN2\n", N2->id);
        exit(1);
    }
    struct InLinkElem* cursorN2 = N2->InlinkList->startInLink;//ean pame na diagrapsoume to arxiko Element pou deixnei se akmh N1->N2, kai einai to monadiko ths listas
    if ((cursorN2->nextInLink == NULL)&&(cursorN2->LinkToCurrentNode->toNode == N2)&&(pivotLink->fromNode == N1)) {
        cursorN2->LinkToCurrentNode = NULL;//these tn deikth stn akmh NULL
        cursorN2->prevInLink = NULL;//tn deikth se previous Element NULL
        N2->InlinkList->startInLink = NULL;//omoiws gia to arxiko Element
        free(cursorN2);//apeleftherwse to Element
        cursorN2 = NULL;
        return DeleteInLinkListForN(N2);//kai diegrapse, mono ean einai kenh pleon, pou prepei na einai.
    }
    while (cursorN2 != NULL) {//alliws ean pame na diagrapsoume kai h lista den einai kenh
        if ((cursorN2->LinkToCurrentNode->toNode == N2)&&(pivotLink->fromNode == N1)) {//elegxos an prokeitai gia tn akmh p theloume
            cursorN2->LinkToCurrentNode = NULL;//arxikopoihse to deikth stn akmh N1->N2 se NULL
            if (cursorN2 == N2->InlinkList->startInLink) {//ean pame na diagrapsoume tn arxh
                cursorN2->prevInLink = NULL;//these tn deikth stn previous Element ws NULL
                N2->InlinkList->startInLink = cursorN2->nextInLink;//to epomenos Element ths arxis tha einai twra h nea arxh/head
                N2->InlinkList->startInLink->prevInLink = NULL;//o previous thanai NULL
            } else {//alliws an pame na diagrapsoume sth mesh h telos
                cursorN2->prevInLink->nextInLink = cursorN2->nextInLink;//thetoume tn epomeno tou prhgoumenou Element na einai to epomeno aftou p tha ginei delete
                if (cursorN2->nextInLink == NULL) {//an einai NULL aftos o epomenos tou to-bedeleted
                    N2->InlinkList->lastInLink = cursorN2->prevInLink;//o teleftaios einai o previous tou
                } else {
                    N2->InlinkList->lastInLink = cursorN2->nextInLink;//alliws o last einai o epomenos
                }

            }
            cursorN2->nextInLink = NULL;
            cursorN2->prevInLink = NULL;
            free(cursorN2);
            cursorN2 = NULL;
            return 1;
        }
        cursorN2 = cursorN2->nextInLink;//psaxoume tn katalhlo Element p deixnei stn akmh p theloume
    }
    printf("failure: N%07d InlinkList startInLink is NULL in DeleteInLinkElemForN2\n", N2->id);
    return 0;
}

int DeleteOutLinkElemForN1(struct Node* N1, struct Node* N2, struct Link* pivotLink) {//Omoiws gia delete OutLinkElement enos komvou N
    if ((N1 == NULL) || (N2 == NULL) || (pivotLink == NULL)) {
        printf("failure: N is NULL or N is NULL or pivotLink is NULL in DeleteOutLinkElemForN2\n");
        return 0;    
    }

    if (N1->OutlinkList == NULL) {
        printf("CRITICAL failure: N%07d OutlinkList SHOULD NOT BE NULL in DeleteOutLinkElemForN2\n", N1->id);
        exit(1);
    } 
    else if (N1->OutlinkList->startOutLink == NULL) {
        printf("CRITICAL failure: N%07d startOutlink SHOULD NOT BE NULL in DeleteOutLinkElemForN2\n", N1->id);
        exit(1);
    }

    struct OutLinkElem* cursorN1 = N1->OutlinkList->startOutLink;
    if ((cursorN1->nextOutLink == NULL)&&(cursorN1->LinkToNextNode->toNode == N2)&&(pivotLink->fromNode == N1)) {
        cursorN1->LinkToNextNode = NULL;
        cursorN1->prevOutLink = NULL;
        N1->OutlinkList->startOutLink = NULL;
        free(cursorN1);
        cursorN1 = NULL;
        return DeleteOutLinkListForN(N1);
    }
    while (cursorN1 != NULL) {
        if ((cursorN1->LinkToNextNode->toNode == N2)&&(pivotLink->fromNode == N1)) {
            cursorN1->LinkToNextNode = NULL;
            if (cursorN1 == N1->OutlinkList->startOutLink) {
                cursorN1->prevOutLink = NULL;
                N1->OutlinkList->startOutLink = cursorN1->nextOutLink;
                N1->OutlinkList->startOutLink->prevOutLink = NULL;
                cursorN1->nextOutLink = NULL;
            } else {
                cursorN1->prevOutLink->nextOutLink = cursorN1->nextOutLink;
                if (cursorN1->nextOutLink == NULL) {
                    N1->OutlinkList->lastOutLink = cursorN1->prevOutLink;
                } else {
                    N1->OutlinkList->lastOutLink = cursorN1->nextOutLink;
                }
                cursorN1->nextOutLink = NULL;
                cursorN1->prevOutLink = NULL;
            }
            free(cursorN1);
            cursorN1 = NULL;
            return 1;
        }
        cursorN1 = cursorN1->nextOutLink;
    }
    cursorN1 = NULL;
    printf("failure: N%07d OutlinkList startOutLink is NULL in DeleteOutLinkElemForN2\n", N1->id);
    return 0;
}

int DeleteInLinksForN(struct Node* N) {//Diagrafei OLA ta InLinkElements gia Node N
    if (N == NULL) {
        printf("failure: N is NULL in DeleteInLinksElemForN\n");
        return 0;
    }

    if (N->InlinkList == NULL) {//an einai deleted h list tote den yparxei kati na diagrafei
        return 1;
    }

    struct InLinkElem* cursorN = N->InlinkList->startInLink;
    struct InLinkElem* cursorD;
    struct Link* cursorLinkN = NULL;
    while (cursorN != NULL) {//gia kathe akmi
        cursorLinkN = cursorN->LinkToCurrentNode;//apothikevoume tn akmi p deixnei stn N
        cursorD = cursorN;
        cursorN = cursorN->nextInLink;//metavenoume stn epomeno Element p deixnei se akmh
        free(cursorD);//free to Element
        free(cursorLinkN);//free kai tn akmh stn opoia deixnei
    }
    N->InlinkList->startInLink = NULL;
    return 1;
}

int DeleteOutLinksForN(struct Node* N) {//Omoiws me parapanw, me th diafora oti tn akmh den tn diagrafoume pali, afou th diagrafoume mono apo tn InLink meria
    if (N == NULL) {
        printf("failure: N is NULL in DeleteInLinksElemForN\n");
        return 0;
    }

    if (N->OutlinkList == NULL) {
        return 1;
    }

    struct OutLinkElem* cursorN = N->OutlinkList->startOutLink;
    struct OutLinkElem* cursorD;

    while (cursorN != NULL) {//gia kathe akmi
        cursorD = cursorN;
        cursorN = cursorN->nextOutLink;
        free(cursorD);
    }
    N->OutlinkList->startOutLink = NULL;
    return 1;

}