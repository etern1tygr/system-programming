#!/bin/bash

if [ -e $1 ]
    then
        cd $1
        for entry in $1/* 
        do
          if [ -d $entry ] && [ -r $entry ]
              then        
                  cd $entry
                  i=0
                  for f in $entry/*
                  do 
                    name=${f##*/}
                    if [ "$name" == "server_ID" ] && [ -f $f ]
                        then
                            content=`cat server_ID`
                            exists=`ps -e | grep $content | awk '{print $1}'` 
                            let i=i+1
                    elif [ -p $f ]
                        then let i=i+1
                    fi
                  done 
                  if [ "$i"  == "5" ] && [ "$exists" == "$content" ]
                      then 
                          echo "active $entry"
                  elif [ "$exists" != "$content" ]
                      then 
                          echo "incactive $entry"  
                  fi  
          fi
        done      
fi    
 
    

