#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <ctype.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

void pathCheckBoard(char* path, int argc, char** argv){

    strncpy(path, argv[1], strlen(argv[1]));
    
    if(path[strlen(argv[1])-1] == '\n'){
        path[strlen(argv[1])-1] = '\0';
    }
    else{
        path[strlen(argv[1])] = '\0';
    }

    char tmp[strlen(path)+1];
    strncpy(tmp, path, strlen(path));
    tmp[strlen(path)] = '\0';


}

int serverExistsBoard(char* path){
    //omoia opws sto board
    
    struct stat buffer;
    char* filename = "/server_ID";
    char filename_path[strlen(path) + strlen(filename)+1];
    strncpy(filename_path, path, strlen(path));
    filename_path[strlen(path)] = '\0';
    strncat(filename_path, filename, strlen(filename));
    if(stat(filename_path, &buffer) == 0){
        return 0;
    }
    else{
        return 1;
    }
}

int pipesExistBoard(char* path, char* pathwriteClientServer, char* pathreadClientServer, char* pathwriteServerClientpost, char* pathreadServerClientpost){
    //omoia opws sto board
    struct stat buffer;
    char* pipename1 = "/writeClientServer";
    char* pipename2 = "/readClientServer";
    char* pipename3 = "/writeServerClientpost";
    char* pipename4 = "/readServerClientpost";
    
    char* pipename1w = "/riteClientServer";
    char* pipename2a = "/redClientServer";
    char* pipename3w = "/riteServerClientpost";
    char* pipename4a = "/redServerClientpost";
    
    char pipename_path1[strlen(path) + strlen(pipename1)+1];
    char pipename_path1w[strlen(path) + strlen(pipename1w)+1];
    
    char pipename_path2[strlen(path) + strlen(pipename2)+1];
    char pipename_path2a[strlen(path) + strlen(pipename2a)+1];
    
    char pipename_path3[strlen(path) + strlen(pipename3)+1];
    char pipename_path3w[strlen(path) + strlen(pipename3w)+1];
    
    char pipename_path4[strlen(path) + strlen(pipename4)+1];
    char pipename_path4a[strlen(path) + strlen(pipename4a)+1];
    
    
    strncpy(pipename_path1, path, strlen(path));
    pipename_path1[strlen(path)] = '\0';
    strncat(pipename_path1, pipename1, strlen(pipename1));
    
    strncpy(pipename_path1w, path, strlen(path));
    pipename_path1w[strlen(path)] = '\0';
    strncat(pipename_path1w, pipename1w, strlen(pipename1w));
    
    strncpy(pipename_path2, path, strlen(path));
    pipename_path2[strlen(path)] = '\0';
    strncat(pipename_path2, pipename2, strlen(pipename2));
    
    strncpy(pipename_path2a, path, strlen(path));
    pipename_path2a[strlen(path)] = '\0';
    strncat(pipename_path2a, pipename2a, strlen(pipename2a));
    
    strncpy(pipename_path3, path, strlen(path));
    pipename_path3[strlen(path)] = '\0';
    strncat(pipename_path3, pipename3, strlen(pipename3));
    
    strncpy(pipename_path3w, path, strlen(path));
    pipename_path3w[strlen(path)] = '\0';
    strncat(pipename_path3w, pipename3w, strlen(pipename3w));
    
    strncpy(pipename_path4, path, strlen(path));
    pipename_path4[strlen(path)] = '\0';
    strncat(pipename_path4, pipename4, strlen(pipename4));
    
    strncpy(pipename_path4a, path, strlen(path));
    pipename_path4a[strlen(path)] = '\0';
    strncat(pipename_path4a, pipename4a, strlen(pipename4a));
    
    int i = 0;
    if(stat(pipename_path1, &buffer) != 0){
        if(stat(pipename_path1w, &buffer) != 0){
            return 1;
        }
        else{
            strncpy(pathwriteClientServer, pipename_path1w, strlen(pipename_path1w));
            pathwriteClientServer[strlen(pipename_path1w)] = '\0';
            i++;
        }    
    }
    else{
        strncpy(pathwriteClientServer, pipename_path1, strlen(pipename_path1));
        pathwriteClientServer[strlen(pipename_path1)] = '\0';
        i++;
    }
    
    
    if(stat(pipename_path2, &buffer) != 0){
        if(stat(pipename_path2a, &buffer) != 0){
            return 1;
        }
        else{
            strncpy(pathreadClientServer, pipename_path2a, strlen(pipename_path2a));
            pathreadClientServer[strlen(pipename_path2a)] = '\0';
            i++;
        }
    }
    else{
         strncpy(pathreadClientServer, pipename_path2, strlen(pipename_path2));
         pathreadClientServer[strlen(pipename_path2)] = '\0';
         i++;
    }
    
    if(stat(pipename_path3, &buffer) != 0){
        if(stat(pipename_path3w, &buffer) != 0){
            return 1;
        }
        else{
            strncpy(pathwriteServerClientpost, pipename_path3w, strlen(pipename_path3w));
            pathwriteServerClientpost[strlen(pipename_path3w)] = '\0';
            i++;
        }
    }
    else{
        strncpy(pathwriteServerClientpost, pipename_path3, strlen(pipename_path3));
        pathwriteServerClientpost[strlen(pipename_path3)] = '\0';
        i++;
    }
    
    if(stat(pipename_path4, &buffer) != 0){
        if(stat(pipename_path4a, &buffer) != 0){
            return 1;
        }
        else{
            strncpy(pathreadServerClientpost, pipename_path4a, strlen(pipename_path4a));
            pathreadServerClientpost[strlen(pipename_path4a)] = '\0';
            i++;
        }
    }
    else{
        strncpy(pathreadServerClientpost, pipename_path4, strlen(pipename_path4));
        pathreadServerClientpost[strlen(pipename_path4)] = '\0';
        i++;
    }
    
    if(i == 4){
        return 0;
    }
    return 1;

    
}