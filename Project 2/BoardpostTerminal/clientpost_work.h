#include <stdio.h>
#include <string.h>
#include <stdlib.h>


int doReadfromInput(char* str, char* id, char* name);
void pathCheckBoard(char* path, int argc, char** argv);
int serverExistsBoard(char* path);
int pipesExistBoard(char* path, char* pathwriteClientServer, char* pathreadClientServer, char* pathwriteServerClientpost, char* pathreadServerClientpost);
void doSendFile(char* path, char* id, char* pathfile, char* pathwriteServerClientpost, char* pathreadServerClientpost);
void doSendMessage(char* path, char* id, char* name, char* pathwriteServerClientpost, char* pathreadServerClientpost);
void doPrintList(char* path, char* pathwriteServerClientpost);
void doReadfromServer(char* pathreadServerClientpost, char* path);

