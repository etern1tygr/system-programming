#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <dirent.h>
#include <ctype.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include "clientpost_work.h"
#include "signal.h"


int main(int argc, char** argv) {
    signal(SIGINT, SIG_IGN);
    
    if(argc!= 2){
        printf("No path in input, or more than 2 arguments\n");
        fflush(stdout);
        exit(1);
    }

    int pathsize = strlen(argv[1])+1;
    char path[pathsize];

    pathCheckBoard(path, argc, argv);
    
    int pipesize = strlen("/writeClientServer")+1; 
    char pathwriteClientServer[pathsize+pipesize-1]; 

    pipesize = strlen("/readClientServer");
    char pathreadClientServer[pathsize+pipesize-1]; 

    pipesize = strlen("/writeServerClientpost")+1;
    char pathwriteServerClientpost[pathsize+pipesize-1]; 

    pipesize = strlen("/readServerClientpost")+1;
    char pathreadServerClientpost[pathsize+pipesize-1]; 


    
    if(serverExistsBoard(path) == 0){//me 0 an yparxei me 1 an dn yparxei
        if(pipesExistBoard(path, pathwriteClientServer, pathreadClientServer, pathwriteServerClientpost, pathreadServerClientpost) == 0){
            char str[1500];
            char id[10];
            char name[50];
            int function = 0;
            printf("Type user commands:\n");
            fflush(stdout);
            while(1){
                fgets(str, sizeof(str), stdin);
                function = doReadfromInput(str, id, name);
                if(function == 0){
                    continue;
                }
                else if(function == 1){
                    doSendFile(path, id, name, pathwriteServerClientpost, pathreadServerClientpost);
                    doReadfromServer(pathreadServerClientpost, path);
                }
                else if(function == 2){
                    doSendMessage(path, id, name, pathwriteServerClientpost, pathreadServerClientpost);
                    doReadfromServer(pathreadServerClientpost, path);
                }
                else if(function == 3){
                    doPrintList(path, pathwriteServerClientpost);
                    doReadfromServer(pathreadServerClientpost, path);
  
                }
 
            }
        }
        exit(1);
    }
    exit(1);
}