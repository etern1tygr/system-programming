#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <ctype.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include "clientpost_work.h"



int doReadfromInput(char* str, char* id, char* name){
    char command[strlen(str)+1];//static pinaka n chars +1 gia null terminator
    strncpy(command, str, (strlen(str)));//copy to string stn static
    if(command[strlen(str)-1] == '\n'){
        command[strlen(str)-1] = '\0';
    }
    else{
        command[strlen(str)] = '\0';
    }
    int function = 0;//ean einai write, send, list to arxiko argument
    char* token = token = strtok(command, " ");
    if(!strcmp(command, "send")){
        function = 1;
    }
    
    else if(!strcmp(command, "write")){
        function = 2;
        
    }
    else if(!strcmp(command, "list")){
        function = 3;
        if(strtok(NULL, " ") == NULL){
            return function;
        }
    }
    else if(token == NULL){// a dn parei orismata mhnhma lathous
        printf("failure: No arguments at all\n");
        return 0;
    }
    else{
        printf("Wrong command\n");
        return 0;
    }
    int token_length;
    int flag = 0;
    int flag1 = 0;
    int number = 0;
    int notnumber = 0;
    token = strtok(NULL, " ");
    while (token != NULL){
        token_length = strlen(token);
        char digits[token_length];
        strncpy(digits, token, token_length);
        int i = 0;
        while(i<= token_length-1){//elegxos ean proketai gia digits
            if(isdigit(digits[i]) == 0){
                notnumber++;
                flag = 1;
                break;
            }
            i++;
        }

        if(flag == 0){
            number++;
        }

        if((notnumber == 0)&&(number == 1)){
             //theloume mono ena arithmo
            strncpy(id, token, token_length+1);
        }

        else if((notnumber == 1)&&(number == 1)){
            flag1 = 1;
            strncpy(name, token, token_length+1);//theloyme meta ton arithmo, char*

        }
        else{
            printf("Wrong argument sequence\n");
            return 0;
        }
        token = strtok(NULL, " ");
        flag = 0;
    }
    if(flag1!= 1){
        printf("Wrong argument sequence\n");
        return 0;
    }
    return function;
}

void doSendFile(char* path, char* id, char* pathfile, char* pathwriteServerClientpost, char* pathreadServerClientpost){
    int fd;
    if((fd = open(pathwriteServerClientpost, O_WRONLY, 0666)) < 0){//anoigei to pipe gia grafh
         perror("Error: ");
         exit(1);
    }
    
    FILE* fp;
    char message[3000];
    message[0] = '\0';
    if((fp = fopen(pathfile, "r")) == 0){//anoigei to file
         perror("Error: ");
         exit(1);
    }
    else{ 
        char p[2];
        while(1){
            p[0] = fgetc(fp);
            if(feof(fp)){
                break;
            }
            p[1] = '\0';
            strncat(message, p, strlen(p));
            
        }
         if(fclose(fp)!= 0){
            perror("Error: ");
            exit(1);
        }
    }
  
    //molis diavasei to content, stelnei id-pathfile-content#

    char command[8000];
    command[0] = '\0';
    strncat(command, id, strlen(id));
    strncat(command, "-", 1);
    strncat(command, pathfile, strlen(pathfile));
    strncat(command, "-", 1);
    strncat(command, message, strlen(message));
    int length = strlen(command);
    command[length] = '#';
    
    int bytes;
    if((bytes = write(fd, command, length+1)) ==  -1){
        perror("Error: ");
        exit(1);
    }

    if(bytes!= length+1){
        printf("Error in write\n");
        exit(1);
    }

    if(close(fd)!= 0){
        perror("Error: ");
        exit(1);
    }
}

void doSendMessage(char* path, char* id, char* name, char* pathwriteServerClientpost, char* pathreadServerClientpost){
    
    int fd;

    if((fd = open(pathwriteServerClientpost, O_WRONLY, 0666)) < 0){
         perror("Error: ");
         exit(1);
    }

    //anoigei to pipe eggrafhs kai stelnei id-message#
    int bytes;
    int length = strlen(name);
    int length_id = strlen(id);
    int length_mesg = length+length_id+2;
    char message[length_mesg];
    char id_space[length_id+2];    
    
    strncpy(id_space, id, length_id);
    id_space[length_id] = '-';
    id_space[length_id+1] = '\0';
    
    strncpy(message, id_space, length+2);
    strncat(message, name, strlen(name)+1);
    strncat(message, "#", strlen("#"));
    
    if((bytes = write(fd, message, length_mesg)) ==  -1){
        perror("Error: ");
        exit(1);
    }

    if(bytes!= length_mesg){
        printf("Error in write\n");
        exit(1);
    } 

    if(close(fd)!= 0){
        perror("Error: ");
        exit(1);
    }
}

void doPrintList(char* path, char* pathwriteServerClientpost){
    int fd;

    if((fd = open(pathwriteServerClientpost, O_WRONLY, 0666)) < 0){
         perror("Error: ");
         exit(1);
    }

    //omoiws stelnei LIST-#
    int bytes;
    
    if((bytes = write(fd, "LIST-#", strlen("LIST-#"))) ==  -1){
        perror("Error: ");
        exit(1);
    }

    if(bytes!= strlen("LIST-#")){
        printf("Error in write\n");
        exit(1);
    } 

    if(close(fd)!= 0){
        perror("Error: ");
        exit(1);
    }
    
}

void doReadfromServer(char* pathreadServerClientpost, char* path){ 
    int fd;
    
    if((fd = open(pathreadServerClientpost, O_RDONLY, 0644)) < 0){//anoigei to pipe diavasmatos
         perror("Error: ");
         exit(1);
    } 
    int bytes;
    char message[3000];
    message[0] = '\0';
    char data[1];
    char data1[2];
    data[0] = '\0';
    data1[0] ='\0';
    int flag = 1;
    
    do{ 
    
        if((bytes = read(fd, data, 1)) ==  -1){
            perror("Error: ");
            exit(1);
        }
        
        if(data[0]!='#'){
            data1[0] = data[0];
            data1[1] = '\0';
            strncat(message, data1, 1);
            if(message[0] == 'L'){
                flag = 0;
            }
        }
   
    }while(data[0]!= '#');
    
    //elegxei poia apokrish einai
    if(!strcmp(message, "SUCCESS file")){
        puts(message);
    }
    
    if(!strcmp(message, "SUCCESS message")){
        puts(message);
    }
    
    if(!strcmp(message, "Channel doesn't exist")){
        puts(message);
    }
    
    if(!strcmp(message, "No Channels")){
        puts(message);
    }
    
    if(flag == 0){//ean lavei L-name-id-name-id# ws apokrish ths list
        char* token = strtok(message, "-");
        int i = 0;
        while(token!= NULL){
            if(i == 1){
                printf("%s-", token);
                fflush(stdout);
            }
            else if(i == 2){
                printf("%s", token);
                fflush(stdout);
                printf("\n");
                fflush(stdout);
                i = 0;
            }
            token = strtok(NULL, "-");
            i++;
        }
    }
    
    if(close(fd)!= 0){
        perror("Error: ");
        exit(1);
    }
}