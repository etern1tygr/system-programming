#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "server_structs.h"
#include "client_work.h"


int createFileList(struct channelList** clist, int id){//ean yparxei kanali me id, ftiaxnei emia file list
 
    if(*clist == NULL){
        printf("Channel list doesn't exist\n");
        fflush(stdout);
        return 0;
    }
    
    else if((*clist)->start == NULL){
        printf("Channel list is empty\n");
        fflush(stdout);
        return 0;
    }
    
    struct channelElement* cursor = (*clist)->start;
    while(cursor!= NULL){
        if(cursor->id == id){
            break;
        }
        cursor = cursor->next;
    }
    
    if(cursor->id != id){
        printf("Channel with id: %d doesnt exist\n", id);
        fflush(stdout);
        return 0;
    }
    
    
    struct fileList* flist;
    if((flist = malloc(sizeof(struct fileList))) == NULL){
        perror("Error in file list create: ");
        exit(1);
    }
    
    flist->start = NULL;
    cursor->fList = flist;
    return 1;
}

int createFileElement(char* path, struct channelList** clist, int id, char* filecontent, char* filename){
    
    //dhmiourgei ena file element kai ftiaxnei mia file list ean dn yparxei
    
    struct channelElement* cursor;
    if(*clist == NULL){
        printf("Channel list doesn't exist\n");
        fflush(stdout);
        return 0;
    }
    else if((*clist)->start == NULL){
        printf("Channel list is empty\n");
        fflush(stdout);
        return 0;
    }
    else{
        cursor = (*clist)->start;
        while(cursor->next!= NULL){
            if(cursor->id == id){
                break;
            }
            cursor = cursor->next;
        }

        if(cursor->id != id){
            printf("Channel with id: %d doesnt exit\n", id);
            fflush(stdout);
            return 0;
        } 
    
        if(cursor->fList == NULL){
            if(!createFileList(clist, id)){
                return 0;
            }
        }
    }
    
    struct fileElement* cursorf = cursor->fList->start;
    struct fileElement* prev = cursorf;
    while((cursorf!= NULL)&&(cursorf->next!= NULL)){
        prev = cursorf;
        cursorf = cursorf->next;
    }
    
    struct fileElement* fElement;
    if((fElement = malloc(sizeof(struct fileElement))) == NULL){
        perror("Error in message Element create: ");
        exit(1);
    }
    
    //apothikevei to filename kai content sto element
    strncpy(fElement->filecontent, filecontent, strlen(filecontent)+1);
    strncpy(fElement->pathfile, filename, strlen(filename)+1);
    fElement->next = NULL;
    
    if(prev == NULL){//an h list einai kenh
        cursor->fList->start = fElement;
    }
    else{//alliws
        cursorf->next = fElement;
    }
    putinfile(path, filecontent, filename);
    return 1;

}

int deleteFileList(struct channelList** clist, int id){//omoia me channel list
    if(*clist == NULL){
        printf("Channel list is already null\n");
        fflush(stdout);
        return 1;
    }
    
    if((*clist)->start == NULL){
        printf("Channel list is empty\n");
        return 1;
    }
        
    struct channelElement* cursor = (*clist)->start;
    while(cursor!= NULL){
        if(cursor->id == id){
            break;
        }
        cursor = cursor->next;
    }
    
    if(cursor->id == id){
        if(cursor->fList == NULL){
            printf("Channel with id: %d has no File list\n", id);
            return 1;
        }
        struct fileElement* cursorfile = cursor->fList->start;
        struct fileElement* cursorfile1 = cursorfile;
        while(cursorfile!=NULL){
            cursorfile1 = cursorfile;
            cursorfile = cursorfile->next;
            free(cursorfile1);
        }
        free(cursor->fList);
        cursor->fList = NULL;
        return 1;
    }
    return 0;
  
}
