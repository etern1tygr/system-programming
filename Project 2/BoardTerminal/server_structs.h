#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct fileElement{
    char pathfile[500]; //krataw to sended path oxi tp (pithanon) allagmeno apo ton server
    char filecontent[3000]; //content tou file
    struct fileElement* next;
};

struct fileList{ //list apo files
    struct fileElement* start;
};

struct messageElement{ //list apo messages
    char message[3000];
    struct messageElement* next;
    //struct messageElement* previous;
};


struct messageList{
    struct messageElement* start;
};


struct channelElement{ //list apo kanalia
    int id;
    char name[500];
    struct messageList* mList;
    struct fileList* fList;
    struct channelElement* next;

};

struct channelList{
    struct channelElement* start;
};



