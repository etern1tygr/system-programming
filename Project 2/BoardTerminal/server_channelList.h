#include <stdio.h>
#include <string.h>
#include <stdlib.h>


int createChannelList(struct channelList** clist);
int createChannelElement(struct channelList** clist, int id, char* name);
int deleteChannelList(struct channelList** clist);
int deleteAll(struct channelList** clist);
