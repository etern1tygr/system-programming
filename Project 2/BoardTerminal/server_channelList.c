
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "server_structs.h"
#include "server_messageList.h"
#include "server_fileList.h"


int createChannelList(struct channelList** clist){//dhmiourgei kenh channel list
    
    if((*clist)!= NULL){
        printf("Channel List already exists\n");
        return 0;
    }
    
    if((*clist = malloc(sizeof(struct channelList))) == NULL){
        perror("Error in channel list create: ");
        exit(1);
    }
    (*clist)->start = NULL;
    return 1;   
}

int createChannelElement(struct channelList** clist, int id, char* name){//dhmiourgei ena channel element
    
    if(*clist == NULL){//ean dn yparxei list tn dhmiourgoume
        if(!createChannelList(clist)){
            printf("Error in createChannelList\n");
            fflush(stdout);
            exit(1);
        }
    }
    
    struct channelElement* cursor = (*clist)->start;
    struct channelElement* prev = cursor;
    
    while(cursor!= NULL){
        if(cursor->id == id){//psaxnoume ean yparxei hdh channel me id
            printf("Channel with id: %d already exists\n", id);
            fflush(stdout);
            return 0;
        }
        prev = cursor;
        cursor = cursor->next;
    }
    
    struct channelElement* cElement;
    if((cElement = malloc(sizeof(struct channelElement))) == NULL){
        perror("Error in channel Element create: ");
        exit(1);
    }
    //arxikopoiei to neo channel
    cElement->id = id;
    strncpy(cElement->name, name, strlen(name));
    cElement->name[strlen(name)] = '\0';
    cElement->fList = NULL;
    cElement->mList = NULL;
    cElement->next = NULL;
    
    if(prev == NULL){//tote h list einai kenh
        (*clist)->start = cElement;
    }
    else{//alliws dn einai
        prev->next = cElement;
    }
    return 1;
}

int deleteChannelList(struct channelList** clist){//diagrafei ena ena ta channel elements kai sto telos tn list
    
    if(*clist == NULL){
        printf("Channel list is already null\n");
        fflush(stdout);
        return 1;
    }
    
    if((*clist)->start == NULL){
        printf("Channel list is empty\n");
        return 1;
    }
    else{
        struct channelElement* cursor = (*clist)->start;
        struct channelElement* cursor1 = cursor;
        while(cursor!=NULL){
            cursor1 = cursor; 
            cursor = cursor->next;
            free(cursor1);      
        }
        free(*clist);
        return 1;
    }
    return 0;

}


int deleteAll(struct channelList** clist){//diagrafei gia kathe channel tn msg list k tn file list kai sto telos tn channel list
    
    if(*clist == NULL){
        printf("Channel list is already null\n");
        fflush(stdout);
        return 1;
    }
    
    if((*clist)->start == NULL){
        printf("Channel list is empty\n");
        return 1;
    }
    
    struct channelElement* cursor = (*clist)->start;
    int id;
    
    while(cursor!= NULL){
        id = cursor->id;

        if(!deleteMessageList(clist, id)){
            exit(1);
        }
        if(!deleteFileList(clist, id)){
            exit(1);
        }
        cursor = cursor->next;
    }
    
    if(!deleteChannelList(clist)){
        exit(1);
    }
    
    return 1;
}