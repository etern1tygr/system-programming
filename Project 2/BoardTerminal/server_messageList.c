
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "server_structs.h"


//akrivws omoia me file list oles oi synarthseis..
int createMessageList(struct channelList** clist, int id){

    if(*clist == NULL){
        printf("Channel list doesn't exist\n");
        fflush(stdout);
        return 0;
    }
    
    else if((*clist)->start == NULL){
        printf("Channel list is empty\n");
        fflush(stdout);
        return 0;
    }
    
    struct channelElement* cursor = (*clist)->start;
    while(cursor->next!= NULL){
        if(cursor->id == id){
            break;
        }
        cursor = cursor->next;
    }
    
    if(cursor->id != id){
        printf("Channel with id: %d doesnt exist\n", id);
        fflush(stdout);
        return 0;
    }

    struct messageList* mlist;
    if((mlist = malloc(sizeof(struct messageList))) == NULL){
        perror("Error in message list create: ");
        exit(1);
    }
    
    mlist->start = NULL;
    cursor->mList = mlist;
    return 1;
}

int createMessageElement(struct channelList** clist, int id, char* message){
    
    struct channelElement* cursor; 
    if(*clist == NULL){
        printf("Channel list doesn't exist\n");
        fflush(stdout);
        return 0;
    }
    else if((*clist)->start == NULL){
        printf("Channel list is empty\n");
        fflush(stdout);
        return 0;
    }
    else{
        cursor = (*clist)->start;
        while(cursor->next!= NULL){
            if(cursor->id == id){
                break;
            }
            cursor = cursor->next;
        }

        if(cursor->id != id){
            printf("Channel with id: %d doesnt exit\n", id);
            fflush(stdout);
            return 0;
        }
        
        if(cursor->mList == NULL){
            if(!createMessageList(clist, id)){
                return 0;
            }
        }
    }
    
    struct messageElement* cursorm = cursor->mList->start;
    struct messageElement* prev = cursorm;
    while((cursorm!= NULL)&&(cursorm->next!= NULL)){
        prev = cursorm;
        cursorm = cursorm->next;
    }

    struct messageElement* mElement;
    if((mElement = malloc(sizeof(struct messageElement))) == NULL){
        perror("Error in message Element create: ");
        exit(1);
    }
    
    strncpy(mElement->message, message, strlen(message)+1);
    mElement->next = NULL; 
    
    if(prev == NULL){
        cursor->mList->start = mElement;
    }
    else{
        cursorm->next = mElement;
    }
    return 1;
}

int deleteMessageList(struct channelList** clist, int id){
    
    if(*clist == NULL){
        printf("Channel list is already null\n");
        fflush(stdout);
        return 1;
    }
    
    if((*clist)->start == NULL){
        printf("Channel list is empty\n");
        return 1;
    }
        
    struct channelElement* cursor = (*clist)->start;
    while(cursor!= NULL){
        if(cursor->id == id){
            break;
        }
        cursor = cursor->next;
    }
    
    if(cursor->id == id){
        if(cursor->mList == NULL){
            printf("Channel with id: %d has no Message list\n", id);
            return 1;
        }
        struct messageElement* cursormsg = cursor->mList->start;
        struct messageElement* cursormsg1 = cursormsg;
        while(cursormsg!=NULL){
            cursormsg1 = cursormsg;
            cursormsg = cursormsg->next;
            free(cursormsg1);
        }
        free(cursor->mList);
        cursor->mList = NULL;
        return 1;
    }
    return 0;
  
}