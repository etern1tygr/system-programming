#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int createFileList(struct channelList** clist, int id);
int createFileElement(char* path, struct channelList** clist, int id, char* filecontent, char* filename);
int deleteFileList(struct channelList** clist, int id);
