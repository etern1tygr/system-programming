#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <dirent.h>
#include <ctype.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include "client_work.h"
#include "server_structs.h"
#include "server_channelList.h"
#include "server_fileList.h"
#include "server_messageList.h"
#include "server_work.h"
#include "client_work.h"
#include "prepare_board.h"
#include <signal.h>

int main(int argc, char** argv) {

    signal(SIGINT, SIG_IGN);
    if (argc != 2) {
        printf("No path in input, or more than 2 arguments\n");
        fflush(stdout);
        exit(1);
    }

    int pathsize = strlen(argv[1]) + 1;
    char path[pathsize];

    pathCheckBoard(path, argc, argv);

    int pipesize = strlen("/writeClientServer") + 1; //for possible extra char and null term
    char pathwriteClientServer[pathsize + pipesize - 1]; // for null term

    pipesize = strlen("/readClientServer") + 1;
    char pathreadClientServer[pathsize + pipesize - 1]; // for null term

    pipesize = strlen("/writeServerClientpost") + 1;
    char pathwriteServerClientpost[pathsize + pipesize - 1]; // for null term

    pipesize = strlen("/readServerClientpost") + 1;
    char pathreadServerClientpost[pathsize + pipesize - 1]; // for null term

    //ean yparxei o server, tote ean yparxoyn pipes synexise,  alliws exit
    if(serverExistsBoard(path) == 0) {//me 0 an yparxei me 1 an dn yparxei
        if (pipesExistBoard(path, pathwriteClientServer, pathreadClientServer, pathwriteServerClientpost, pathreadServerClientpost) == 1) {
            //1 ean dn yparxoun pipes, 0 ean yparxoyn
            exit(1);
        }
        char str[8000];
        char id[50];
        char name[500];
        int function = 0;
        printf("Type client commands in existing server:\n");
        fflush(stdout);

        while (1) {//diavazei apo to stdin
            if(fgets(str, sizeof (str), stdin)== NULL){
                exit(1);
            }
            
            //analoga me to return ths read
            function = doReadfromInput(str, id, name);
            if (function == 0) {
                continue;
            } else if (function == 1) {
                doCreateChannel(path, id, name, pathwriteClientServer);
                doReadfromServer(pathreadClientServer, path);
            } else if (function == 2) {
                doGetMessage(path, id, pathwriteClientServer);
                doReadfromServer(pathreadClientServer, path);
            } else if (function == 3) {
                exit(0); //isws dn arkei

            } else if (function == 4) {
                doShutdown(pathwriteClientServer, path);
                exit(0);

            }

        }
    } else {//alliws ean dn yparxei o server, ftiaxnei board kai pipes
        if (makeDirectoryBoard(path) != 1) {
            exit(1);
        }

        if (makePipesBoard(path, pathwriteClientServer, pathreadClientServer, pathwriteServerClientpost, pathreadServerClientpost) == 0) {
            exit(1);
        }
        pid_t f;
        if ((f = fork()) != 0) {//parent, returns to parent the childs id
            if(f == -1){
                perror("Error: ");
                exit(1);
            }
            signal(SIGINT, SIG_IGN);
            char str[8000];
            char id[50];
            char name[500];
            int function = 0;
            printf("Type client commands:\n");
            fflush(stdout);
            while (1) {//omoiws me apo panw
                if(fgets(str, sizeof (str), stdin)== NULL){
                    exit(1);
                }
                function = doReadfromInput(str, id, name);
                if (function == 0) {
                    continue;
                } else if (function == 1) {
                    doCreateChannel(path, id, name, pathwriteClientServer);
                    doReadfromServer(pathreadClientServer, path);
                } else if (function == 2) {
                    doGetMessage(path, id, pathwriteClientServer);
                    doReadfromServer(pathreadClientServer, path);
                } else if (function == 3) {
                    exit(0); //isws dn arkei

                } else if (function == 4) {
                    doShutdown(pathwriteClientServer, path);
                    exit(0);

                }

            }
        } else {//child, return to child zero
            signal(SIGINT, SIG_IGN);
            signed int server_id = getpid();
            keepServerID(path, server_id);
            struct channelList* clist = NULL;
            int i = 0;
            fd_set base_set;

            FD_ZERO(&base_set);

            int fd1, fd2, fd3, fd4;//anoigoume ta fds gia tn select
            if ((fd1 = open(pathwriteClientServer, O_RDWR)) < 0) {
                perror("Error: ");
                exit(1);
            }
            if ((fd2 = open(pathreadClientServer, O_RDWR)) < 0) {
                perror("Error: ");
                exit(1);
            }
            if ((fd3 = open(pathwriteServerClientpost, O_RDWR)) < 0) {
                perror("Error: ");
                exit(1);
            }
            if ((fd4 = open(pathreadServerClientpost, O_RDWR)) < 0) {
                perror("Error: ");
                exit(1);
            }

            FD_SET(fd1, &base_set);
            FD_SET(fd3, &base_set);

            while (1) {
                fd_set workingset = base_set;

                if (select(FD_SETSIZE, &workingset, NULL, NULL, NULL) < 0) {
                    perror("select");
                    exit(EXIT_FAILURE);
                }

                if (FD_ISSET(fd1, &workingset)) {
                    if((i = doReadfromClient(&clist, fd1, fd2)) == 0){
                        exit(1);
                    }
                   
                }

                if (FD_ISSET(fd3, &workingset)) {
                    if(doReadfromClientPost(path, &clist, fd3, fd4) == 0){
                        exit(1);
                    }
                }
                
                if(i == 99){//ean prokeitai gia shutdown, break kai kleinw ta fds
                    break;
                }
            }
            
            if (close(fd1) != 0) {
                perror("Error: ");
                exit(1);
            }

            if (close(fd2) != 0) {
                perror("Error: ");
                exit(1);
            }

            if (close(fd3) != 0) {
                perror("Error: ");
                exit(1);
            }

            if (close(fd4) != 0) {
                perror("Error: ");
                exit(1);
            }
            //diagrafei ths plhrofories tou board, se dhlwsh oti dn yparxei server
            deleteDirectoryContent(path, pathwriteClientServer, pathreadClientServer, pathwriteServerClientpost, pathreadServerClientpost);
            exit(EXIT_SUCCESS);
        }
    }

    return 1;
}