#include <stdio.h>
#include <string.h>
#include <stdlib.h>


void pathCheckBoard(char* path, int argc, char** argv);
int pipesExistBoard(char* path, char* pathwriteClientServer, char* pathreadClientServer, char* pathwriteServerClientpost, char* pathreadServerClientpost);
int makeDirectoryBoard(char* path);
int makePipesBoard(char* path, char* pathwriteClientServer, char* pathreadClientServer, char* pathwriteServerClientpost, char* pathreadServerClientpost);
int serverExistsBoard(char* path);