#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <ctype.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

void pathCheckBoard(char* path, int argc, char** argv){//apla vgazei tn allagh grammhs
    
    strncpy(path, argv[1], strlen(argv[1]));
    
    if(path[strlen(argv[1])-1] == '\n'){
        path[strlen(argv[1])-1] = '\0';
    }
    else{
        path[strlen(argv[1])] = '\0';
    }
}

int serverExistsBoard(char* path){//elegxei ean yparxei to arxeio pou dhmiourgeitai mono ean ksekinhsei server ki diagrafetai monos to shutdown
    struct stat buffer;
    char* filename = "/server_ID";
    char filename_path[strlen(path) + strlen(filename)+1];
    strncpy(filename_path, path, strlen(path));
    filename_path[strlen(path)] = '\0';
    strncat(filename_path, filename, strlen(filename));
    if(stat(filename_path, &buffer) == 0){//elegxos ean yparxei arxeio me to path
        return 0;
    }
    else{
        return 1;
    }
}

int pipesExistBoard(char* path, char* pathwriteClientServer, char* pathreadClientServer, char* pathwriteServerClientpost, char* pathreadServerClientpost){
    struct stat buffer;
    
    //original pipenames
    char* pipename1 = "/writeClientServer";
    char* pipename2 = "/readClientServer";
    char* pipename3 = "/writeServerClientpost";
    char* pipename4 = "/readServerClientpost";
    
    
    //enallaktikh onomasia pipies, afairea to 1o kai 3o gramma enallaksa gia na mn desmefsw neo pinaka allou megethous..
    char* pipename1w = "/riteClientServer"; 
    char* pipename2a = "/redClientServer";
    char* pipename3w = "/riteServerClientpost";  
    char* pipename4a = "/redServerClientpost";
   
    
    //path array original kai alternative names
    char pipename_path1[strlen(path) + strlen(pipename1)+1];
    char pipename_path1w[strlen(path) + strlen(pipename1w)+1];
    
    char pipename_path2[strlen(path) + strlen(pipename2)+1];
    char pipename_path2a[strlen(path) + strlen(pipename2a)+1];
    
    char pipename_path3[strlen(path) + strlen(pipename3)+1];
    char pipename_path3w[strlen(path) + strlen(pipename3w)+1];
    
    char pipename_path4[strlen(path) + strlen(pipename4)+1];
    char pipename_path4a[strlen(path) + strlen(pipename4a)+1];
    
    
    strncpy(pipename_path1, path, strlen(path));
    pipename_path1[strlen(path)] = '\0';
    strncat(pipename_path1, pipename1, strlen(pipename1));
    
    strncpy(pipename_path1w, path, strlen(path));
    pipename_path1w[strlen(path)] = '\0';
    strncat(pipename_path1w, pipename1w, strlen(pipename1w));
    
    strncpy(pipename_path2, path, strlen(path));
    pipename_path2[strlen(path)] = '\0';
    strncat(pipename_path2, pipename2, strlen(pipename2));
    
    strncpy(pipename_path2a, path, strlen(path));
    pipename_path2a[strlen(path)] = '\0';
    strncat(pipename_path2a, pipename2a, strlen(pipename2a));
    
    strncpy(pipename_path3, path, strlen(path));
    pipename_path3[strlen(path)] = '\0';
    strncat(pipename_path3, pipename3, strlen(pipename3));
    
    strncpy(pipename_path3w, path, strlen(path));
    pipename_path3w[strlen(path)] = '\0';
    strncat(pipename_path3w, pipename3w, strlen(pipename3w));
    
    strncpy(pipename_path4, path, strlen(path));
    pipename_path4[strlen(path)] = '\0';
    strncat(pipename_path4, pipename4, strlen(pipename4));
    
    strncpy(pipename_path4a, path, strlen(path));
    pipename_path4a[strlen(path)] = '\0';
    strncat(pipename_path4a, pipename4a, strlen(pipename4a));
    
    int i = 0;
    if(stat(pipename_path1, &buffer) != 0){// akolouthei idia logikh sta parakatw, dld, ean yparxei pipe me to original name h ean yparxei me to alternative
        if(stat(pipename_path1w, &buffer) != 0){//kai gia kathe name vriskoume to path
            return 1;
        }
        else{
            strncpy(pathwriteClientServer, pipename_path1w, strlen(pipename_path1w));
            pathwriteClientServer[strlen(pipename_path1w)] = '\0';
            i++;
        }    
    }
    else{
        strncpy(pathwriteClientServer, pipename_path1, strlen(pipename_path1));
        pathwriteClientServer[strlen(pipename_path1)] = '\0';
        i++;
    }
    
    
    if(stat(pipename_path2, &buffer) != 0){
        if(stat(pipename_path2a, &buffer) != 0){
            return 1;
        }
        else{
            strncpy(pathreadClientServer, pipename_path2a, strlen(pipename_path2a));
            pathreadClientServer[strlen(pipename_path2a)] = '\0';
            i++;
        }
    }
    else{
         strncpy(pathreadClientServer, pipename_path2, strlen(pipename_path2));
         pathreadClientServer[strlen(pipename_path2)] = '\0';
         i++;
    }
    
    if(stat(pipename_path3, &buffer) != 0){
        if(stat(pipename_path3w, &buffer) != 0){
            return 1;
        }
        else{
            strncpy(pathwriteServerClientpost, pipename_path3w, strlen(pipename_path3w));
            pathwriteServerClientpost[strlen(pipename_path3w)] = '\0';
            i++;
        }
    }
    else{
        strncpy(pathwriteServerClientpost, pipename_path3, strlen(pipename_path3));
        pathwriteServerClientpost[strlen(pipename_path3)] = '\0';
        i++;
    }
    
    if(stat(pipename_path4, &buffer) != 0){
        if(stat(pipename_path4a, &buffer) != 0){
            return 1;
        }
        else{
            strncpy(pathreadServerClientpost, pipename_path4a, strlen(pipename_path4a));
            pathreadServerClientpost[strlen(pipename_path4a)] = '\0';
            i++;
        }
    }
    else{
        strncpy(pathreadServerClientpost, pipename_path4, strlen(pipename_path4));
        pathreadServerClientpost[strlen(pipename_path4)] = '\0';
        i++;
    }
    
    if(i == 4){
        return 0;
    }
    return 1;
}

int makeDirectoryBoard(char* path){
    
    char tmp[strlen(path)+1];
    strncpy(tmp, path, strlen(path));
    tmp[strlen(path)] = '\0';
    
    
    DIR* mydir;
    char temp[strlen(path)+1];
    temp[0] = '\0';
    int i = 0;
    char* token = strtok(tmp, "/");//spame to path ana / kai sta tokens exoume ta names
    while(token != NULL){
        strncat(temp, "/", 1);//kratame stn temp to path kathws to xtizoume, prostetwdas /
        strncat(temp, token, strlen(token));// kai name meta to /
        if((mydir = opendir(temp)) == NULL){//epixeiroume na anoiksoume ton catalog me path temp
            printf("No directory with path:" "%s" "\n", temp);
            fflush(stdout);
            printf("Creating directory...\n");
            fflush(stdout);
            if(mkdir(temp, 0777) == -1){
                perror("Error in makeDirectory: ");
                printf("\n");
                fflush(stdout);
                exit(1);
            }
            printf("Created directory\n");
            fflush(stdout);
        }
        else if(mydir != NULL){//ean yparxei hdh
            printf("Directory with path:" "%s " "already exists\n", temp);
            fflush(stdout);
            if(closedir(mydir) != 0){
                perror("Error: ");
                exit(1);
            }
        }
        else{
            return 0;
        }
        token = strtok(NULL, "/");
        i++;
    }
    return 1;
}        

int makePipesBoard(char* path, char* pathwriteClientServer, char* pathreadClientServer, char* pathwriteServerClientpost, char* pathreadServerClientpost){
    
    struct dirent* mycatalog;
    DIR* mycatalog_p;
    int flag1 = 0;
    int flag2 = 0;
    int flag3 = 0;
    int flag4 = 0;
    
    if(((mycatalog_p) = opendir(path)) == NULL){//anoigoume tn katalogo
        perror("Error: ");
        fflush(stdout);
        exit(1);
    } 
    else{
        while((mycatalog = readdir(mycatalog_p)) != NULL){//diavazoume ta arxeia tou kai eksetazoume ean exoun to original name twn pipes
            if(!strcmp(mycatalog->d_name, "writeClientServer")){
                flag1 = 1;
            }
            
            if(!strcmp(mycatalog->d_name, "readClientServer")){
                flag2 = 1;
            }
            
            if(!strcmp(mycatalog->d_name, "writeServerClientpost")){
                flag3 = 1;
            }
            
            if(!strcmp(mycatalog->d_name, "readServerClientpost")){
                flag4 = 1;
            }
        }
    }

    if(closedir(mycatalog_p) != 0){
        perror("Error: ");
        exit(1);
    }
    
    strncpy(pathwriteClientServer, path, strlen(path));
    strncpy(pathreadClientServer, path, strlen(path));
    strncpy(pathwriteServerClientpost, path, strlen(path));
    strncpy(pathreadServerClientpost, path, strlen(path));
    
    
    pathwriteClientServer[strlen(path)] = '\0';
    pathreadClientServer[strlen(path)] = '\0';
    pathwriteServerClientpost[strlen(path)] = '\0';
    pathreadServerClientpost[strlen(path)] = '\0';
    
    //ean ta original names einai piasmena xrhshmopoioume ta alternative
    if(flag1 == 1){
        strncat(pathwriteClientServer, "/riteClientServer", strlen("/riteClientServer"));
    }
    else{
        strncat(pathwriteClientServer, "/writeClientServer", strlen("/writeClientServer"));
    }
    if(flag2 == 1){
        strncat(pathreadClientServer, "/redClientServer", strlen("/redClientServer"));
    }
    else{
        strncat(pathreadClientServer, "/readClientServer", strlen("/readClientServer"));
    }

    if(flag3 == 1){
        strncat(pathwriteServerClientpost, "/riteServerClientpost", strlen("/riteServerClientpost"));
    }
    else{
        strncat(pathwriteServerClientpost, "/writeServerClientpost", strlen("/writeServerClientpost"));
    }
    
    if(flag4 == 1){
        strncat(pathreadServerClientpost, "/redServerClientpost", strlen("/redServerClientpost"));
    }
    else{
       strncat(pathreadServerClientpost, "/readServerClientpost", strlen("/readServerClientpost")); 
    }
    
    
    if(mkfifo(pathwriteClientServer, 0666) == -1){
        if(errno!= EEXIST){
            perror("Error in makePipes, for writing Client-Server: ");
            printf("\n");
            fflush(stdout);
            exit(1);
        }
    }
    
    if(mkfifo(pathreadClientServer, 0666) == -1){
        if(errno!= EEXIST){
            perror("Error in makePipes, for reading Client-Server: ");
            printf("\n");
            fflush(stdout);
            exit(1);
        }
    }
    
    if(mkfifo(pathwriteServerClientpost, 0666) == -1){
        if(errno!= EEXIST){
            perror("Error in makePipes, for writing Server-Clientpost: ");
            printf("\n");
            fflush(stdout);
            exit(1);
        }
    }
    
    if(mkfifo(pathreadServerClientpost, 0666) == -1){
        if(errno!= EEXIST){
            perror("Error makePipes, for reading Server-Clientpost: ");
            printf("\n");
            fflush(stdout);
            exit(1);
        }
    }

    return 1;

}