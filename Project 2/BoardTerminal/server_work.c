
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <ctype.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include "server_structs.h"
#include "server_channelList.h"
#include "server_fileList.h"
#include "server_messageList.h"
#include "server_work.h"

//-----client----server----//

void keepServerID(char* path, signed int server_id){//kratame to pid tou server proc gia na kseroume ean yparxei
    int fd;
    char* filename = "/server_ID";
    char filename_path[strlen(path) + strlen(filename)+1];
    strncpy(filename_path, path, strlen(path));
    filename_path[strlen(path)] = '\0';
    strncat(filename_path, filename, strlen(filename));
    
    if((fd = (open(filename_path, O_CREAT | O_WRONLY, 0644))) < 0){
         perror("Error: ");
         exit(1);
    }
    char id[50];
    int bytes;
    if(sprintf(id, "%d", server_id)<0){//adigrafoume to id sto file p ftiaksame
        perror("Error in sprintf");
        exit(1);
    }
    if((bytes = write(fd, id, strlen(id))) == -1){
        perror("Error: ");
        exit(1);
    }
    
    if(bytes!= strlen(id)){
        printf("Error in write\n");
        exit(1);
    }
    if(close(fd) != 0){
        perror("Error: ");
        exit(1);
    }
}

int doReadfromClient(struct channelList** clist, int request_fd, int response_fd){//diavazei apo tn client 
    int bytes;
    char command[8000];
    char name[500];
    command[0] = '\0';
    char data[1];
    char data1[2]; 
    data[0] = '\0';
    data1[0] ='\0';
    do{  
        if((bytes = read(request_fd, data, 1)) ==  -1){
          perror("Error: ");
          exit(1);  
        } 
 
        if(data[0]!='#'){
            data1[0] = data[0];
            data1[1] = '\0';
            strncat(command, data1, 1);
        }

    }while(data[0]!= '#');

    int i = 1;
    int id;
    if(!strcmp(command, "SH")){//shmainei oti einai shutdown
        i = 1;  
    }
    else{
        char* token = strtok(command, "-");

        while(token!=NULL){

             if(i == 2){//ean diavasei id mono tote einai getmessages id
               id = atoi(token);
            }
             else if(i == 3){//ean diavasei kai name einai createchannel id name
                strncpy(name, token, strlen(token));
                name[strlen(token)] = '\0';
            }
            token = strtok(NULL, "-");
            if(token != NULL){
                i++;
            }
        }
    }   
    
    return doResponseToClient(i, id, clist, name, response_fd);
}
    
int doResponseToClient(int i, int id, struct channelList** clist, char* name, int response_fd){ 
   
    if(i == 1){
        deleteAll(clist);//delete oles tis domes
        return 99;   
        
    }
    
    
    else if(i == 2){//shmainei oti einai h getmessages <id>
        char message[8000];
        char message1[8000];
        message[0]= '\0';
        message1[0]= '\0';
        int flag = 0;
        //////////////--------------message response------------------////
        
        if(((*clist) != NULL)&&((*clist)->start!= NULL)){//ean yparxoun channels
            struct channelElement* cursor = (*clist)->start;
            while(cursor!= NULL){
                if((cursor->id == id)&&(cursor->mList!=NULL)){//ean vroume to channel id me mesasage list
                     flag = 1;
                    struct messageElement* cursorm = cursor->mList->start;
                    strncat(message, "M-", strlen("M-"));//ftiaxnoume to message
                    while(cursorm!= NULL){
                        //M-message-message-message-......-message#
                        strncat(message, cursorm->message, strlen(cursorm->message));
                        if(cursorm->next != NULL){
                             strncat(message, "-", 1);
                            
                        }
                        cursorm = cursorm->next;
                    }
                    
                    deleteMessageList(clist, id);//delete th msg list
                    break;
                }
                cursor = cursor->next;
            }
        }
        //////////////--------------file response------------------////
        

        if(((*clist) != NULL)&&((*clist)->start!= NULL)){//omoia me message list
            struct channelElement* cursor = (*clist)->start;
            while(cursor!= NULL){
                if((cursor->id == id)&&(cursor->fList!= NULL)){
                    if(flag == 1){//ean eixe messagelist
                        flag = 3;
                    }
                    else{//ean dn eixe
                        flag = 2;
                    }
                    struct fileElement* cursorf = cursor->fList->start;
                    strncat(message1, "F-", strlen("F-"));
                    while(cursorf!= NULL){
                        
                       ////F-filename-filecontent-filename-filecontent#
                        strncat(message1, cursorf->pathfile, strlen(cursorf->pathfile));  
                        strncat(message1, "-", 1);
                        strncat(message1, cursorf->filecontent, strlen(cursorf->filecontent));
                         if(cursorf->next != NULL){
                             strncat(message1, "-", 1);
                            
                        }
                        cursorf = cursorf->next;
                    }
                    deleteFileList(clist, id);
                    break;
                }
                cursor = cursor->next;
            }
        }
        

        /////---------create file or message or neither response-------////
        
        if(flag == 1){//ean exei mono message list
            int bytes;
            int length = strlen(message);
            message[length] = '#';
            if((bytes = write(response_fd, message, length+1)) ==  -1){
                perror("Error: ");
                exit(1);
            }

            if(bytes!= length+1){
                printf("Error in write\n");
                exit(1);
            } 
            
            return 1;
            
        }
        else if(flag == 2){//ean exei mono filelist
            int bytes;
            int length = strlen(message1);
            message1[length] = '#';

            if((bytes = write(response_fd, message1, length+1)) ==  -1){
                perror("Error: ");
                exit(1);
            }

            if(bytes!= length+1){
                printf("Error in write\n");
                exit(1);
            }  
            return 1;
        }
        else if(flag == 3){//ean exie kai msg list kai filelist
            int bytes;
            strncat(message, "&", 1); //diaxwristiko &
            
            strncat(message, message1, strlen(message1));
            int length = strlen(message);
            message[length] = '#';
            
            if((bytes = write(response_fd, message, length+1)) ==  -1){
                perror("Error: ");
                exit(1);
            }

            if(bytes!= length+1){
                printf("Error in write\n");
                exit(1);
            }
            return 1;
        }
        
        else if(flag == 0){//alliws
            int bytes;
            int length = strlen("Channel has no data#");
            if((bytes = write(response_fd, "Channel has no data#", length)) ==  -1){
                perror("Error: ");
                exit(1);
            }

            if(bytes!= length){
                printf("Error in write\n");
                exit(1);
            }
            return 1;
        } 
    }
    
    //////////////--------------channel create response ------------------////
    
    else if(i == 3){//shmainei oti einai h createchannel id name
        if(createChannelElement(clist, id, name)){

            int bytes;
            int length = strlen("SUCCESS#");//se epityxia
            if((bytes = write(response_fd, "SUCCESS#", length)) ==  -1){
                perror("Error: ");
                exit(1);
            }

            if(bytes!= length){
                printf("Error in write\n");
                exit(1);
            }
  
            return 1;
        }
        else{//ean dn yparxei to kanali
            int bytes;
            int length = strlen("Channel already Exists#");
            if((bytes = write(response_fd, "Channel already Exists#", length)) ==  -1){
                perror("Error: ");
                exit(1);
            }

            if(bytes!= length){
                printf("Error in write\n");
                exit(1);
            }
            return 1;
         }
    }
    return 0;
}


//-----server-----clientpost-------///

int doReadfromClientPost(char* path, struct channelList** clist, int request_fd, int response_fd){ 
    int bytes;
    char command[8000];
    command[0] = '\0';
    char name[500];
    name[0] = '\0';
    char filecontent[3000];///
    filecontent[0] = '\0';
    char data[1];
    char data1[2];
    data[0] = '\0';
    data1[0] ='\0';
    do{ 
       
        if((bytes = read(request_fd, data, 1)) ==  -1){//diavazei apo to pipe sto opoio grafei o boardpost
            perror("Error: ");
            exit(1);
        }
        
        if(data[0]!='#'){
            data1[0] = data[0];
            data1[1] = '\0';
            strncat(command, data1, 1);
        }
   
    }while(data[0]!= '#');
  
    
    int i = 1;
    int id;
    if(!strcmp(command, "LIST-")){//ean dextei edolh list
        i = 0;
        id = -1;
        return doResponseToClientPost(path, i, id, clist, name, filecontent, response_fd);
    }

       
    
    char* token = strtok(command, "-");
    
    while(token!=NULL){
        
        if(i == 1){//ean einai message
           id = atoi(token);
        }
        else if(i == 2){//ean einai message einai to message to idio, ean einai file to pathname
            strncpy(name, token, strlen(token));//ean 
            name[strlen(token)] = '\0';
        }
        else if(i == 3){//ean einai file exei k content
            strncpy(filecontent, token, strlen(token)+1);
            
        }
        token = strtok(NULL, "-");
        if(token != NULL){
            i++;
        }
    }
    
    return doResponseToClientPost(path, i, id, clist, name, filecontent, response_fd);
}

int doResponseToClientPost(char* path, int i, int id, struct channelList** clist, char* name, char* filecontent, int response_fd){ 
    

    char* response;
    if(i == 3){//einai h edolh send id file filecontent
        if(createFileElement(path, clist, id, filecontent, name)){//dhmourgei file element
            response = "SUCCESS file#";   
        }
        else{
            response = "Channel doesn't exist#";
        }
        int bytes;
        int length = strlen(response);//analoga me to response epistrefei message stn client post
        if((bytes = write(response_fd, response, length)) ==  -1){
            perror("Error: ");
            exit(1);
        }

        if(bytes!= length){
            printf("Error in write\n");
            exit(1);
        } 
        
        return 1;
    }
    else if(i == 2){//einai h write id string
        if(createMessageElement(clist, id, name)){//dhmiourgei message element kai akoloutheitai omoia logikh
            response = "SUCCESS message#";   
        }
        else{
            response = "Channel doesn't exist#";
        }
        int bytes;
        int length = strlen(response);
        if((bytes = write(response_fd, response, length)) ==  -1){
            perror("Error: ");
            exit(1);
        }

        if(bytes!= length){
            printf("Error in write\n");
            exit(1);
        }
        return 1;
    }
    else if(i == 0){//ean einai h edolh list
        if(((*clist) != NULL)&&((*clist)->start!= NULL)){
            struct channelElement* cursor = (*clist)->start;
            char message[8000];
            message[0]= '\0';
            char id[50];
            id[0]= '\0';
            strncat(message, "L-", strlen("L-"));
            
            while(cursor!= NULL){//L-name-id-name-id# einai afto p stelnetai stn client post
                strncat(message, cursor->name, strlen(cursor->name));
                strncat(message, "-", 1);//
                
                sprintf(id, "%d", cursor->id);
                strncat(message, id, strlen(id));
                if(cursor->next != NULL){
                     strncat(message, "-", 1);

                }
                cursor = cursor->next;
                id[0]= '\0';
            }
            int bytes;
            int length = strlen(message);
            message[length] = '#';

            if((bytes = write(response_fd, message, length+1)) ==  -1){
                perror("Error: ");
                exit(1);
            }

            if(bytes!= length+1){
                printf("Error in write\n");
                exit(1);
            }
            
            return 1;
        }
        else{//diaforetika
            int bytes;
            if((bytes = write(response_fd, "No Channels#", strlen("No Channels#"))) ==  -1){
                    perror("Error: ");
                    exit(1);
                }

                if(bytes!= strlen("No Channels#")){
                    printf("Error in write\n");
                    exit(1);
                }
                return 1;
            }
    }
    return 0;
}

void deleteDirectoryContent(char* path, char* pathwriteClientServer, char* pathreadClientServer, char* pathwriteServerClientpost, char* pathreadServerClientpost){
    
    //diagrafei ta pipes
    
    unlink(pathwriteClientServer);
    unlink(pathreadClientServer);
    unlink(pathwriteServerClientpost);
    unlink(pathreadServerClientpost);
    
    char* server = "/server_ID";

    struct dirent* mycatalog;
    DIR* mycatalog_p;
    
    if(((mycatalog_p) = opendir(path)) == NULL){
        perror("Error: ");
        fflush(stdout);
        exit(1);
    } 
    
    while((mycatalog = readdir(mycatalog_p)) != NULL){//diagrafei to arxeio p dhlwnei tn yparksh server
        if(!strcmp(mycatalog->d_name, "server_ID")){
            strncat(path, server, strlen(server));
            unlink(path);    
        }
  
    }
    if(closedir(mycatalog_p) != 0){
        perror("Error: ");
        exit(1);
    }
}