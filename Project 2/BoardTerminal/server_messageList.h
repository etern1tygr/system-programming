#include <stdio.h>
#include <string.h>
#include <stdlib.h>


int createMessageList(struct channelList** clist, int id);
int createMessageElement(struct channelList** clist, int id, char* message);
int deleteMessageList(struct channelList** clist, int id);
