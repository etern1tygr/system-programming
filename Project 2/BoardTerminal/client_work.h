#include <stdio.h>
#include <string.h>
#include <stdlib.h>


//---client----server-----//

int doReadfromInput(char* str, char* id, char* name);
void doShutdown(char* pathwriteClientServer, char* path);
void doCreateChannel(char* path, char* cid, char* name, char* pathwriteClientServer);
void doGetMessage(char* path, char* gid, char* pathwriteClientServer);
void doReadfromServer(char* pathreadClientServer, char* path);
void putinfile(char* path, char* filecontent, char* filename);


