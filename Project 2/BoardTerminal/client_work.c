#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <ctype.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include "client_work.h"

int doReadfromInput(char* str, char* id, char* name){//epistrefoume analoga me to poia edolh einai, ena sigekrimeno int stn main
    char command[strlen(str)+1];//static pinaka n chars +1 gia null terminator
    strncpy(command, str, (strlen(str)));//copy to string stn static
    if(command[strlen(str)-1] == '\n'){
        command[strlen(str)-1] = '\0';
    }
    else{
        command[strlen(str)] = '\0';
    }
    char* token = token = strtok(command, " ");
    
    if(!strcmp(command, "createchannel")){//an einai h createchannel
        if(token == NULL){// a dn parei orismata mhnhma lathous
            printf("failure: No arguments at all\n");
            return 0;
        }
        int token_length;
        int flag = 0;
        int flag1 = 0;
        int number = 0;
        int notnumber = 0;
        token = strtok(NULL, " ");
        while (token != NULL){
            token_length = strlen(token);
            char digits[token_length];
            strncpy(digits, token, token_length);
            int i = 0;
            while(i<= token_length-1){//elegxos ean proketai gia digits
                if(isdigit(digits[i]) == 0){
                    notnumber++;//ean dn einai arithmos
                    flag = 1;
                    break;
                }
                i++;
            }
            
            if(flag == 0){//ean einai
                number++;
            }
            
            if((notnumber == 0)&&(number == 1)){
                 //theloume mono ena arithmo
                strncpy(id, token, token_length+1);
            }
            
            else if((notnumber == 1)&&(number == 1)){
                flag1 = 1;
                strncpy(name, token, token_length+1);//theloyme meta ton ENA arithmo, char*
                
            }
            else{
                printf("Wrong argument sequence\n");
                return 0;
            }
            token = strtok(NULL, " ");
            flag = 0;
        }
        if(flag1!= 1){
            printf("Wrong argument sequence\n");
            return 0;
        }
        return 1;
    }
    
    
    else if(!strcmp(command, "getmessages")){
        
        token = strtok(NULL, " ");
        if(token == NULL){// a dn parei orismata mhnhma lathous
            printf("failure: No arguments at all\n");
            return 0;
        }
        int token_length;
        int flag = 0;
        int flag1 = 0;
        int number = 0;
        int notnumber = 0;
        while (token != NULL){

            token_length = strlen(token);
            char digits[token_length];
            strncpy(digits, token, token_length);
            int i = 0;
            while(i<= token_length-1){//elegxos ean proketai gia digits
                if(isdigit(digits[i]) == 0){
                    notnumber++;
                    flag = 1;
                    break;
                }
                i++;
            }
            
            if(flag == 0){
                number++;
            }
            
            if((notnumber == 0)&&(number == 1)){
                 //theloume mono ena arithmo
                strncpy(id, token, token_length+1);
                flag1 = 1;
            }
            
            else{
                printf("Wrong argument sequence\n");
                return 0;
            }
            token = strtok(NULL, " ");
            flag = 0;
        }
        if(flag1!= 1){
            printf("Wrong argument sequence\n");
            return 0;
        }
        return 2;
    }
    
    
    else if(!strcmp(command, "exit")){
        token = strtok(NULL, " ");
        if(token == NULL){// a dn parei orismata 
            return 3;
        }
        else{
            printf("Wrong argument sequence\n");
            return 0;
        }
    }
    
    else if(!strcmp(command, "shutdown")){
        token = strtok(NULL, " ");
        if(token == NULL){// a dn parei orismata 
            return 4;
        }
        else{
            printf("Wrong argument sequence\n");
            return 0;
        } 
    }
    else{
        printf("Wrong argument sequence\n");
        return 0;
    }
    
}

void doShutdown(char* pathwriteClientServer, char* path){//kleinei tn server, client, pipes alla dn svhnei ta files sto directory, pera apo afto p dhlwnei oti yparxei server
     int fd;

    if((fd = open(pathwriteClientServer, O_WRONLY, 0644)) < 0){//anoigoume to pipe egraffhs
         perror("Error: ");
         exit(1);
    }

    int bytes;
    char message[3];//SH# einai h edolh kai # termatikos char
    strncpy(message, "SH#", 3);
    
    if((bytes = write(fd, message, 3)) ==  -1){
        perror("Error: ");
        exit(1);
    }

    
    if(bytes!= 3){
        printf("Error in write\n");
        exit(1);
    } 
    
    if(close(fd)!= 0){
        perror("Error: ");
        exit(1);
    }
    
    
}

void doCreateChannel(char* path, char* cid, char* name, char* pathwriteClientServer){ //dhmiourgei kanali me id kai name
    int fd;
    if((fd = open(pathwriteClientServer, O_WRONLY, 0644)) < 0){//anoigei to pipe egraffhs
         perror("Error: ");
         exit(1);
    }
    
    int bytes;
    int length_id = strlen(cid);
    int length_name = strlen(name);

    char id_space[length_id+2];
    char message[length_id+length_name+9];//CREATE-id-name# einai h edolh p stelnoume kai # termatikos char


    strncpy(id_space, cid, length_id);
    id_space[length_id] = '-';
    id_space[length_id+1] = '\0';
    strncpy(message, "CREATE-", 8);
    strncat(message, id_space, length_id+2);
    strncat(message, name, length_name);
    message[length_id+length_name+8] = '#';

    if((bytes = write(fd, message, length_id+length_name+9)) ==  -1){
        perror("Error: ");
        exit(1);
    }

    if(bytes!= length_id+length_name+9){
        printf("Error in write\n");
        exit(1);
    }

    if(close(fd)!= 0){
        perror("Error: ");
        exit(1);
    }
}

void doGetMessage(char* path, char* gid, char* pathwriteClientServer){//fenrei arxeia kai messages apo to kanali id
    
    int fd;

    if((fd = open(pathwriteClientServer, O_WRONLY, 0644)) < 0){//anoigei to pipe egraffhs
         perror("Error: ");
         exit(1);
    }

    int bytes;
    int length = strlen(gid);

    int length_mesg = length+5;
    char message[length_mesg];//GET-id# h edolh p stelnoume kai # termatikos char
    

    strncpy(message, "GET-", 5);
    strncat(message, gid, length);
    message[length_mesg-1] = '#';
    
    if((bytes = write(fd, message, length_mesg)) ==  -1){
        perror("Error: ");
        exit(1);
    }

    if(bytes!= length_mesg){
        printf("Error in write\n");
        exit(1);
    } 

    if(close(fd)!= 0){
        perror("Error: ");
        exit(1);
    }
}

void doReadfromServer(char* pathreadClientServer, char* path){ //diavazei apo to pipe read
    int fd;
    
    
    if((fd = open(pathreadClientServer, O_RDONLY, 0644)) < 0){//to anoigei
         perror("Error: ");
         exit(1);
    }
   
   
    int bytes;
    char message[8000];//generic static array gia to mhnyma p erxetai
    message[0] = '\0';
    char filemessage[4000]; //edw krataw mono ta content twn file
    filemessage[0] = '\0';
    char Mmessage[4000]; // edw mono twn messages
    Mmessage[0] = '\0';
    
    char data[1];
    char data1[2];
    data[0] = '\0';
    data1[0] = '\0';
    int flag = -1;
    do{  
        if((bytes = read(fd, data, 1)) ==  -1){
            perror("Error: ");
            exit(1);
        }
        
        if(data[0]!='#'){//oso dn exoume diavasei termatiko xarakthra
            data1[0] = data[0];
            data1[1] = '\0';
            strncat(message, data1, 1);
            if(data[0] == 'M'){//ean ksekinaei apo M tote shmainei einai message
                flag = 0;
            }
            else if(data[0] == '&'){//ean exei & shmainei oti mallom einai message kai file
                flag = 1;
                 
            }
            else if((data[0] == 'F')&&(flag == 1)){//ean einai F kai prin kapou diavasame & shmainei einai message kai file
                flag = 2;
            }
            else if((data[0] == 'F')&&(flag == -1)){//ean dn diavasame & einai file
                flag = 3;
            }
            
            if(flag >1){//analoga me ta flags apothikevoume ta data stous adistoixous pinakes
                strncat(filemessage, data1, 1);
            }
            else if(flag == 0){
                strncat(Mmessage, data1, 1);
            }
        }

   
    }while(data[0]!= '#');
    
    if(!strcmp(message, "SUCCESS")){//ean egine createchannel
        puts(message); 
    }
    else if(!strcmp(message, "Channel already Exists")){//ean yphrxe hdh to channel
        puts(message);
    }
    else if(!strcmp(message, "Channel has no data")){//ean dn yparxei to channel
        puts(message);
    }
    //gia shutdown epeleksa na mn epistrefei kt
    
    else if(flag == 3){//F-filename-content-filename-content-filename-content#, einai to minima p erxetai
        int i = 0;
        char filename[500];
        char filecontent[3000];
        filename[0] = '\0';
        filecontent[0] = '\0';
        char* token = strtok(filemessage, "-");
        while(token!= NULL){
            if(i == 1){//ksekiname me 1 dioti dn theloume na asxolhthoume me to F
                strncat(filename, token, strlen(token));
            }
            else if(i == 2){
                strncat(filecontent, token, strlen(token));
                putinfile("/tmp", filecontent, filename);//epilegw na topothethsei o client ta arxeia sto tmp
                if(token!=NULL){
                    filename[0] = '\0';
                    filecontent[0] = '\0';
                }
                
                i = 0;
            }
  
            token = strtok(NULL, "-");
       
            if(token != NULL){
                i++;
            }
        }
    }
    else if(flag == 0){//M-message-message-message#, omoiws to minima p erxetai
        char* token = strtok(Mmessage, "-");
        int i = 0;
        while(token!= NULL){
            if(i>=1){//ektyponoume ta messages
                puts(token);
                 fflush(stdout);
            }
            token = strtok(NULL, "-");
            i++;
        }
    }
    else if(flag == 2){//M-message-message-message&F-file-filecontent-file-filecontent#, omoiws..
        char* tokenm = strtok(Mmessage, "-");
        int y = 0;
        while(tokenm!= NULL){//ektypwnei to kommati me ta messges
            if(y>=1){
                puts(tokenm);
                 fflush(stdout);
            }
            tokenm = strtok(NULL, "-");
            y++;
        }

        int i = 0;
        char filename[500];
        char filecontent[3000];
        filename[0] = '\0';
        filecontent[0] = '\0';
        char* token = strtok(filemessage, "-");
        while(token!= NULL){//meta pairnei tn plhroforia p exoun ta files
            if(i == 1){
                strncat(filename, token, strlen(token));
            }
            else if(i == 2){
                strncat(filecontent, token, strlen(token));
                putinfile("/tmp", filecontent, filename);//ftiaxnei file
                if(token!=NULL){
                    filename[0] = '\0';
                    filecontent[0] = '\0';
                }
                
                i = 0;
                
            }
            token = strtok(NULL, "-");
            i++;
        }
        //token = strtok(NULL, "/");
    }
    if(close(fd)!= 0){
        perror("Error: ");
        exit(1);
    }
}

void putinfile(char* path, char* filecontent, char* filename){
    DIR* mycatalog_p;
    struct dirent* mycatalog;
    char name[500];
    char temp[500];
    temp[0] = '\0';
    if((mycatalog_p = opendir(path)) == NULL){//anoigei to /tmp
        perror("Error: ");
        exit(1);
    } 
    else{
        char* token;
        char filename1[500];
        char filename_temp[500];
        filename_temp[0] = '\0';
        strncpy(filename1, filename, strlen(filename));
        filename1[strlen(filename)] = '\0';//kratame se dummy to filename gia na mn xalasei spo tn strtok
        
        
        token = strtok(filename1, "/");
        while(token!= NULL){
            strncat(filename_temp, token, strlen(token));//spame to path mexri na paroume to filename
            filename_temp[strlen(token)] = '\0';//to kratame se 2o array
            
            token = strtok(NULL, "/");
            if(token!=NULL){
                filename_temp[0] = '\0';
            }
        }
         name[0] = '/';//edw kratame to teliko onoma
         name[1] = '\0';
        strncat(temp, filename_temp, strlen(filename_temp));//3o dummy gia tn strcat...(dn xreizotan)
        strncat(name, filename_temp, strlen(filename_temp));
        while((mycatalog = readdir(mycatalog_p)) != NULL){//spaxnoume ena yparxei file me filename, kai ean yparxei prosthetoume 1 sto filename
           if(!strcmp(mycatalog->d_name, temp)){
               strncat(name, "1", strlen("1"));
               strncat(temp, "1", strlen("1"));
               
            } 
            
        }
        if(closedir(mycatalog_p)){
            perror("Error: ");
            exit(1);
        }
    }

    int fd;
    char filename_path[strlen(path) + strlen(name)+1];//exoume to path kai to teliko name
    strncpy(filename_path, path, strlen(path));//dhmiourgoume to teliko path
    filename_path[strlen(path)] = '\0';
    strncat(filename_path, name, strlen(name));
    
    if((fd = (open(filename_path, O_CREAT | O_WRONLY, 0644))) < 0){//ftiaxnoume t file
         perror("Error: ");
         exit(1);
    }
    int bytes;
    if((bytes = write(fd, filecontent, strlen(filecontent))) == -1){//pername to content
        close(fd);
        perror("Error: ");
        exit(1);
    }
    
    if(bytes!= strlen(filecontent)){
        close(fd);
        printf("Error in write\n");
        exit(1);
    }
    if(close(fd)!= 0){
        perror("Error: ");
        exit(1);
    }
    
}