#include <stdio.h>
#include <string.h>
#include <stdlib.h>

//---client----server-----//

void keepServerID(char* path, signed int server_id);
int doResponseToClient(int i, int id, struct channelList** clist, char* name, int pathreadClientServer); 
int doReadfromClient(struct channelList** clist, int pathwriteClientServer, int pathreadClientServer);
void deleteDirectoryContent(char* path, char* pathwriteClientServer, char* pathreadClientServer, char* pathwriteServerClientpost, char* pathreadServerClientpost);


//---server----boardpost-----//
int doReadfromClientPost(char* path, struct channelList** clist, int pathwriteServerClientpost, int pathreadServerClientpost);
int doResponseToClientPost(char* path, int i, int id, struct channelList** clist, char* name, char* filecontent, int pathreadServerClientpost);

