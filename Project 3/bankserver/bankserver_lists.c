
#include "bankserver_structs.h"
#include <stdio.h>
#include <stdlib.h>


int InTransferListCreate(struct Account* N) {//dhmiourgei kenh lista gia InTransfers gia tn account N
    if (N == NULL) {
        printf("failure: N is NULL\n");
        fflush(stdout);
        return 0;
    }

    if (N->InTransferList == NULL) {//dhmiourgei ti lista
        if ((N->InTransferList = malloc(sizeof (struct ListOfInTransfers))) == NULL) {
            printf("failure: %s InTransferList malloc FAILED \n", N->name);
            exit(1);
        }
        N->InTransferList->Account = N;//thetei tn account, N,  ston opoio anhkei
        N->InTransferList->startInTransfer = NULL; //arxikopoiei tn arxh
        N->InTransferList->lastInTransfer = NULL;//to telos
        N->InTransferList->size = 0;//kai to megethos
        return 1;
    }//alliws exei hdh dhmiourghthei
    return 1;
}

int OutTransferListCreate(struct Account* N) {//dhmiourgei thn kenh lista gia Outtransfers gia account N, omoiws me parapanw
    if (N == NULL) {
        printf("failure: N is NULL\n");
        fflush(stdout);
        return 0;
    }

    if (N->OutTransferList == NULL) {
        if ((N->OutTransferList = malloc(sizeof (struct ListOfOutTransfers))) == NULL) {
            printf("failure: N%s OutTransferList malloc FAILED\n", N->name);
            exit(1);
        }
        N->OutTransferList->Account = N;
        N->OutTransferList->startOutTransfer = NULL;
        N->OutTransferList->lastOutTransfer = NULL;
        N->OutTransferList->size = 0;
        return 1;
    }
    return 1;
}

int InTransferElemCreate(struct Account* N, struct Transfer* newIncoming) {//dhmiourgei ena stoixeio sthn IntransferList tou account N

    if ((N == NULL) || (newIncoming == NULL)) {
        printf("failure: N is NULL or newIncoming Transfer is NULL\n");
        fflush(stdout);
        return 0;
    }

    if (N->InTransferList == NULL) {//an den yparxei h IntransferList, dhmiourghse mia kenh
        InTransferListCreate(N);
    }

    struct InTransferElem* cursor = N->InTransferList->startInTransfer;
    struct InTransferElem* newElem;//dhmiourgei to neo IntransferElement
    if ((newElem = malloc(sizeof (struct InTransferElem))) == NULL) {
        printf("failure: newElem's malloc for %s FAILED\n", N->name);
        fflush(stdout);
        exit(1);
    }

    if (cursor == NULL) {//an den yparxei arxiko Element, dld h IntransferList einai kenh
        newElem->nextInTransfer = NULL; //these ton epomeno tou neou Element ws NULL
        newElem->prevInTransfer = NULL;//ton prohgoumeno se void
        newElem->TransferToCurrentNode = newIncoming;//deixnei stp transfer pou paei stn epomeno account
        N->InTransferList->startInTransfer = newElem;//thes to neo Element ws to arxiko
        N->InTransferList->lastInTransfer = newElem;//ws to teliko
        N->InTransferList->Account = N;//these tn account stn opoio anhkei h lista
        newElem = NULL;
        cursor = NULL;
        return 1;
    }

    while (cursor->nextInTransfer != NULL) {//alliws an den einai kenh
        cursor = cursor->nextInTransfer;//vres to teleftaio != NULL Element
    }

    newElem->nextInTransfer = NULL; //these ton epomeno tou neou Element ws NULL
    newElem->prevInTransfer = cursor; //ton prohgoumeno tou sto teleftaio egyro Element
    newElem->TransferToCurrentNode = newIncoming; //na deixnei stn akmh pou deixnei ston account N(ston eafto tou dld)
    cursor->nextInTransfer = newElem;//these ton epomeno tou egyrou Element sto neo Element
    N->InTransferList->lastInTransfer = newElem; //these ton neo Element ws to teliko
    N->InTransferList->Account = N;
    newElem = NULL;
    cursor = NULL;
    return 1;
}

int OutTransferElemCreate(struct Account* N, struct Transfer* newOutcoming) {//omoiws gia tn dhmiourgia OuttransferElement gia OuttransferLIst gia account N
    if ((N == NULL) || (newOutcoming == NULL)) {
        printf("failure: N is NULL or newOutcoming Transfer is NULL\n");
        fflush(stdout);
        return 0;
    }

    if (N->OutTransferList == NULL) {
        OutTransferListCreate(N);
    }
    struct OutTransferElem* cursor = N->OutTransferList->startOutTransfer;
    struct OutTransferElem* newElem;
    if ((newElem = malloc(sizeof (struct OutTransferElem))) == NULL) {
        printf("failure: newElem's malloc for %s FAILED\n", N->name);
        exit(1);
    }
    if (cursor == NULL) {
        newElem->TransferToNextNode = newOutcoming;
        newElem->nextOutTransfer = NULL;
        newElem->prevOutTransfer = NULL;
        N->OutTransferList->startOutTransfer = newElem;
        N->OutTransferList->lastOutTransfer = newElem;
        N->OutTransferList->Account = N;
        newElem = NULL;
        cursor = NULL;
        return 1;
    }

    while (cursor->nextOutTransfer != NULL) {
        cursor = cursor->nextOutTransfer;
    }
    newElem->TransferToNextNode = newOutcoming;
    newElem->nextOutTransfer = NULL;
    newElem->prevOutTransfer = cursor;
    cursor->nextOutTransfer = newElem;
    N->OutTransferList->lastOutTransfer = newElem;
    N->OutTransferList->Account = N;
    newElem = NULL;
    cursor = NULL;
    return 1;

}

int DeleteInTransferListForN(struct Account* N) {//diagrafei tn IntransferList gia account N, MONO ean den exei kanena stoixeio
    if (N == NULL) {
        printf("failure: N is NULL\n");
        fflush(stdout);
        return 0;
    }

    if (N->InTransferList == NULL) {//an einai hdh NULL, shmainei oti exei ginei deleted prohgoumenos
        return 1;
    }

    if (N->InTransferList->startInTransfer == NULL) {//alliws ean yparxei h lista, alla einai adeia
        N->InTransferList->Account = NULL;//arxikopoihse olous tous deiktes se NULL
        N->InTransferList->lastInTransfer = NULL;
        free(N->InTransferList);//kai free tn mnhmh
        N->InTransferList = NULL;
        return 1;
    } 
    
    else {//alliws shmainei oti h List dn einai kenh, alla h DeleteIntransferList kaleitai mono ean einai kenh, opote einai critical
        printf("CRITICAL failure: %s InTransferList IS NOT EMPTY yet\n", N->name);
        fflush(stdout);
        exit(1);
    }
}

int DeleteOutTransferListForN(struct Account* N) {//Omoiws gia diagrafh OuttransferList gia account N
    if (N == NULL) {
        printf("failure: N is NULL\n");
        exit(1);
    }

    if (N->OutTransferList == NULL) {
        return 1;
    }

    if (N->OutTransferList->startOutTransfer == NULL) {
        N->OutTransferList->Account = NULL;
        N->OutTransferList->lastOutTransfer = NULL;
        free(N->OutTransferList);
        N->OutTransferList = NULL;
        return 1;
    } else {
        printf("CRITICAL failure: %s OutTransferList IS NOT EMPTY yet\n", N->name);
        exit(1);
    }
}

int DeleteInTransfersForN(struct Account* N) {//Diagrafei OLA ta IntransferElements gia Node N
    if (N == NULL) {
        printf("failure: N is NULL\n");
        fflush(stdout);
        return 0;
    }

    if (N->InTransferList == NULL) {//an einai deleted h list tote den yparxei kati na diagrafei
        return 1;
    }

    struct InTransferElem* cursorN = N->InTransferList->startInTransfer;
    struct InTransferElem* cursorD;
    struct Transfer* cursorTransferN = NULL;
    while (cursorN != NULL) {//gia kathe akmi
        cursorTransferN = cursorN->TransferToCurrentNode;//apothikevoume tn akmi p deixnei stn N
        cursorD = cursorN;
        cursorN = cursorN->nextInTransfer;//metavenoume stn epomeno Element p deixnei se akmh
        free(cursorD);//free to Element
        free(cursorTransferN);//free kai tn akmh stn opoia deixnei
    }
    N->InTransferList->startInTransfer = NULL;
    return 1;
}

int DeleteOutTransfersForN(struct Account* N) {//Omoiws me parapanw, me th diafora oti tn akmh den tn diagrafoume pali, afou th diagrafoume mono apo tn Intransfer meria
    if (N == NULL) {
        printf("failure: N is NULL \n");
        fflush(stdout);
        return 0;
    }

    if (N->OutTransferList == NULL) {
        return 1;
    }

    struct OutTransferElem* cursorN = N->OutTransferList->startOutTransfer;
    struct OutTransferElem* cursorD;

    while (cursorN != NULL) {//gia kathe akmi
        cursorD = cursorN;
        cursorN = cursorN->nextOutTransfer;
        free(cursorD);
    }
    N->OutTransferList->startOutTransfer = NULL;
    return 1;

}

