#ifndef BANKSERVER_MISC_H
#define BANKSERVER_MISC_H

#include "bankserver_structs.h"

int EmptyAccounts(struct HashStruct** hashStruct);
int DeleteHashStruct(struct HashStruct** hashStruct);
int DeleteAccount(char* name, struct HashTableElement* tobedeleted);
double AccountSum(char* name, struct HashStruct** hashStruct, int check1);

#endif	// BANKSERVER_MISC_H

