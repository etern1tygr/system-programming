#include "bankserver_hashtable.h"
#include "bankserver_accounts.h"
#include "bankserver_commands.h"
#include "bankserver_work.h"
#include <pthread.h>
#include <stdlib.h>	        
#include <ctype.h>
#include <stdio.h>
#include <string.h>


void TerminalArg(int* port, int* pool_size, int* queue_size, int argc, char** argv) {

    if (argc == 7) {//prepei na lavoume 7 orismata analoga me tis metatheseis
        if ((!strcmp(argv[1], "-p"))&&(!strcmp(argv[3], "-s"))&&(!strcmp(argv[5], "-q"))) {
            *port = atoi(argv[2]);
            *pool_size = atoi(argv[4]);
            *queue_size = atoi(argv[6]);

        }
        else if ((!strcmp(argv[1], "-p"))&&(!strcmp(argv[3], "-q"))&&(!strcmp(argv[5], "-s"))) {
            *port = atoi(argv[2]);
            *pool_size = atoi(argv[6]);
            *queue_size = atoi(argv[4]);
        }
            //an lavoume toul 5, eite than einai ./elegxos -o filename -b HashEntries + garbage
        else if ((!strcmp(argv[1], "-s"))&&(!strcmp(argv[3], "-p"))&&(!strcmp(argv[5], "-q"))) {
            *port = atoi(argv[4]);
            *pool_size = atoi(argv[2]);
            *queue_size = atoi(argv[6]);
        } 

        else if ((!strcmp(argv[1], "-s"))&&(!strcmp(argv[3], "-q"))&&(!strcmp(argv[5], "-p"))) {
            *port = atoi(argv[6]);
            *pool_size = atoi(argv[2]);
            *queue_size = atoi(argv[4]);
        }
            //eite than einai ./elegxos -b HashEntries -o filename + garbage
        else if ((!strcmp(argv[1], "-q"))&&(!strcmp(argv[3], "-s"))&&(!strcmp(argv[5], "-p"))) {
            *port = atoi(argv[6]);
            *pool_size = atoi(argv[4]);
            *queue_size = atoi(argv[2]);
        }
        else if ((!strcmp(argv[1], "-q"))&&(!strcmp(argv[3], "-p"))&&(!strcmp(argv[5], "-s"))) {
            *port = atoi(argv[4]);
            *pool_size = atoi(argv[6]);
            *queue_size = atoi(argv[2]);
        }
        else{
            printf("wrong argument sequence\n");
            fflush(stdout);
            exit(1);
        } 
    }
    
    else{
        printf("wrong number of arguments\n");
        fflush(stdout);
        exit(1);
    }

}
//logiko parsing
int add_accountInput(int fd, struct HashStruct** hashStruct, char* str) {
    double amount = -1;
    char name[100];
    int delay = -1;
    char delayarray[100];
    char response[8000];
    char init_amount[100];
    char command[strlen(str) + 1]; //static pinaka n chars +1 gia null terminator
    strncpy(command, str, (strlen(str)+1)); //copy to string stn static
    char* start = command;
    char* rest;
    char* token = strtok_r(start, " ", &rest);

    if (!strcmp(token, "add_account")) {//an einai h add_account
        if (token == NULL) {// a dn parei orismata mhnhma lathous
            printf("failure: No arguments at all\n");
            fflush(stdout);
            return 0;
        }
        int token_length;
        int flag = 0;
        int number = 0;
        int notnumber = 0;
        token = strtok_r(NULL, " ", &rest);
        while (token != NULL) {
            token_length = strlen(token);
            char digits[token_length];
            strncpy(digits, token, token_length);
            int i = 0;
            while (i <= token_length - 1) {//elegxos ean proketai gia digits
                if (isdigit(digits[i]) == 0) {
                    notnumber++; //ean dn einai arithmos
                    flag = 1;
                    break;
                }
                i++;
            }

            if (flag == 0) {//ean einai
                number++;
            }

            if ((notnumber == 0)&&(number == 1)) {
                //theloume mono ena arithmo
                amount = atoi(token);
            }
            else if ((notnumber == 1)&&(number == 1)) {
                strncpy(name, token, token_length + 1); //theloyme meta ton ENA arithmo, char*

            } 
            
            else if ((notnumber == 1)&&(number == 2)) {
                delay = atoi(token);
            } 
            
            else {
                printf("Wrong argument sequence\n");
                fflush(stdout);
                strncpy(response, "Error. Account creation failed ", strlen("Error. Account creation failed ")+1);
                doSendtoClient(fd, response);
                return 1;
            }
            token = strtok_r(NULL, " ", &rest);
            flag = 0;
        }
        
        if(!add_account(hashStruct, name, amount, delay)){
            strncpy(response, "Error. Account creation failed (", strlen("Error. Account creation failed (")+1);
        }
        else{
            strncpy(response, "Success. Account creation (", strlen("Success. Account creation (")+1);
        }
        strncat(response, name, strlen(name));
        strncat(response, ":", 1);
        sprintf(init_amount, "%f", amount);
        strncat(response, init_amount, strlen(init_amount));
        
        if(delay != -1){
            strncat(response, ":", 1);
            sprintf(delayarray, "%d", delay);
            strncat(response, delayarray, strlen(delayarray));
        }

        strncat(response, ")", 1);
        doSendtoClient(fd, response);
        return 1;
    }
    return 0;
}

int add_transferInput(int fd, struct HashStruct** hashStruct, char* str) {
    char response[8000];
    char amountarray[100];
    char delayarray[100];
    double amount = -1;
    char src_name[100];
    char dst_name[100];
    int delay = -1;
    char command[strlen(str) + 1]; //static pinaka n chars +1 gia null terminator
    strncpy(command, str, (strlen(str)+1)); //copy to string stn static

    char* start = command;
    char* rest;
    char* token = strtok_r(start, " ", &rest);
    
    if (!strcmp(token, "add_transfer")) {//an einai h add_account
        if (token == NULL) {// a dn parei orismata mhnhma lathous
            printf("failure: No arguments at all\n");
            fflush(stdout);
            return 0;
        }
        int token_length;
        int flag = 0;
        int number = 0;
        int notnumber = 0;
        token = strtok_r(NULL, " ", &rest);
        while (token != NULL) {
            token_length = strlen(token);
            char digits[token_length];
            strncpy(digits, token, token_length);
            int i = 0;
            while (i <= token_length - 1) {//elegxos ean proketai gia digits
                if (isdigit(digits[i]) == 0) {
                    notnumber++; //ean dn einai arithmos
                    flag = 1;
                    break;
                }
                i++;
            }

            if (flag == 0) {//ean einai
                number++;
            }

            if ((notnumber == 0)&&(number == 1)) {
                //theloume mono ena arithmo
                amount = atoi(token);
                if(amount == 0){//oxi mhdeniko transfer
                    return 0;
                }
            }
            else if ((notnumber == 1)&&(number == 1)) {
                strncpy(src_name, token, token_length + 1); //theloyme meta ton ENA arithmo, char*

            } 
            
            else if ((notnumber == 2)&&(number == 1)) {
                strncpy(dst_name, token, token_length + 1); //theloyme meta ton char*, char*

            } 
            
            else if ((notnumber == 2)&&(number == 2)) {
                delay = atoi(token);
            } 
            
            else {
                printf("Wrong argument sequence\n");
                return 0;
            }
            token = strtok_r(NULL, " ", &rest);
            flag = 0;
        }
        if(!add_transfer(hashStruct, amount, src_name, dst_name, delay)){
            strncpy(response, "Error. Transfer addition failed (", strlen("Error. Transfer addition failed (")+1);
        }
        else{
            strncpy(response, "Success. Transfer addition (", strlen("Success. Transfer addition (")+1);
        }
        strncat(response, src_name, strlen(src_name));
        strncat(response, ":", 1);
        strncat(response, dst_name, strlen(dst_name));
        strncat(response, ":", 1);
        sprintf(amountarray, "%f", amount);
        strncat(response, amountarray, strlen(amountarray));
        if(delay != -1){
            strncat(response, ":", 1);
            sprintf(delayarray, "%d", delay);
            strncat(response, delayarray, strlen(delayarray));
        }
        strncat(response, ")", 1);
        doSendtoClient(fd, response);
        return 1;
    }
    return 0;
}

int add_multi_transferInput(int fd, struct HashStruct** hashStruct, char* str) {
    char response[8000];
    double amount = -1;
    char amountarray[100];
    char src_name[100];
    int delay = -1;
    char delayarray[100];
    char tmp_command[strlen(str)+1];
    char command[strlen(str) + 1]; //static pinaka n chars +1 gia null terminator
    
    strncpy(command, str, (strlen(str)+1)); //copy to string stn static
    strncpy(tmp_command, str, (strlen(str)+1)); //copy to string stn static
    
    char* start = command;
    char* rest;
    char* token = strtok_r(start, " ", &rest);
    
    char* start_tmp = tmp_command;
    char* rest_tmp;
    char* token_tmp = strtok_r(start_tmp, " ", &rest_tmp);

    if (!strcmp(token, "add_multi_transfer")) {//an einai h add_multi_transfer
        if (token == NULL) {// a dn parei orismata mhnhma lathous
            printf("failure: No arguments at all\n");
            return 0;
        }
        
        int args = 0;
        int token_length_tmp;
        int notnumber_tmp = 0;
        while(token_tmp != NULL){//vriskw posa einai ta dest_names ths edolhs
            token_length_tmp = strlen(token_tmp);
            char digits_tmp[token_length_tmp];
            strncpy(digits_tmp, token_tmp, token_length_tmp);
            int i = 0;
            while (i <= token_length_tmp - 1) {//elegxos ean proketai gia digits
                if (isdigit(digits_tmp[i]) == 0) {
                    notnumber_tmp++; //ean dn einai arithmos
                    break;
                }
                i++;
            }
            args++;
            token_tmp = strtok_r(NULL, " ", &rest_tmp);

        }
        int size = notnumber_tmp - 2;
        char* dst_name_list[size];
        
        int token_length;
        int flag = 0;
        int number = 0;
        int notnumber = 0;
        token = strtok_r(NULL, " ", &rest);
        int y = 0;
        while (token != NULL) {
            token_length = strlen(token);
            char digits[token_length];
            strncpy(digits, token, token_length);
            int i = 0;
            while (i <= token_length - 1) {//elegxos ean proketai gia digits
                if (isdigit(digits[i]) == 0) {
                    notnumber++; //ean dn einai arithmos
                    flag = 1;
                    break;
                }
                i++;
            }

            if (flag == 0) {//ean einai
                number++;
            }

            if ((notnumber == 0)&&(number == 1)) {
                //theloume mono ena arithmo
                amount = atoi(token);
                if(amount == 0){
                    return 0;
                }
            }
            else if ((notnumber == 1)&&(number == 1)) {
                strncpy(src_name, token, token_length + 1); //theloyme meta ton ENA arithmo, char*

            } 
            
            else if ((notnumber >= 1)&&(number == 1)) {
                dst_name_list[y] = token;
                y++;

            } 
            
            else if ((notnumber >= 2)&&(number == 2)) {
                delay = atoi(token);
            }
            
            else {
                printf("Wrong argument sequence\n");
                return 0;
            }
            token = strtok_r(NULL, " ", &rest);
            flag = 0;
        }
        if(!add_multi_transfer(hashStruct, amount, src_name, dst_name_list, size, delay)){
            strncpy(response, "Error. Multi Transfer addition failed (", strlen("Error. Multi Transfer addition failed (")+1);
        }
        else{
            strncpy(response, "Success. Multi Transfer addition (", strlen("Success. Multi Transfer addition (")+1);
        }
        strncat(response, src_name, strlen(src_name));
        strncat(response, ":", 1);

        sprintf(amountarray, "%f", amount);
        strncat(response, amountarray, strlen(amountarray));
        if(delay != -1){
            strncat(response, ":", 1);
            sprintf(delayarray, "%d", delay);
            strncat(response, delayarray, strlen(delayarray));
        }
        strncat(response, ")", 1);
        doSendtoClient(fd, response);
        return 1;
    }
    return 0;
}

int print_balanceInput(int fd, struct HashStruct** hashStruct, char* str) {
    int token_length;
    int flag = 0;
    char name[100];
    char response[8000];
    char command[strlen(str) + 1];
    strncpy(command, str, (strlen(str)+1));

    char* start = command;
    char* rest;
    char* token = strtok_r(start, " ", &rest);
    
    if (!strcmp(token, "print_balance")) {
      
        token = strtok_r(NULL, " ", &rest);
        if (token != NULL) {
            token_length = strlen(token);
            char digits[token_length];
            strncpy(digits, token, token_length);
            int i = 0;
            while (i <= token_length - 1) {//elegxos ean proketai gia digits
                if (isdigit(digits[i]) == 0) {
                    flag = 1;
                    break;
                }
                i++;
            }

            if (flag == 0) {
                return 0;
            }
            strncpy(name, token, token_length + 1);
            token = strtok_r(NULL, " ", &rest);
            if(token == NULL){
                double sum;
                char sumarray[100];
                int h = Hash(name) % (*hashStruct)->size;
        
                pthread_mutex_lock(&(*hashStruct)->mutexes[h]);
                if((sum = print_balance(hashStruct, name)) < 0){        
                    pthread_mutex_unlock(&(*hashStruct)->mutexes[h]);
                    strncpy(response, "Error. Balance failed (", strlen("Error. Balance failed (")+1);
                    strncat(response, name, strlen(name));
                    strncat(response, ")", 1);
                }
                else{
                    pthread_mutex_unlock(&(*hashStruct)->mutexes[h]);
                    strncpy(response, "Success. Balance (", strlen("Success. Balance (")+1);
                    strncat(response, name, strlen(name));
                    strncat(response, ":", 1);
                    sprintf(sumarray, "%f", sum);
                    strncat(response, sumarray, strlen(sumarray));
                    strncat(response, ")", 1);
                }
                doSendtoClient(fd, response);
                return 1;
            }
            else{
                return 0;
            }
        }
        return 0;
    }
    return 0;
}

int print_multi_balanceInput(int fd, struct HashStruct** hashStruct, char* str) {
    int token_length;
    int flag = 0;
    char response[8000];
    char tmp_command[strlen(str)+1];
    char command[strlen(str) + 1]; //static pinaka n chars +1 gia null terminator
    
    strncpy(command, str, (strlen(str)+1)); //copy to string stn static
    strncpy(tmp_command, str, (strlen(str)+1)); //copy to string stn static
    
    char* start = command;
    char* rest;
    char* token = strtok_r(start, " ", &rest);
    
    char* start_tmp = tmp_command;
    char* rest_tmp;
    char* token_tmp = strtok_r(start_tmp, " ", &rest_tmp);

    if (!strcmp(token, "print_multi_balance")) {
        
        int args = 0;
        while(token_tmp != NULL){//vriskw prwta me ena perasma posa einai ta names tis edolhs
            token_tmp = strtok_r(NULL, " ", &rest_tmp);
            args++;
        }
        
        
        int size = args - 1;
        char* name_list[size];

        token = strtok_r(NULL, " ", &rest);
        int y = 0;
        while (token != NULL) {
            token_length = strlen(token);
            char digits[token_length];
            strncpy(digits, token, token_length);
            int i = 0;
            while (i <= token_length - 1) {//elegxos ean proketai gia digits
                if (isdigit(digits[i]) == 0) {
                    flag = 1;
                    break;
                }
                i++;
            }

            if (flag == 0) {
                return 0;
            }
            name_list[y] = token;
            y++;
            token = strtok_r(NULL, " ", &rest);
            flag = 1;
        }
        char sumarray[100];
        double sum_list[size];
        int i;
        int table[size];
        
        for(i = 0; i <= size-1; i++){//vazw to hash kathe name se ena pinaka
            table[i] = Hash(name_list[i]) % (*hashStruct)->size;
        }

        int tmp;
        int j;
        for(i = 0; i < size-1; i++){
            for(j = 0; j<size-1-i; j++){
                if(table[j] > table[j+1]){
                    tmp = table[j];
                    table[j] = table[j+1];
                    table[j+1] = tmp;
                }   
            }
        }
        
        for(i = 0; i <= size-1; i++){//kleidwnw ola ta adikeimena vasei taksinomhshs
            pthread_mutex_lock(&(*hashStruct)->mutexes[table[i]]);
        }

        for(i = 0; i<=size-1; i++){//gia kathe name ektelw tn print_balance
            if((sum_list[i] = print_balance(hashStruct, name_list[i])) < 0){//ean apotyxei estw k mia
                for(i = 0; i <= size-1; i++){//ksekleidw ola ta mutexes
                    pthread_mutex_unlock(&(*hashStruct)->mutexes[table[i]]);
                }
                strncpy(response, "Error. Multi Balance failed (", strlen("Error. Multi Balance failed (")+1);                
                for(y = 0; y<=size-1; y++){
                    strncat(response, name_list[y], strlen(name_list[y]));
                    if(y < size-1){
                        strncat(response, ":", 1);
                    }
                }
                strncat(response, ")", 1);
                doSendtoClient(fd, response);
                return 1;
            }
        }
        if(i == size){//ean ftasei edw shmainei oti ektelesthkan oles oi print_balance
            for(i = 0; i <= size-1; i++){//ara ksekleidwnw pali ta pada
                pthread_mutex_unlock(&(*hashStruct)->mutexes[table[i]]);
            }
            strncpy(response, "Success. Multi Balance (", strlen("Success. Multi Balance (")+1);
            for(i = 0; i<=size-1; i++){
                strncat(response, name_list[i], strlen(name_list[i]));
                strncat(response, "/", 1);
                sprintf(sumarray, "%f", sum_list[i]);
                strncat(response, sumarray, strlen(sumarray));
                if(i < size-1){
                    strncat(response, ":", 1);
                }
            }
            strncat(response, ")", 1);
            doSendtoClient(fd, response);
            return 1;
        }
    }
    return 0;
}
