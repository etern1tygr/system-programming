#include <ctype.h>
#include <pthread.h>
#include <unistd.h>
#include "bankserver_structs.h"
#include "bankserver_hashtable.h"
#include "bankserver_commands.h"
#include "bankserver_lists.h"
#include "bankserver_accounts.h"


int add_account(struct HashStruct** hashStruct, char* name, double init_amount, int delay){
    //kleidwnei to name, elegxei ean yparxei to dhmiourgei ean dn yparxei kai ksekleidwnei
    int h = Hash(name) % (*hashStruct)->size;
        
    pthread_mutex_lock(&(*hashStruct)->mutexes[h]);
  
    int check = SearchNodeInHashTableAndCollisionLists(hashStruct, name);
    int output = FillHashTableElem(hashStruct, name, init_amount, check);
    

    if(delay >= 0){
        printf("I am sleeping\n");
        fflush(stdout);
        usleep(delay*1000);
        printf("Woke Up\n");
        fflush(stdout);
    }

    pthread_mutex_unlock(&(*hashStruct)->mutexes[h]);
    return output;
}

int add_transfer(struct HashStruct** hashStruct, double amount, char* src_name, char* dst_name, int delay){
    //topothethsh se afksousa seira ta hash, wste na ginei kleidwma me taksinomhsh
    
    int h1 = Hash(src_name) % (*hashStruct)->size;
    int h2 = Hash(dst_name) % (*hashStruct)->size;
    int tmp;
    if(h1 > h2){
        tmp = h1;
        h1 = h2;
        h2 = tmp;
    }
    pthread_mutex_lock(&(*hashStruct)->mutexes[h1]);
    pthread_mutex_lock(&(*hashStruct)->mutexes[h2]);
    
    
    if(!strcmp(src_name, dst_name)){   //den bainei o idios account
        printf("failure: %s = %s \n", src_name, dst_name); 
        fflush(stdout);
        

        if(delay >= 0){
            printf("I am sleeping\n");
            fflush(stdout);
            usleep(delay*1000);
            printf("Woke Up\n");
            fflush(stdout);
        }

        pthread_mutex_unlock(&(*hashStruct)->mutexes[h1]);
        pthread_mutex_unlock(&(*hashStruct)->mutexes[h2]);
        return 0;//
    }
    
    
    if((*hashStruct) == NULL){//elegxos
        printf("failure: hashstruct is NULL\n");
        fflush(stdout);
        
        if(delay >= 0){
            printf("I am sleeping\n");
            fflush(stdout);
            usleep(delay*1000);
            printf("Woke Up\n");
            fflush(stdout);
        }

        pthread_mutex_unlock(&(*hashStruct)->mutexes[h1]);
        pthread_mutex_unlock(&(*hashStruct)->mutexes[h2]);
        return 0;
    }
    //elegxos ean yparxei to account kai elegxos gia negative balance
    int check1 = SearchNodeInHashTableAndCollisionLists(hashStruct, src_name);
    if((AccountSum(src_name, hashStruct, check1)-amount) < 0){
        printf("failure: negative balance\n"); 
        fflush(stdout);
        

        if(delay >= 0){
            printf("I am sleeping\n");
            fflush(stdout);
            usleep(delay*1000);
            printf("Woke Up\n");
            fflush(stdout);
        }

        pthread_mutex_unlock(&(*hashStruct)->mutexes[h1]);
        pthread_mutex_unlock(&(*hashStruct)->mutexes[h2]);
        return 0;
    }
    int check2 = SearchNodeInHashTableAndCollisionLists(hashStruct, dst_name);
    unsigned long hash1 = Hash(src_name);
    int table_index1 = hash1%(*hashStruct)->size;//vres an yparxei to account
    unsigned long hash2 = Hash(dst_name);
    int table_index2 = hash2%(*hashStruct)->size;//vres an yparxei to account
    struct HashTableElement* src = (*hashStruct)->table[table_index1];
    struct HashTableElement* dst = (*hashStruct)->table[table_index2];
    if((check1 >=0 )&&(check2 >=0)){//an yparxoun 
        while(src!=NULL){
            if(check1 == 0){//an einai sto hash table
                break;
            }
            
            src = src->nextCollisionAccount;//alliws psakse sta collision list
            --check1;
        }
        
        while(dst!=NULL){//omoiws gia tn allon account
            if(check2 == 0){
                break;
            }
            
            dst = dst->nextCollisionAccount;
            --check2;
        }
        
        struct Account* N1 = src->Account;
        struct Account* N2 = dst->Account;
        OutTransferListCreate(N1);
        InTransferListCreate(N2);

        struct OutTransferElem* from = N1->OutTransferList->startOutTransfer;
        struct InTransferElem* to = N2->InTransferList->startInTransfer;

        struct Transfer* target = malloc(sizeof(struct Transfer));
        if(target == NULL){//apotyxia malloc
            printf("failure: malloc FAILED\n");
            fflush(stdout);
            pthread_mutex_unlock(&(*hashStruct)->mutexes[h1]);
            pthread_mutex_unlock(&(*hashStruct)->mutexes[h2]);
            exit(1);
        }
        //topothetoume amount kai deiktes gia pros kai apo account
        target->amount = amount;
        target->toAccount = N2;
        target->fromAccount = N1;

        if((from == NULL)||(to == NULL)){//dhmiourgei ta Elements gia ekastote list account ean einai adeies
            OutTransferElemCreate(N1, target);  
            InTransferElemCreate(N2, target);
            printf("success: ADDED transaction (%s, %s) with amount %f\n", N1->name, N2->name, amount);
            fflush(stdout);
            

            if(delay >= 0){
                printf("I am sleeping\n");
                fflush(stdout);
                usleep(delay*1000);
                printf("Woke Up\n");
                fflush(stdout);
            }

            pthread_mutex_unlock(&(*hashStruct)->mutexes[h1]);
            pthread_mutex_unlock(&(*hashStruct)->mutexes[h2]);
            return 1;
        }
        else{//an dn einai adeies
            struct OutTransferElem* cursorN1 = N1->OutTransferList->startOutTransfer;        
            while(cursorN1!= NULL){//gia kathe Element           
                if((cursorN1->TransferToNextNode->toAccount == N2)&&(cursorN1->TransferToNextNode->fromAccount == N1)){//an deixnei stn akmh p theloume na dhmiourghsoume         
                    cursorN1->TransferToNextNode->amount += amount;//shmainei oti yparxei hdh, ara kanei add to amount mono
                    from = NULL;//free to malloc
                    to = NULL;
                    target->fromAccount = NULL;
                    target->toAccount = NULL;
                    free(target);
                    target = NULL;
                    cursorN1 = NULL;
                    printf("success: ADDED existing transaction (%s, %s) with amount %f\n", N1->name, N2->name, amount);
                    fflush(stdout);
                    

                    if(delay >= 0){
                        printf("I am sleeping\n");
                        fflush(stdout);
                        usleep(delay*1000);
                        printf("Woke Up\n");
                        fflush(stdout);
                    }

                    pthread_mutex_unlock(&(*hashStruct)->mutexes[h1]);
                    pthread_mutex_unlock(&(*hashStruct)->mutexes[h2]);
                    return 1;
                }  
                cursorN1 = cursorN1->nextOutTransfer;
            }
            cursorN1 = NULL;//an den yparxei h akmh se mh kenes lists, th dhmiourgei
            OutTransferElemCreate(N1, target);  
            InTransferElemCreate(N2, target);
            target = NULL;
            printf("success: ADDED transaction (%s, %s) with amount %f\n", N1->name, N2->name, amount);
            

            if(delay >= 0){
                printf("I am sleeping\n");
                fflush(stdout);
                usleep(delay*1000);
                printf("Woke Up\n");
                fflush(stdout);
            }

            pthread_mutex_unlock(&(*hashStruct)->mutexes[h1]);
            pthread_mutex_unlock(&(*hashStruct)->mutexes[h2]);
            return 1;
        }//alliws error
        printf("failure: Error, transaction NOT CREATED OR FOUND\n");
        fflush(stdout);

        pthread_mutex_unlock(&(*hashStruct)->mutexes[h1]);
        pthread_mutex_unlock(&(*hashStruct)->mutexes[h2]);
        exit(1);
         
    }
    else{//alliws dn yparxei kanenas tous h kapoios apo tous dyo, opote dn kanei addtran
        printf("failure: Account dont exist\n");
        fflush(stdout);
        

        if(delay >= 0){
            printf("I am sleeping\n");
            fflush(stdout);
            usleep(delay*1000);
            printf("Woke Up\n");
            fflush(stdout);
        }

        pthread_mutex_unlock(&(*hashStruct)->mutexes[h1]);
        pthread_mutex_unlock(&(*hashStruct)->mutexes[h2]);
        return 0;
    }
    printf("failure: Error, unknown\n");
    fflush(stdout);
    pthread_mutex_unlock(&(*hashStruct)->mutexes[h1]);
    pthread_mutex_unlock(&(*hashStruct)->mutexes[h2]);
    exit(1);
}  

double print_balance(struct HashStruct** hashStruct, char* name){
    
    int check_src = SearchNodeInHashTableAndCollisionLists(hashStruct, name);
    if(check_src < 0){ // error
        printf("failure: Error, %s dont exist, either hashStruct or its table is NULL\n", name);
        fflush(stdout);
        return -1;
    }
    
    int output = AccountSum(name, hashStruct, check_src);
        
    return output;  
}

int add_multi_transfer(struct HashStruct** hashStruct, double amount,  char* src_name, char** dst_name_list, int size, int delay){
    int i;
    int table[size+1];
    table[0] = Hash(src_name) % (*hashStruct)->size;
    for(i = 1; i <= size; i++){
        table[i] = Hash(dst_name_list[i-1]) % (*hashStruct)->size;
    }
    
    int tmp;
    int j;
    for(i = 0; i < size; i++){
        for(j = 0; j<size-i; j++){
            if(table[j] > table[j+1]){
                tmp = table[j];
                table[j] = table[j+1];
                table[j+1] = tmp;
            }   
        }
    }
    
    for(i = 0; i <= size; i++){
        pthread_mutex_lock(&(*hashStruct)->mutexes[table[i]]);
    }
    
    int total_amount = amount*size;
    
    int check_src = SearchNodeInHashTableAndCollisionLists(hashStruct, src_name);
    
    if((AccountSum(src_name, hashStruct, check_src) - total_amount) < 0){
        printf("failure: negative balance\n"); 
        fflush(stdout);
        
        

        if(delay >= 0){
            printf("I am sleeping\n");
            fflush(stdout);
            usleep(delay*1000);
            printf("Woke Up\n");
            fflush(stdout);
        }

        for(i = 0; i <= size; i++){
            pthread_mutex_unlock(&(*hashStruct)->mutexes[table[i]]);
        }
        return 0;
    }
    
    int check_dst_array[size];
    
    for(i = 0; i<=size-1; i++){
        check_dst_array[i] = SearchNodeInHashTableAndCollisionLists(hashStruct, dst_name_list[i]);
        if((check_dst_array[i] < 0)||(check_src < 0)){ // error
            printf("failure: Error, %s or %s dont exist, either hashStruct or its table is NULL\n", src_name, dst_name_list[i]);
            fflush(stdout);
            

            if(delay >= 0){
                printf("I am sleeping\n");
                fflush(stdout);
                usleep(delay*1000);
                printf("Woke Up\n");
                fflush(stdout);
            }

            for(i = 0; i <= size; i++){
                pthread_mutex_unlock(&(*hashStruct)->mutexes[table[i]]);
            }
            return 0;
        }
    }
    
    unsigned long hash1 = Hash(src_name);
    int table_index1 = hash1%(*hashStruct)->size;
    int table_index2;
    unsigned long hash2;
    struct HashTableElement* src = (*hashStruct)->table[table_index1];
    struct HashTableElement* dst = NULL;
    
    while(src!=NULL){
        if(check_src == 0){//an einai sto hash table
            break;
        }
        src = src->nextCollisionAccount;//alliws psakse sta collision list
        --check_src;
    }
    
    struct Account* N1 = src->Account;
    struct Account* N2 = NULL;
    OutTransferListCreate(N1);
    struct OutTransferElem* from = N1->OutTransferList->startOutTransfer;
    struct InTransferElem* to = NULL;
    struct OutTransferElem* cursorN1 = NULL;
    struct Transfer* target = NULL;
    
    for(i = 0; i<=size-1; i++){
        if(!strcmp(src_name, dst_name_list[i])){   //den bainei o idios account
            printf("failure: %s = %s \n", src_name, dst_name_list[i]); 
            fflush(stdout);
            

            if(delay >= 0){
                printf("I am sleeping\n");
                fflush(stdout);
                usleep(delay*1000);
                printf("Woke Up\n");
                fflush(stdout);
            }

            for(i = 0; i <= size; i++){
                pthread_mutex_unlock(&(*hashStruct)->mutexes[table[i]]);
            }
            return 0;//
        }

        if((*hashStruct) == NULL){
            printf("failure: hashstruct is NULL\n");
            fflush(stdout);
            

            if(delay >= 0){
                printf("I am sleeping\n");
                fflush(stdout);
                usleep(delay*1000);
                printf("Woke Up\n");
                fflush(stdout);
            }

            for(i = 0; i <= size; i++){
                pthread_mutex_unlock(&(*hashStruct)->mutexes[table[i]]);
            }
            return 0;
        }
        
        hash2 = Hash(dst_name_list[i]);
        table_index2 = hash2%(*hashStruct)->size;//vres an yparxei o account
        dst = (*hashStruct)->table[table_index2];

        while(dst!=NULL){//omoiws gia tn allon account
            if(check_dst_array[i] == 0){
                break;
            }

            dst = dst->nextCollisionAccount;
            --check_dst_array[i];
        }
        
        
        N2 = dst->Account;
        InTransferListCreate(N2);
        to = N2->InTransferList->startInTransfer;

        target = malloc(sizeof(struct Transfer));
        if(target == NULL){//apotyxia malloc
            printf("failure: malloc FAILED\n");
            fflush(stdout);
            for(i = 0; i <= size; i++){
                pthread_mutex_unlock(&(*hashStruct)->mutexes[table[i]]);
            }
            exit(1);
        }
        //topothetoume amount kai deiktes gia pros kai apo account
        target->amount = amount;
        target->toAccount = N2;
        target->fromAccount = N1;

        if((from == NULL)||(to == NULL)){//dhmiourgei ta Elements gia ekastote list account ean einai adeies
            OutTransferElemCreate(N1, target);  
            InTransferElemCreate(N2, target);
            printf("success: ADDED transaction (%s, %s) with amount %f\n", N1->name, N2->name, amount);
            fflush(stdout);
        }
        else{//an dn einai adeies
            int flag = 0;
            cursorN1 = N1->OutTransferList->startOutTransfer;        
            while(cursorN1!= NULL){//gia kathe Element           
                if((cursorN1->TransferToNextNode->toAccount == N2)&&(cursorN1->TransferToNextNode->fromAccount == N1)){//an deixnei stn akmh p theloume na dhmiourghsoume         
                    cursorN1->TransferToNextNode->amount += amount;//shmainei oti yparxei hdh, ara kanei add to amount mono
                    from = NULL;//free to malloc
                    to = NULL;
                    target->fromAccount = NULL;
                    target->toAccount = NULL;
                    free(target);
                    target = NULL;
                    cursorN1 = NULL;
                    printf("success: ADDED existing transaction (%s, %s) with amount %f\n", N1->name, N2->name, amount);
                    fflush(stdout);
                    flag = 1;
                    break;
                }  
                cursorN1 = cursorN1->nextOutTransfer;
            }
            if(flag == 0){
                cursorN1 = NULL;//an den yparxei h akmh se mh kenes lists, th dhmiourgei
                OutTransferElemCreate(N1, target);  
                InTransferElemCreate(N2, target);
                target = NULL;
                printf("success: ADDED transaction (%s, %s) with amount %f\n", N1->name, N2->name, amount);
            }    
        }
    }

    if(delay >= 0){
        printf("I am sleeping\n");
        fflush(stdout);
        usleep(delay*1000);
        printf("Woke Up\n");
        fflush(stdout);
    }

    for(i = 0; i <= size; i++){
        pthread_mutex_unlock(&(*hashStruct)->mutexes[table[i]]);
    }
    return 1;
}   

