#include "bankserver_structs.h"
#include "bankserver_lists.h"
#include "bankserver_accounts.h"
#include "bankserver_hashtable.h"


int EmptyAccounts(struct HashStruct** hashStruct){//adeiazei ta hash table elements apo accounts
    
   if(((*hashStruct) == NULL)||((*hashStruct)->table == NULL)){
        printf("failure: hashStruct or its table is NULL\n");
        fflush(stdout);
        return 0;
   }
   int i;
   for(i=0; i<(*hashStruct)->size; i++){//gia kathe table element
        if((*hashStruct)->table[i] == NULL){//den prepei na einai null efoson exoume dhmiourghsei hash struct
            printf("failure: Table[%d] is NULL\n", i);
            fflush(stdout);
            return 0;
        }
        struct HashTableElement* cursorCollision = (*hashStruct)->table[i]->nextCollisionAccount;
        struct HashTableElement* tmp = cursorCollision;
        while(tmp!= NULL){//gia kathe collision element ksekinodas apthn arxh(to head ths list dn einai collision element)
            
            DeleteAccount(cursorCollision->name, cursorCollision);//arxikopoihsh account pou tha diagrafei metepeita
            tmp = tmp->nextCollisionAccount;
            (*hashStruct)->table[i]->nextCollisionAccount = cursorCollision->nextCollisionAccount; //thetoume neo epomeno collision element
            strncpy(cursorCollision->name, cursorCollision->Account->name, strlen(cursorCollision->Account->name)+1);
            free(cursorCollision->Account);//free tn account tou element telikws
            cursorCollision->Account = NULL;
            free(cursorCollision);//free kai to element
            cursorCollision = tmp;
            
        }
        if((*hashStruct)->table[i]->Account == NULL){//alliws an dn yparxei collision element kai to head d exei account, proxwrame xwris delete node
            continue;
        }
        DeleteAccount((*hashStruct)->table[i]->Account->name, (*hashStruct)->table[i]);//alliws diagrafh tou account sto head etc
        strncpy((*hashStruct)->table[i]->name, (*hashStruct)->table[i]->Account->name, strlen((*hashStruct)->table[i]->Account->name)+1);
        free((*hashStruct)->table[i]->Account);
        (*hashStruct)->table[i]->Account = NULL;
    }
    printf("success: cleaned memory\n");
    fflush(stdout);
    return 1;
}

int createAccount(char* name, struct HashTableElement* tobecreated, int init_amount, int size){//dhmiourgei enan account   
    if(tobecreated == NULL){
        printf("failure: HashElement is NULL\n");
        fflush(stdout);
        return 0;
    }
    
    struct Account* newAccount;//an apotyxei to malloc
    if((newAccount = malloc(sizeof(struct Account))) == NULL){
        printf("failure: newNode malloc FAILED \n");
        exit(1);
    }//arxikopoiei tn Outlink Inlink list tou neou account
    strncpy(newAccount->name, name, strlen(name)+1);
    newAccount->amount = init_amount;
    newAccount->InTransferList = NULL;
    newAccount->OutTransferList = NULL;
    tobecreated->Account = newAccount;//to element tou hashtable deixnei stn account
    tobecreated->nextCollisionAccount = NULL;//to epomenp element einai Collision Element k arxikopoeitai me NULL
    tobecreated->key = Hash(name)%size;
    newAccount = NULL;
    printf("success: CREATED %s\n", name);
    fflush(stdout);
    return 1;
}

int DeleteAccount(char* name, struct HashTableElement* tobedeleted){//diagrafei enan account N
    
    if((tobedeleted == NULL)||(tobedeleted->Account == NULL)){
        printf("failure: HashElement is NULL or pointer to %s is NULL\n", name);
        fflush(stdout);
        return 0;
    }
    
    if(tobedeleted->Account->OutTransferList != NULL){
        if(tobedeleted->Account->OutTransferList->startOutTransfer == NULL){
            printf("CRITICAL failure: InTransferList without startOutTransfer!\n");
            exit(1);//den ginetai na yparxei outlinklist xwris arxiko element
        }
        if(tobedeleted->Account->OutTransferList->startOutTransfer != NULL){
            DeleteOutTransfersForN(tobedeleted->Account);//alliws diegrapse ta Outlinks tou N
            DeleteOutTransferListForN(tobedeleted->Account); //kai tn list efoson einai adeia
        }
    }    
    
    if(tobedeleted->Account->InTransferList != NULL){
        if(tobedeleted->Account->InTransferList->startInTransfer == NULL){
            printf("CRITICAL failure: InTransferList without startOutTransfer!\n");
            fflush(stdout);
            exit(1);//den ginetai na yparxei inlinklist xwris arxiko element
        }
        if(tobedeleted->Account->InTransferList->startInTransfer != NULL){
            DeleteInTransfersForN(tobedeleted->Account);//alliws diegrapse ta Inlinks tou N
            DeleteInTransferListForN(tobedeleted->Account);//kai tn list efoson einai adeia
        }   
    }//efoson exoun diagrafei oi lists tou account ston opoio deixnei to HashTableElement, arxikopoioumai me -1 ta members tou      
    strncpy(tobedeleted->Account->name, "invalid", strlen("invalid")+1);
    return 1;
}

double AccountSum(char* name, struct HashStruct** hashStruct, int check1){
    
    if((*hashStruct) == NULL){
        printf("failure: HashStruct for %s is NULL\n", name);
        return -1;
    }
    
    unsigned long hash = Hash(name);
    int table_index = hash%(*hashStruct)->size;//vres an yparxei o account
    struct HashTableElement* cursorN1 = (*hashStruct)->table[table_index];
    if(check1 >=0){//psaxnei tn account
        while(cursorN1!=NULL){
            if(check1 == 0){
                break;
            }
            
            cursorN1 = cursorN1->nextCollisionAccount;
            --check1;
        }
         
        struct Account* N = cursorN1->Account;
        double sumout = 0;
        if(N->OutTransferList != NULL){//ean exei ekswterikes transfers
            struct OutTransferElem* cursorOut = N->OutTransferList->startOutTransfer;
            while(cursorOut!= NULL){//briskei to synoliko amount tous
                sumout+=cursorOut->TransferToNextNode->amount;
                cursorOut = cursorOut->nextOutTransfer;
            }
            cursorOut = NULL;
            

        }
        else{
            sumout = 0;
        }
        
        double sumin = 0;
        if(N->InTransferList != NULL){//ean exei ekswterikes transfers
            struct InTransferElem* cursorIn = N->InTransferList->startInTransfer;
            while(cursorIn!= NULL){//briskei to synoliko amount tous
                sumin+=cursorIn->TransferToCurrentNode->amount;
                cursorIn = cursorIn->nextInTransfer;
            }
            cursorIn = NULL;
        }
        else{
            sumin = 0;
        }
        return (cursorN1->Account->amount+sumin-sumout);
    }
    else{//alliws ean dn yparxei to account
        printf("failure: %s doesnt exist\n", name);
        fflush(stdout);
        return -1;
    }
    printf("failure: Error %s \n", name);
    fflush(stdout);
    return -1; 
}
