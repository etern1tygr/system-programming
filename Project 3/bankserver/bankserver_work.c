#include "bankserver_structs.h"
#include "bankserver_work.h"
#include "bankserver_parsing.h"
#include <stdio.h>
#include <sys/wait.h>	     /* sockets */
#include <sys/types.h>	     /* sockets */
#include <sys/socket.h>	     /* sockets */
#include <netinet/in.h>	     /* internet sockets */
#include <netdb.h>	         /* gethostbyaddr */
#include <unistd.h>	         /* fork */		
#include <stdlib.h>	         /* exit */
#include <ctype.h>	         /* toupper */
#include <signal.h>          /* signal */
#include <stdint.h>

void service(int newsock, struct HashStruct** hashStruct) {//diavazoume oso dn exei kleisei to socket apo tn meria tou client(p tha epistrepsei 0 bytes))
    int function;

    while ((function = doReadfromClient(newsock, hashStruct)) > 0) {
        
    }
    //kleinoume to socket k apth meria tou server
    close(newsock); /* parent closes socket to client */
}


void doSendtoClient(int fd, char* str) {//pali stelnoume prwta ta bytes meta to message
    int32_t l = htonl(strlen(str)+1);
    write(fd, &l, sizeof(l));   // 4B = strlen(str)
    printf("server sent: %d chars\n", (int)(strlen(str)+1));
    fflush(stdout);
    write(fd, str, strlen(str)+1);          // str data
}

int doReadfromClient(int sock, struct HashStruct** hashStruct) {//diavazei apo tn client 
    int32_t bytes1;
    char * message;
    printf("waiting client commands:\n");
    fflush(stdout);
    printf("\n");
    fflush(stdout);
    
    //diavazoyme bytes apo tn client, opou ean epistrafei 0 shmainei oti ekleise o client ara epistrefoume 0 stn service gia na kleisei tn epikoinwnia
    if (read(sock, &bytes1, sizeof(int)) == 0) {
        return 0;
    }
    int32_t bytes = ntohl(bytes1);
    message = malloc(bytes);
    if (read(sock, message, bytes) == 0) {//omoiws diavazoume to mhnyma
        return 0;
    }
    
    printf("command: %s received\n", message);
    fflush(stdout);
    printf("server read: %d chars\n", bytes);
    fflush(stdout);
    
    //kanoume logiko parsing ksexwrista gia kathe pithanh edolh
    if (add_accountInput(sock, hashStruct, message)) {
    } 
    else if (add_transferInput(sock, hashStruct, message)) {

    } 
    else if (add_multi_transferInput(sock, hashStruct, message)) {

    } 
    else if (print_balanceInput(sock, hashStruct, message)) {

    } 
    else if (print_multi_balanceInput(sock, hashStruct, message)) {
    
    }
    else{//se periptwsh p exoume sydaktika swsth edolh alla dn adistoixei se kamia tote exoume unknown command
        int l = strlen("Error. Unknown Command");
        char response[l+1];
        strncpy(response, "Error. Unknown Command", l+1);
        doSendtoClient(sock, response);
    }
    free(message);    
    return 1;
}

int doPrepareSocket(int port) {
    int mastersock;
    struct sockaddr_in server;
    struct sockaddr *serverptr = (struct sockaddr *) &server;

    if ((mastersock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        perror("socket");
        return -1;
    }
    
    server.sin_family = AF_INET; /* Internet domain */
    server.sin_addr.s_addr = htonl(INADDR_ANY);
    server.sin_port = htons(port); /* The given port */
    
    if (bind(mastersock, serverptr, sizeof (server)) < 0) {
        perror("socket");
        return -1;
    }
    
    if (listen(mastersock, 55) < 0) {
        perror("listen");
        return -1;
    }
    
    return mastersock;
}

void doClearSocket(int mastersock) {
    close(mastersock);
}
