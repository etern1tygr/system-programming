#ifndef BANKSERVER_HASHTABLE_H
#define BANKSERVER_HASHTABLE_H

#include "bankserver_structs.h"


unsigned long Hash(char *str);
int CreateHashStruct(struct HashStruct** hashStruct, int HashEntries);
int FillHashTableElem(struct HashStruct** hashStruct, char* name, int init_amount, int check);
int SearchNodeInHashTableAndCollisionLists(struct HashStruct** hashStruct, char* name);
int DeleteHashStruct(struct HashStruct** hashStruct);
int CreateCollisionElement(struct HashTableElement* prevElement, char* name);

#endif	// BANKSERVER_HASHTABLE_H

