#include <pthread.h>
#include "bankserver_hashtable.h"
#include "bankserver_lists.h"
#include "bankserver_accounts.h"

unsigned long Hash(char* str) {//exei dothei parapobh sto readme
    unsigned long hash = 5381;
    int c;

    while ((c = *str++))
        hash = ((hash << 5) + hash) + c; /* hash * 33 + c */

    return hash;
}

int CreateCollisionElement(struct HashTableElement* prevElement, char* name) {//dhmiourgei enan HashTableElement otan exoume collision
    if ((prevElement == NULL) || (prevElement->Account == NULL)) {
        printf("failure: Last Element or Element's pointer to %s is NULL in Collision List\n", name);
        fflush(stdout);
        return 0;
    }
    struct HashTableElement* newHashTableElement = malloc(sizeof (struct HashTableElement));
    if (newHashTableElement == NULL) {
        printf("failure: malloc failed\n");
        fflush(stdout);
        exit(1);
    }
    newHashTableElement->Account = NULL; //to neo element dn deixnei kapou gia tn wra
    strncpy(newHashTableElement->name, name, strlen(name) + 1); //exei tou account pou tha eisaxthei metepeita]
    newHashTableElement->key = prevElement->key; //exei to key toy head hashtable element
    newHashTableElement->nextCollisionAccount = NULL; //to epomeno collision element einai null
    prevElement->nextCollisionAccount = newHashTableElement; //to prohgoumeno deixnei sto neo
    newHashTableElement = NULL;
    printf("Collision!\n");
    fflush(stdout);
    return 1;
}

int CreateHashStruct(struct HashStruct** hashStruct, int HashEntries) {//dhmiourgei to hashtable me ola ta elements
    if ((((*hashStruct) = malloc(sizeof (struct HashStruct))) == NULL) || (HashEntries == 0)) {
        printf("failure: HashStruct malloc FAILED or zero HashEntries\n");
        fflush(stdout);
        exit(1);
    }
    (*hashStruct)->size = HashEntries;
    if (((*hashStruct)->table = malloc(sizeof (struct HashTableElement*)*HashEntries)) == NULL) {
        printf("failure: HashTable malloc FAILED\n");
        fflush(stdout);
        exit(1);
    }

    if (((*hashStruct)->mutexes = malloc(sizeof (pthread_mutex_t) * HashEntries)) == NULL) {
        printf("failure: HashTable malloc mutexes FAILED\n");
        fflush(stdout);
        exit(1);
    }

    int i;
    for (i = 0; i < HashEntries; i++) {//gia kathe hash element
        if (((*hashStruct)->table[i] = malloc(sizeof (struct HashTableElement))) == NULL) {
            printf("failure: HashTable[%d] malloc FAILED\n", i);
            fflush(stdout);
            exit(1);
        }
        (*hashStruct)->table[i]->nextCollisionAccount = NULL; //den exei arxika collision lisy
        (*hashStruct)->table[i]->Account = NULL; //dn deixnei se account
        strncpy((*hashStruct)->table[i]->name, "invalid", strlen("invalid") + 1); //den exei name
        (*hashStruct)->table[i]->key = -1; //oute kleidi(id%size)       


        pthread_mutexattr_t attr;
        pthread_mutexattr_init(&attr);
        pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);
        pthread_mutex_init(& (*hashStruct)->mutexes[i], &attr);
    }
    printf("HashStruct and its components CREATED\n");
    fflush(stdout);
    return 1;
}

int FillHashTableElem(struct HashStruct** hashStruct, char* name, int init_amount, int check) {//gemizei to hash element me enan account
    if ((*hashStruct) == NULL) {
        printf("failure: hashStruct for %s is NULL\n", name);
        fflush(stdout);
        return 0;
    }
    // int check = SearchNodeInHashTableAndCollisionLists(hashStruct, name);//psaxnei na vrei ean yparxei account me name 
    if (check == 0) { // found in head
        printf("failure: found in head, Will NOT FILL HashTableElem with %s\n", name);
        fflush(stdout);
        return 0;
    } else if (check >= 1) { // found in collision list
        printf("failure: found in collision list, Will NOT FILL HashTableElem with %s\n", name);
        fflush(stdout);
        return 0;
    } else if (check == -1) { // error
        printf("failure: Error, Will NOT FILL HashTableElem with %s\n", name);
        fflush(stdout);
        return 0;
    }
    //ean dn ton vrei, ton dhmiourgei
    unsigned long hash = Hash(name);
    int table_index = hash % (*hashStruct)->size; //vres an yparxei to account
    struct HashTableElement* cursor = (*hashStruct)->table[table_index];
    if (strcmp((*hashStruct)->table[table_index]->name, "invalid")) { // ean einai element p exei hdh account stn yparxousa thesh me table[i]->name kai p theloume na valoume tn neo, exoume collision safto to keli
        while (cursor->nextCollisionAccount != NULL) {//ftase sto last collision element ean exei collision list
            cursor = cursor->nextCollisionAccount;
        }//ean exei hdh element stn collision list, o cursor einai sto last collision element, alliws sto table[i], dld to head ths collision list otan einai praktika kenh
        if (!CreateCollisionElement(cursor, name)) {
            cursor = NULL;
            printf("failure: Will NOT FILL HashTableElem(no Collision Detection) with %s\n", name);
            fflush(stdout);
            return 0;
        }
        cursor = cursor->nextCollisionAccount;
    }
    else {//alliws an dn exei hdh account, tote dn exoume collision opote eisagoume to neo name sto element
        strncpy((*hashStruct)->table[table_index]->name, name, strlen(name) + 1);
    }
    //se kathe periptwsh ftiaxnoume tn account
    if (createAccount(name, cursor, init_amount, (*hashStruct)->size)) {
        cursor = NULL;
        return 1;
    }
    printf("failure: Error, COULD NOT FILL HashTableElem with %s\n", name);
    fflush(stdout);
    cursor = NULL;
    return 0;
}

int SearchNodeInHashTableAndCollisionLists(struct HashStruct** hashStruct, char* name) {//sarwnei to hashtable element gia account
    unsigned long hash = Hash(name);
    int table_index = hash % (*hashStruct)->size; //vres an yparxei to account
    if (((*hashStruct) == NULL) || ((*hashStruct)->table == NULL)) {
        printf("%s, hashStruct or its table is NULL\n", name);
        fflush(stdout);
        return -1;
    }

    if (((*hashStruct)->table[table_index] != NULL)&&
            (((*hashStruct)->table[table_index]->key) == (table_index))&&
            (!strcmp((*hashStruct)->table[table_index]->name, name))) {//an yparxei mesa stis kefales toy hashtable element
        printf("Account %s exists\n", name);
        fflush(stdout);
        return 0;
    } else if (((*hashStruct)->table[table_index] != NULL)&&(((*hashStruct)->table[table_index]->key) == table_index)
            &&(strcmp((*hashStruct)->table[table_index]->name, name))) {//an yparxei se collision list, epistrefei poso apexei apo tn kefali
        struct HashTableElement* cursor = (*hashStruct)->table[table_index];
        int steps = 0;
        while (cursor != NULL) {
            if (!strcmp(cursor->name, name)) {
                printf("Account %s exists\n", name);
                fflush(stdout);
                cursor = NULL;
                return steps;
            }
            cursor = cursor->nextCollisionAccount;
            steps++;
        }
    }
    printf("Account %s doesn't exist\n", name);
    fflush(stdout);
    return -2;
}

int DeleteHashStruct(struct HashStruct** hashStruct) {


    if (((*hashStruct) == NULL) || ((*hashStruct)->table == NULL)) {
        printf("failure: hashStruct or its table is NULL\n");
        fflush(stdout);
        return 0;
        ;
    }

    if (EmptyAccounts(hashStruct)) {//adeiazoume to hashtable apo ta accounts, dld katastrofh 
        int i;
        for (i = 0; i < (*hashStruct)->size; i++) {//kai free ola ta hash elements(dld ta heads kathe collision list)
            free((*hashStruct)->table[i]);
            (*hashStruct)->table[i] = NULL;

            pthread_mutex_destroy(& (*hashStruct)->mutexes[i]);
        }
        free((*hashStruct)->table); //kane free telika to table kai to struct
        (*hashStruct)->table = NULL;
        free((*hashStruct)->mutexes);
        free((*hashStruct));
        (*hashStruct) = NULL;
        printf("HashStruct DELETED\n");
        fflush(stdout);
        return 1;
    }
    printf("failure: Error, HashStruct NOT DELETED \n");
    fflush(stdout);
    return 0;
}
