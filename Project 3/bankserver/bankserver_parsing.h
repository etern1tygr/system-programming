#ifndef BANKSERVER_PARSING_H
#define BANKSERVER_PARSING_H

#include "bankserver_structs.h"
void TerminalArg(int* port, int* pool_size, int* queue_size, int argc, char** argv);
int add_accountInput(int fd, struct HashStruct** hashStruct, char* str);
int add_transferInput(int fd, struct HashStruct** hashStruct, char* str);
int add_multi_transferInput(int fd, struct HashStruct** hashStruct, char* str);
int print_balanceInput(int fd, struct HashStruct** hashStruct, char* str);
int print_multi_balanceInput(int fd, struct HashStruct** hashStruct, char* str);
#endif	// BANKSERVER_PARSING_H

