#ifndef BANKSERVER_WORK_H
#define BANKSERVER_WORK_H

#include "bankserver_structs.h"

void service(int newsock, struct HashStruct** hashStruct);
int doPrepareSocket(int port);
void doClearSocket(int fd);
int doReadfromClient(int sock, struct HashStruct** hashStruct);
void doSendtoClient(int fd, char* str);

#endif	// BANKSERVER_WORK_H

