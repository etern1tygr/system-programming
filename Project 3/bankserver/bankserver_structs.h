#ifndef BANKSERVER_STRUCTS_DEFINES_H
#define BANKSERVER_STRUCTS_DEFINES_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>


typedef struct {
    int queue_size;
    int* data;
    int start;
    int end;
    int count;
} pool_t;



struct HashStruct{//periexei to hashtable kai megethos
    int size;
    struct HashTableElement** table;
    pthread_mutex_t * mutexes;
};


struct HashTableElement{//einai to element tou hashtable
    int key;
    char name[100];
    struct HashTableElement* nextCollisionAccount;//deixnei ston epomeno collision node
    struct Account* Account; //kai stn komvo tou grafou
};


struct InTransferElem{// dipla syndedemenh lista apo elements p deixnoun se eswterikes akmes
    struct InTransferElem* nextInTransfer;
    struct InTransferElem* prevInTransfer;
    struct Transfer* TransferToCurrentNode;
};


struct ListOfInTransfers{//h lista
    int size;
    struct Account* Account;//se poio komvo anhkei
    struct InTransferElem* startInTransfer;
    struct InTransferElem* lastInTransfer;

};
 

struct OutTransferElem{// dipla syndedemenh lista apo elements p deixnoun se ekswterikes akmes
    struct OutTransferElem* nextOutTransfer;
    struct OutTransferElem* prevOutTransfer;
    struct Transfer* TransferToNextNode;
};


struct ListOfOutTransfers{//h adistoixh lista
    int size;
    struct Account* Account;//se poio komvo anhkei
    struct OutTransferElem* startOutTransfer;
    struct OutTransferElem* lastOutTransfer;
};


struct Transfer{//h akmh metaksy dyo komvwn, deixnei stn komvo proelefsis kai katalhkshs
    double amount;
    struct Account* toAccount;
    struct Account* fromAccount;
};


struct Account {//o komvos me tis list's tou
    char name[100];
    double amount;
    struct ListOfInTransfers* InTransferList;
    struct ListOfOutTransfers* OutTransferList;
};

#endif	// BANKSERVER_STRUCTS_DEFINES_H

