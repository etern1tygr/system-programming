#ifndef BANKSERVER_COMMAND_H
#define BANKSERVER_COMMAND_H

#include "bankserver_structs.h"


int add_account(struct HashStruct** hashStruct, char* name, double init_amount, int delay);
int add_transfer(struct HashStruct** hashStruct, double amount, char* src_name, char* dst_name, int delay);
int add_multi_transfer(struct HashStruct** hashStruct, double amount,  char* src_name, char** dst_name_list, int size, int delay);
double print_balance(struct HashStruct** hashStruct, char* name);


#endif	// BANKSERVER_COMMAND_H

