#include <string.h>
#include <sys/wait.h>
#include <dirent.h>
#include <fcntl.h>
#include <errno.h>
#include <pthread.h>
#include <unistd.h>
#include "bankserver_structs.h"
#include "bankserver_work.h"
#include "bankserver_hashtable.h"
#include "bankserver_parsing.h"
#include <stdio.h>
#include <sys/wait.h>      /* sockets */
#include <sys/types.h>      /* sockets */
#include <sys/socket.h>      /* sockets */
#include <netinet/in.h>      /* internet sockets */
#include <netdb.h>          /* gethostbyaddr */
#include <unistd.h>          /* fork */  
#include <stdlib.h>          /* exit */
#include <ctype.h>          /* toupper */
#include <signal.h>          /* signal */
#define SLOTS 211

int mastersock = -1;
struct HashStruct* hashStruct = NULL;

pthread_mutex_t mtx;
pthread_cond_t cond_nonempty;
pthread_cond_t cond_nonfull;
pool_t pool;
pthread_t * ids;
int threadpoolsize = -1;
int queuesize = -1;

void doPreparePool(pool_t * pool, int queuesize);
void DeletePool(pool_t * pool);
void place(pool_t * pool, int data);
int obtain(pool_t * pool);
void * main_thread(void * a);
void doPrepareThreads(int threadpoolsize);

void doPreparePool(pool_t * pool, int queuesize) {
    pool->start = 0;
    pool->end = -1;
    pool->count = 0;
    pool->queue_size = queuesize;
    pool->data = malloc(sizeof (int)*queuesize);

    pthread_mutex_init(&mtx, 0);
    pthread_cond_init(&cond_nonempty, 0);
    pthread_cond_init(&cond_nonfull, 0);
}

void DeletePool(pool_t * pool) {
    free(pool->data);
    pool->data = NULL;

    pthread_cond_destroy(&cond_nonempty);
    pthread_cond_destroy(&cond_nonfull);
    pthread_mutex_destroy(&mtx);
}

void place(pool_t * pool, int data) {
    pthread_mutex_lock(&mtx);
    while (pool->count >= pool->queue_size) {
        printf(">> Found Buffer Full \n");
        pthread_cond_wait(&cond_nonfull, &mtx);
    }
    pool->end = (pool->end + 1) % pool->queue_size;
    pool->data[pool->end] = data;
    pool->count++;
    pthread_cond_signal(&cond_nonempty);
    pthread_mutex_unlock(&mtx);
}

int obtain(pool_t * pool) {
    int data = 0;
    pthread_mutex_lock(&mtx);
    while (pool->count <= 0) {
        printf(">> Found Buffer Empty \n");
        pthread_cond_wait(&cond_nonempty, &mtx);
    }
    data = pool->data[pool->start];
    pool->start = (pool->start + 1) % pool->queue_size;
    pool->count--;
    pthread_cond_signal(&cond_nonfull);
    pthread_mutex_unlock(&mtx);
    return data;
}

void * main_thread(void * a) {
    sigset_t set;
    int newsock;
    //edw theloume na ginei blocked to signal apo ta threads
    sigemptyset(&set);
    sigaddset(&set, SIGINT);
    pthread_sigmask(SIG_SETMASK, &set, NULL);

    while ((newsock = obtain(&pool)) > 0) {//kathe thread pairnei ena socket fd apo tn oura, oso dn epistrafei/diavasei ws socket to 0(dld sinithiki termatismou thread)
        service(newsock, &hashStruct);//ksekinaei to service tou sigekrimenou socket-client kai ean kleisei to socket apo tn plevra tou server(kai prwta client) epixeirei na analavei epomeno client
    }
    //pavei to thread na ektelei tn main_thread
    return NULL;
}

void doPrepareThreads(int threadpoolsize) {//dhmiourgia threads kai ektelesh ths main_thread apo to kathena
    pthread_t id;
    int i = 0;
    ids = malloc(threadpoolsize * sizeof (pthread_t));
    while (threadpoolsize > 0) {
        pthread_create(&id, NULL, main_thread, NULL);
        ids[i] = id;
        printf("thread %lu created \n", ids[i]);
        i++;
        threadpoolsize--;
    }
}

void sig(int n) {
    int i;
    doClearSocket(mastersock);

    printf("Shutting down %d threads ... \n", threadpoolsize);
    
    for (i = 0; i < threadpoolsize; i++) {
        place(&pool, 0);
    }

    for (i = 0; i < threadpoolsize; i++) {
        printf("joining thread %lu \n", ids[i]);
        pthread_join(ids[i], NULL);
    }

    puts("DeleteHashStruct");
    DeleteHashStruct(&hashStruct);
    puts("DeletePool");
    DeletePool(&pool);
    free(ids);
    hashStruct = NULL;
    ids = NULL;

    printf("Final Exit in 1 sec\n");
    sleep(1);
    exit(0);
}


int main(int argc, char** argv) {
    int port = -1;
    //signal p tha ginei catched apo tn main=patera kai blocked apo ta threads=childs
    struct sigaction shutdownmain;
    shutdownmain.sa_handler = sig;
    shutdownmain.sa_flags = 0;
    sigemptyset(&shutdownmain.sa_mask);
    sigaddset(&shutdownmain.sa_mask, SIGINT);

    sigaction(SIGINT, &shutdownmain, NULL);

    //logiko parsing
    TerminalArg(&port, &threadpoolsize, &queuesize, argc, argv); //kathorizei ta arguments apo to terminal
    if((threadpoolsize <= 0)||(queuesize <= 0)||(port < 0)){
        printf("invalid values \n");
        fflush(stdout);
        exit(1);
    }

    CreateHashStruct(&hashStruct, SLOTS);

    if ((mastersock = doPrepareSocket(port)) < 0) {
        perror("Error: ");
        exit(1);
    }

    doPreparePool(&pool, queuesize);//dhmiourgia queue

    doPrepareThreads(threadpoolsize); //dhmiourgia threads

    while (1) {//loop gia tn main=patera opou perimenei nea connections/clients
        struct sockaddr_in client;
        socklen_t clientlen = sizeof(client);
        struct sockaddr *clientptr = (struct sockaddr *) &client;
        int newsock;

        printf("Listening for connections to port %d\n", port);
        fflush(stdout);

        if ((newsock = accept(mastersock, clientptr, &clientlen)) < 0) {
            perror("accept");
            return -1;
        }

        printf("Accepted connection\n");
        fflush(stdout);
        //topothetei stn oura to socket fd tou client
        place(&pool, newsock);
    }

    return 0;
}
