#ifndef BANKSERVER_LISTS_H
#define BANKSERVER_LISTS_H

#include "bankserver_structs.h"

int InTransferListCreate(struct Account* N);
int OutTransferListCreate(struct Account* N);

int InTransferElemCreate(struct Account* N, struct Transfer* newIncoming);
int OutTransferElemCreate(struct Account* N, struct Transfer* newOutcoming);

int DeleteInTransferListForN(struct Account* N);
int DeleteOutTransferListForN(struct Account* N);

int DeleteInTransferElemForN2(struct Account* N1, struct Account* N2, struct Transfer* pivotTransfer);
int DeleteOutTransferElemForN1(struct Account* N1, struct Account* N2, struct Transfer* pivotTransfer);

int DeleteInTransfersForN(struct Account* N);
int DeleteOutTransfersForN(struct Account* N);

int createAccount(char* name, struct HashTableElement* tobecreated, int init_amount, int size);


#endif	// BANKSERVER_LISTS_H

