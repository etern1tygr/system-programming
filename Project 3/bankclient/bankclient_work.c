#include <ctype.h>
#include <stdio.h>
#include <sys/types.h>	     /* sockets */
#include <sys/socket.h>	     /* sockets */
#include <netinet/in.h>	     /* internet sockets */
#include <unistd.h>          /* read, write, close */
#include <netdb.h>	         /* gethostbyaddr */
#include <stdlib.h>	         /* exit */
#include <string.h>
#include <stdint.h>
#include <fcntl.h>	         /* strlen */
#include "bankclient_work.h"

void TerminalArg(char* hostname, int* port, char* commandfile, int argc, char** argv) {

    if (argc == 7) {
            //an lavoume  7 orismata me oles tis metatheseis
        if ((!strcmp(argv[1], "-h"))&&(!strcmp(argv[3], "-p"))&&(!strcmp(argv[5], "-i"))) {
            strncpy(hostname, argv[2], strlen(argv[2])+1);
            *port = atoi(argv[4]);
            strncpy(commandfile, argv[6], strlen(argv[6])+1);

        }
        else if ((!strcmp(argv[1], "-h"))&&(!strcmp(argv[3], "-i"))&&(!strcmp(argv[5], "-p"))) {
            strncpy(hostname, argv[2], strlen(argv[2])+1);
            *port = atoi(argv[6]);
            strncpy(commandfile, argv[4], strlen(argv[4])+1);
        }
        else if ((!strcmp(argv[1], "-i"))&&(!strcmp(argv[3], "-p"))&&(!strcmp(argv[5], "-h"))) {
            strncpy(hostname, argv[6], strlen(argv[6])+1);
            *port = atoi(argv[4]);
            strncpy(commandfile, argv[2], strlen(argv[2])+1);
        } 

        else if ((!strcmp(argv[1], "-i"))&&(!strcmp(argv[3], "-h"))&&(!strcmp(argv[5], "-p"))) {
            strncpy(hostname, argv[4], strlen(argv[4])+1);
            *port = atoi(argv[6]);
            strncpy(commandfile, argv[2], strlen(argv[2])+1);
        }
        else if ((!strcmp(argv[1], "-p"))&&(!strcmp(argv[3], "-h"))&&(!strcmp(argv[5], "-i"))) {
            strncpy(hostname, argv[4], strlen(argv[4])+1);
            *port = atoi(argv[2]);
            strncpy(commandfile, argv[6], strlen(argv[6])+1);
        }
        else if ((!strcmp(argv[1], "-p"))&&(!strcmp(argv[3], "-i"))&&(!strcmp(argv[5], "-h"))) {
            strncpy(hostname, argv[6], strlen(argv[6])+1);
            *port = atoi(argv[2]);
            strncpy(commandfile, argv[4], strlen(argv[4])+1);
        }
        else{
            printf("wrong argument sequence\n");
            fflush(stdout);
            exit(1);
        } 
    }
    else if(argc == 5){
            //an lavoume  toul 5, arxikopoiioume to commandfile
        if ((!strcmp(argv[1], "-h"))&&(!strcmp(argv[3], "-p"))) {
            strncpy(hostname, argv[2], strlen(argv[2])+1);
            *port = atoi(argv[4]);
            commandfile[0] = '\0';

        }
        else if ((!strcmp(argv[1], "-p"))&&(!strcmp(argv[3], "-h"))) {
            strncpy(hostname, argv[4], strlen(argv[4])+1);
            *port = atoi(argv[2]);
            commandfile[0] = '\0';
        }
        else{
            printf("wrong argument sequence\n");
            fflush(stdout);
            exit(1);
        }
    }
    else{
        printf("wrong number of arguments\n");
        fflush(stdout);
        exit(1);
    }

}

void doSendtoServer(int fd, char* str) {//epistrefoume analoga me to poia edolh einai, ena sigekrimeno int stn main
    //vgazoume tn allagh grammhs
    char command[strlen(str) + 1]; //static pinaka n chars +1 gia null terminator
    strncpy(command, str, (strlen(str))); //copy to string stn static
    
    if (command[strlen(str) - 1] == '\n') {
        command[strlen(str) - 1] = '\0';
    } 
    else {
        command[strlen(str)] = '\0';
    }
    //stelnoume posa bytes einai kai epeita to idio to mhnhma
    int32_t l = htonl(strlen(command)+1);
    write(fd, &l, sizeof(l));   // 4B = strlen(str)
    write(fd, command, strlen(command)+1);          // str data
    printf("command: %s sent\n", command);
    printf("client send: %d chars\n", (int)(strlen(command)+1));
    fflush(stdout);
    fflush(stdout);
}

void doReadfromServer(int fd) { //diavazei apo to pipe read
    char *message;
    int32_t l1;
    //omoiws diavazoume posa bytes einai to mhnyma, kai epeita to mhnyma
    read(fd, &l1, sizeof(l1));
    
    int32_t l = ntohl(l1);
    message = malloc(l);
    read(fd, message, l);
    
    printf("response: %s \n", message);
    printf("client read: %d chars\n", l);
    fflush(stdout);
    fflush(stdout);
    free(message);
}

int doReadfromInput(char* str) {//epistrefoume analoga me to poia edolh einai, ena sigekrimeno int stn main
    //ginetai to sydaktiko parsing gia oles edoles. Logiko parsing ginetai sto server
    int token_length;
    int flag = 0;
    int number = 0;
    int notnumber = 0;
    int check = 0;
    char* token;
    char command[strlen(str) + 1]; //static pinaka n chars +1 gia null terminator
    strncpy(command, str, (strlen(str))); //copy to string stn static
    if (command[strlen(str) - 1] == '\n') {
        command[strlen(str) - 1] = '\0';
    } 
    else {
        command[strlen(str)] = '\0';
    }
    char* rest;
    char* start = command;
    token = strtok_r(start, " ", &rest);

    if (!strcmp(token, "add_account")) {//an einai h add_account
        token = strtok_r(NULL, " ", &rest);
        while (token != NULL) {
            token_length = strlen(token);
            char digits[token_length];
            strncpy(digits, token, token_length);
            int i = 0;
            while (i <= token_length - 1) {//elegxos ean proketai gia digits
                if (isdigit(digits[i]) == 0) {
                    notnumber++; //ean dn einai arithmos
                    flag = 1;
                    break;
                }
                i++;
            }

            if (flag == 0) {//ean einai
                number++;
            }

            if ((notnumber == 0)&&(number == 1)) {
                check++;

            }
            else if ((notnumber == 1)&&(number == 1)) {
                check++;

            } 
            else if ((notnumber == 1)&&(number == 2)) {
                check++;

            } 
            else {
                return -1;
            }
            token = strtok_r(NULL, " ", &rest);
        }
        
        if((check == 3)||(check == 2)){
            return 0;
        }
        else{
            return -1;
        }
    }
    else if(!strcmp(token, "add_transfer")){
        token = strtok_r(NULL, " ", &rest);
        while (token != NULL) {
            token_length = strlen(token);
            char digits[token_length];
            strncpy(digits, token, token_length);
            int i = 0;
            while (i <= token_length - 1) {//elegxos ean proketai gia digits
                if (isdigit(digits[i]) == 0) {
                    notnumber++; //ean dn einai arithmos
                    flag = 1;
                    break;
                }
                i++;
            }

            if (flag == 0) {//ean einai
                number++;
            }

            if ((notnumber == 0)&&(number == 1)) {
                check++;
            }
            else if ((notnumber == 1)&&(number == 1)) {
                check++;

            } 
            else if ((notnumber == 2)&&(number == 1)) {
                check++;
            } 
            else if ((notnumber == 2)&&(number == 2)) {
                check++;
            } 
            else{
                return -1;
            }
            token = strtok_r(NULL, " ", &rest);
            flag = 0;
        }
        if((check == 4)||(check == 3)){
            return 0;
        }
        else{
            return -1;
        }  
    }
    else if(!strcmp(token, "add_multi_transfer")){  
        token = strtok_r(NULL, " ", &rest);
        while (token != NULL) {
            token_length = strlen(token);
            char digits[token_length];
            strncpy(digits, token, token_length);
            int i = 0;
            while (i <= token_length - 1) {//elegxos ean proketai gia digits
                if (isdigit(digits[i]) == 0) {
                    notnumber++; //ean dn einai arithmos
                    flag = 1;
                    break;
                }
                i++;
          }

            if (flag == 0) {//ean einai
                number++;
            }

            if ((notnumber == 0)&&(number == 1)) {
                check++;
            }
            else if ((notnumber == 1)&&(number == 1)) {
                check++;

            } 
            else if ((notnumber >= 1)&&(number == 1)) {
                check++;
            } 
            
            else if ((notnumber >= 2)&&(number == 2)) {
                check++;
            } 
            
            else {
                return -1;
            }
            token = strtok_r(NULL, " ", &rest);
            flag = 0;
        }
        if(check >= 3){
            return 0;
        }
        else{
            return -1;
        }  
    }
    else if(!strcmp(token, "print_balance")) {
        token = strtok_r(NULL, " ", &rest);
        if (token != NULL) {
            token_length = strlen(token);
            char digits[token_length];
            strncpy(digits, token, token_length);
            int i = 0;
            while (i <= token_length - 1) {//elegxos ean proketai gia digits
                if (isdigit(digits[i]) == 0) {
                    flag = 1;
                    break;
                }
                i++;
            }

            if (flag == 0) {
                return -1;
            }
            token = strtok_r(NULL, " ", &rest);
            if(token == NULL){
                return 0;
            }
            else{
                return -1;
            }
        }
        return -1;
    }
    else if(!strcmp(token, "print_multi_balance")){
        token = strtok_r(NULL, " ", &rest);
        if(token == NULL){
            return -1;
        }
        while (token != NULL) {
            token_length = strlen(token);
            char digits[token_length];
            strncpy(digits, token, token_length);
            int i = 0;
            while (i <= token_length - 1) {//elegxos ean proketai gia digits
                if (isdigit(digits[i]) == 0) {
                    flag = 1;
                    break;
                }
                i++;
            }

            if (flag == 0) {
                return -1;
            }
            token = strtok_r(NULL, " ", &rest);
            flag = 0;
        }
        return 0;
    }
    else if(!strcmp(token, "sleep")){
        token = strtok_r(NULL, " ", &rest);
        if (token != NULL) {
            token_length = strlen(token);
            char digits[token_length];
            strncpy(digits, token, token_length);
            int i = 0;
            while (i <= token_length - 1) {//elegxos ean proketai gia digits
                if (isdigit(digits[i]) == 0) {
                    return -1;
                }
                i++;
            }
            int time;
            time = atoi(token);
            token = strtok_r(NULL, " ", &rest);
            if (token != NULL) {
                return -1;
            }
            else{
                return time;
            }

        }
        return -1;
    }  
    else if(!strcmp(token, "exit")){
        token = strtok_r(NULL, " ", &rest);
        if (token != NULL) {
            return -1;
        }
        else{
            return -2;
        }
    }
    else{
        return 0;
    }
}

int doPrepareSocket(char *hostname, int port) {
    int sock;
    struct sockaddr_in server;
    struct sockaddr *serverptr = (struct sockaddr*) &server;
    struct hostent *rem;
    
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        perror("socket");
        return -1;
    }
    if ((rem = gethostbyname(hostname)) == NULL) {
        perror("gethostbyname");
        return -1;
    }
    server.sin_family = AF_INET; /* Internet domain */
    memcpy(&server.sin_addr, rem->h_addr, rem->h_length);
    server.sin_port = htons(port); /* Server port */
    
    printf("Connecting to %s port %d\n", hostname, port);
    fflush(stdout);
    
    if (connect(sock, serverptr, sizeof (server)) < 0) {
        perror("connect");
        return -1;
    }
    
    return sock;
}

void doClearSocket(int sock) {
    close(sock); 
}