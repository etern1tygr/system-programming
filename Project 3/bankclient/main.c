#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <dirent.h>
#include <ctype.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include "bankclient_work.h"
#include <signal.h>
#include <stdint.h>

int main(int argc, char** argv) {
    signal(SIGINT, SIG_IGN);

    int port = -1;

    char hostname[8000];
    char commandfile[8000];
    char str[8000];

    int function = 0;
    
    //parsing ta args
    TerminalArg(hostname, &port, commandfile, argc, argv); //kathorizei ta arguments apo to terminal
/*
    commandfile[0] = '\0';
    strcpy(hostname, "localhost");
    port = 4445;
*/
    FILE* f;
    //a dn yparxei file sth grammh edolwn
    if(commandfile[0] == '\0'){
        f = NULL;  
    }
    else{//alliws anoikse to
        if((f = fopen(commandfile, "r")) == NULL){
            printf("no valid file found\n");
            fflush(stdout);
            exit(1);
        }
    }
    //proetoimasia socket/dimiourgia
    int fd = doPrepareSocket(hostname, port);        
    
    while (fd > 0){//diavazei apo to stdin
        
        if(f != NULL){//ean exei anoiksei arxeio
           if(fgets(str, sizeof(str), f) == NULL){//diavazoume grammh grammh ews otou EOF
               fclose(f);//kleinoume to arxeio se periptwsh eof
               f = NULL;
               break;
           }
        }
        else {
            printf("\n");
            fflush(stdout);
            printf("Type client command\n");
            fflush(stdout);
            printf("\n");
            fflush(stdout);
            //diavazei apo stdin
            if(fgets(str, sizeof (str), stdin) == NULL) {
                exit(1);
            }
            fflush(stdout);
        }
        
        

        //analoga me to return ths read, stelnoume, diavazoyme, exit h sleep
        function = doReadfromInput(str);
        if (function == -1) {       // syntax error
            printf("wrong command: %s\n", str);
            fflush(stdout);
            continue;
        } else if (function == -2) { // exit
            break;
        } else if (function == 0) {  // sleep
            doSendtoServer(fd, str);
            doReadfromServer(fd);            
        } else if (function > 0) {
            printf("I am sleeping\n");
            fflush(stdout);
            usleep(function*1000);
            printf("Woke Up\n");
            fflush(stdout);
        }
    }
    //eiserxetai meta apo break/exit
    doClearSocket(fd);

    printf("finished. \n");
    return 1;
}
