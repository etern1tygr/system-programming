#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void TerminalArg(char * hostname, int * port, char *commandfile, int argc, char** argv);
int doReadfromInput(char* str);

void doSendtoServer(int fd, char* str);
void doReadfromServer(int fd);

int doPrepareSocket(char *hostname, int port);
void doClearSocket(int fd);


//---client----server-----//
